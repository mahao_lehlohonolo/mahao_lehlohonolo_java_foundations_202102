package com.psybergate.grad2021.jeefnds.servlets.hwweb3;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3.MyAnnotations.DomainClass;

import java.time.LocalDate;

@DomainClass(name = "servlets_customer_tbl")
public class Customer {

  @DomainAttribute(name = "customer_num", type = "int", primaryKey = true)
  private long customerNum;

  @DomainAttribute(name = "name")
  private String name;

  @DomainAttribute(name = "surname")
  private String surname;

  @DomainAttribute(name = "date_of_birth")
  private LocalDate dateOfBirth;

  public Customer(long customerNum, String name, String surname, LocalDate dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public Customer() {
  }

  public long getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }


}
