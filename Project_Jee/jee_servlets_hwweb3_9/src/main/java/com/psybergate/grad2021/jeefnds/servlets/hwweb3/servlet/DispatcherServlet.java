package com.psybergate.grad2021.jeefnds.servlets.hwweb3.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

@WebServlet(urlPatterns = "/dispatcher/*")
public class DispatcherServlet extends HttpServlet {

  private static Properties PROPERTIES = new Properties();

  private static String FILENAME = "resources.properties";

  static {
    try {
      InputStream stream = DispatcherServlet.class.getClassLoader().getResourceAsStream(FILENAME);
      PROPERTIES.load(stream);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter out = resp.getWriter();
    out.println("<h1>" + controllerResponse(req, resp) + "<h1>");
  }

  private String controllerResponse(HttpServletRequest req, HttpServletResponse response) {
    String responseString;
    String pathInfo = req.getPathInfo();
    String property = PROPERTIES.getProperty(pathInfo.substring(1));
    String[] classAndMethodName = property.split("#");
    try {
      Class cls = Class.forName(classAndMethodName[0]);
      Object clz = cls.newInstance();
      String methodName = classAndMethodName[classAndMethodName.length - 1];
      Method method = clz.getClass().getDeclaredMethod(methodName, HttpServletRequest.class,
              HttpServletResponse.class);
      responseString = String.valueOf(method.invoke(clz, req, response));
    } catch (Throwable e) {
      throw new RuntimeException(e);
    }
    return responseString;
  }

}
