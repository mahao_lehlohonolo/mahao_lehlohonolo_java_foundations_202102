package com.psybergate.grad2021.jeefnds.enterprise.ce01a;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityUtility {

  public static void main(String[] args) {
    EntityManagerFactory factory = Persistence.createEntityManagerFactory("customerpersist");
    EntityManager entityManager = factory.createEntityManager();
    Customer customer = new Customer();
    customer.setCustomerNum(10111);
    customer.setName("Lehlohonolo");
    customer.setSurname("Mahao");
    customer.setDateOfBirth("961127");

    entityManager.getTransaction().begin();
    entityManager.persist(customer);
    entityManager.getTransaction().commit();
  }

}
