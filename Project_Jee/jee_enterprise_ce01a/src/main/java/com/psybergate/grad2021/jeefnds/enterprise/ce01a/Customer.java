package com.psybergate.grad2021.jeefnds.enterprise.ce01a;

public class Customer {

  private int customerNum;

  private String name;

  private String surname;

  private String dateOfBirth;

  public Customer() {
  }

  public void setCustomerNum(int customerNum) {
    this.customerNum = customerNum;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

}
