package com.psybergate.grad2021.jeefnds.servlets.arch3.controller;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

public class DateController {

  public String getCurrentDate(HttpServletRequest request) {
    return LocalDate.now() + "";
  }

  public String getTomorrowsDate(HttpServletRequest request) {
    return LocalDate.now().plusDays(1) + "";
  }

}
