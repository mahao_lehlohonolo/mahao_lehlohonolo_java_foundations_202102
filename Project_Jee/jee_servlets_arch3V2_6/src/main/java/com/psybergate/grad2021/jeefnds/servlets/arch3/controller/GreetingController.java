package com.psybergate.grad2021.jeefnds.servlets.arch3.controller;

import javax.servlet.http.HttpServletRequest;

public class GreetingController {

  public String hello(HttpServletRequest request) {
    return "Hello, " + request.getParameter("name") + "!";
  }

  public String goodbye(HttpServletRequest request) {
    return "Goodbye, " + request.getParameter("name") + "!";
  }

}
