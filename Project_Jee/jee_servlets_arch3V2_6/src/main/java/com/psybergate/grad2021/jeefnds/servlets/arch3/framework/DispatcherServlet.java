package com.psybergate.grad2021.jeefnds.servlets.arch3.framework;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

@WebServlet(urlPatterns = "/dispatcher2/*")
public class DispatcherServlet extends HttpServlet {

  private static Properties PROPERTIES = new Properties();

  private static String FILENAME = "resource.properties";

  static {
    try {
      InputStream stream = DispatcherServlet.class.getClassLoader().getResourceAsStream(FILENAME);
      PROPERTIES.load(stream);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter out = resp.getWriter();
    out.println("<h1>" + controllerResponse(req) + "<h1>");
  }

  private String controllerResponse(HttpServletRequest req) {
    String response;
    String pathInfo = req.getPathInfo();
    String property = PROPERTIES.getProperty(pathInfo.substring(1));
    String[] classAndMethodName = property.split("#");
    try {
      Class cls = Class.forName(classAndMethodName[0]);
      Object clz = cls.newInstance();
      String methodName = classAndMethodName[classAndMethodName.length - 1];
      Method method = clz.getClass().getMethod(methodName, HttpServletRequest.class);
      response = String.valueOf(method.invoke(clz, req));
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
    return response;
  }

}
