package com.psybergate.grad2021.jeefnds.servlets.arch1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/helloworld")
public class HelloWorldServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    PrintWriter out = resp.getWriter();
    if (req.getParameter("name") == null) {
      out.println("<h1>Hello!</h1>");
    }
    out.println("<h1>Hello, " + req.getParameter("name") + "!" + "</h1>");
    //input this url 'http://localhost:8080/jee_servlets_arch1/helloworld?name=Nonny' in the browser
  }

}
