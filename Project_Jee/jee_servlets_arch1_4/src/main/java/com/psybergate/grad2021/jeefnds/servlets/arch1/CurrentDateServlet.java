package com.psybergate.grad2021.jeefnds.servlets.arch1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet(urlPatterns = "/getcurrentdate")
public class CurrentDateServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    PrintWriter out = resp.getWriter();
    out.println("<h1>Current Date: " + LocalDate.now() + "</h1>");
  }

}
