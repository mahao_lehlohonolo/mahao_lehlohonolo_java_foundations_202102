package com.psybergate.grad2021.jeefnds.servlets.hwweb1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(urlPatterns = "/httpinfoservlet/*")
public class HttpInfoServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    PrintWriter printWriter = resp.getWriter();
    printWriter.println("<h1>Protocol: " + req.getProtocol() + "<h1>");
    printWriter.println("<h1>RequestURI: " + req.getRequestURI() + "<h1>");
    printWriter.println("<h1>PathInfo: " + req.getPathInfo() + "<h1>");
    printWriter.println("<h1>Method: " + req.getMethod() + "<h1>");
    Enumeration<String> headerNames = req.getHeaderNames();
    int count = 1;
    while (headerNames.hasMoreElements()) {
      String nextElement = headerNames.nextElement();
      printWriter.println("<h1>Headers<h1>");
      printWriter.println("<h1>  " + count++ + ". Header Name: " + nextElement + " | value: " + req.getHeader(nextElement) +
              "<h1>");
    }
  }

}
