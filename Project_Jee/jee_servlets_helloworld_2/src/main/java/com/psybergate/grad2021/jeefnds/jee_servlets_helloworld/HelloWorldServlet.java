package com.psybergate.grad2021.jeefnds.jee_servlets_helloworld;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * A web.xml file has to be done manually for this exercise in order to make a servlet.
 */

public class HelloWorldServlet extends HttpServlet {

  public HelloWorldServlet() {
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    PrintWriter out = resp.getWriter();
    out.println("<h1>Hello, world!</h1>");
    //Requester details
    out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
    out.println("<p>Protocol: " + req.getProtocol() + "</p>");
    out.println("<p>PathInfo: " + req.getPathInfo() + "</p>");
    out.println("<p>Remote Address: " + req.getRemoteAddr() + "</p>");
  }

}
