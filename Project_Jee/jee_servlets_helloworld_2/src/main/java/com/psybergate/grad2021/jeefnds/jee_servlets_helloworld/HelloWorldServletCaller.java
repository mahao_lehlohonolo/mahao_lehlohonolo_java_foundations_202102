package com.psybergate.grad2021.jeefnds.jee_servlets_helloworld;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HelloWorldServletCaller {

  public static void main(String[] args) throws IOException {
    try {
      //not sure why this batch file is not opening this way, other batch scripts seem to work
      // except this one.
      File startupBatch = new File("C:\\myprograms\\jee-servers\\apache-tomcat-9.0" +
              ".41-windows-x64\\apache-tomcat-9.0.41\\bin\\startup.bat");
      Desktop desktop = Desktop.getDesktop();
      desktop.open(startupBatch);
      URI url = new URI("http://localhost:8080/jee_servlets_helloworld/helloworld");
      desktop.browse(url);
    } catch (URISyntaxException | IOException e) {
      throw new RuntimeException(e);
    }
  }

}
