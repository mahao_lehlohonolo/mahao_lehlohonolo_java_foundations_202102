package com.psybergate.grad2021.jeefnds.ce3.version_1.implemenation;

import com.psybergate.grad2021.jeefnds.ce3.version_1.command.SumNumbers;
import com.psybergate.grad2021.jeefnds.ce3.version_1.standards.CommandResponse;

public class CommandResponse1<T extends CommandRequest1> implements CommandResponse {

  private T commandRequest;

  public CommandResponse1(T commandRequest) {
    this.commandRequest = commandRequest;
  }

  @Override
  public void execute() {
    if (commandRequest instanceof SumNumbers) {
      System.out.println("Sum is " + commandRequest.sum());
    }
  }

}
