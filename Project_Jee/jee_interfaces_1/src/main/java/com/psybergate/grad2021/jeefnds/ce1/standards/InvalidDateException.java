package com.psybergate.grad2021.jeefnds.ce1.standards;

public class InvalidDateException extends Exception {

  public InvalidDateException(String message) {
    super(message);
  }

}
