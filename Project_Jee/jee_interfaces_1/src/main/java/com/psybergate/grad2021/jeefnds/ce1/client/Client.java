package com.psybergate.grad2021.jeefnds.ce1.client;

import com.psybergate.grad2021.jeefnds.ce1.standards.InvalidDateException;
import com.psybergate.grad2021.jeefnds.ce1.implementation_1.DateImpl1;
import com.psybergate.grad2021.jeefnds.ce1.standards.Date;

public class Client {

  public static void main(String[] args) {
    try {
      //      DateImpl1 dateImpl = new DateImpl2();
      Date date = new DateImpl1().createDate(4, 7, 1998);
      System.out.println(date.addDays(56));
    } catch (InvalidDateException e) {
      throw new RuntimeException(e);
    }
  }

}
