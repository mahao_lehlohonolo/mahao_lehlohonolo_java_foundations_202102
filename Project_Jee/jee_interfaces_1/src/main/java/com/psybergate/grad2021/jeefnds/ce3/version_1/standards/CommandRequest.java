package com.psybergate.grad2021.jeefnds.ce3.version_1.standards;

public interface CommandRequest {

  public void execute();

}
