package com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.command;

import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.Command;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandRequest;

import java.util.List;

public class Add implements Command {

  public Add() {
  }

  @Override
  public void processRequest(CommandRequest request) {
    System.out.println("Getting response...");
    int sum = 0;
    List<Integer> integers = request.getRequest();
    for (Integer integer : integers) {
      sum += integer;
    }
    System.out.println("Sum = " + sum);
  }

}
