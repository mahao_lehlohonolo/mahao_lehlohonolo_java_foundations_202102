package com.psybergate.grad2021.jeefnds.ce3.version_1.implemenation;

import com.psybergate.grad2021.jeefnds.ce3.version_1.standards.CommandRequest;

public class CommandRequest1 implements CommandRequest {

  private CommandRequest1 commandRequest;

  public CommandRequest1(CommandRequest1 commandRequest) {
    this.commandRequest = commandRequest;
  }

  public CommandRequest1() {
  }

  @Override
  public void execute() {
    CommandResponse1 commandResponse = new CommandResponse1(commandRequest);
    commandResponse.execute();
  }

  public int sum() {
    return 0;
  }

}
