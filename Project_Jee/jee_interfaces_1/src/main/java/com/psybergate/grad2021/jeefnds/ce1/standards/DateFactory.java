package com.psybergate.grad2021.jeefnds.ce1.standards;

public interface DateFactory {

  Date createDate(int day, int month, int year) throws InvalidDateException;
  //must return the implementation
}
