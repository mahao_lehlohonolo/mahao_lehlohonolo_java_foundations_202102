package com.psybergate.grad2021.jeefnds.ce3.version_2.implementation;

import com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.command.Add;
import com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.command.Multiply;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.Command;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandRequest;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandResponse;

public class CommandResponseImpl implements CommandResponse {

  private Command command;

  private CommandRequest request;

  public CommandResponseImpl(Command command, CommandRequest request) {
    this.command = command;
    this.request = request;
  }

  @Override
  public void getResponse() {
    System.out.println("Processing request...");
    if(command instanceof Add){
      ((Add) command).processRequest(request);
    }
    if(command instanceof Multiply){
      ((Multiply) command).processRequest(request);
    }
  }

}
