package com.psybergate.grad2021.jeefnds.ce1.standards;

public interface Date {


  int getDay();

  int getMonth();

  int getYear();

  /**
   * @return true if year is a leap year.
   */
  boolean isLeapYear();

  /**
   * Add days to get a date.
   *
   * @param numberOfDays
   * @return a new Date(Date has to be immutable)
   */
  Date addDays(int numberOfDays) throws InvalidDateException;

}
