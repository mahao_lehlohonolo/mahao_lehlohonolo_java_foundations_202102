package com.psybergate.grad2021.jeefnds.ce3.version_2.implementation;

import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandRequest;

import java.util.List;

public class CommandRequestImpl implements CommandRequest {

  private List<Integer> integers;

  public CommandRequestImpl(List<Integer> integers) {
    this.integers = integers;
  }

  @Override
  public List<Integer> getRequest() {
    return integers;
  }

  @Override
  public String toString() {
    return "CommandRequestImpl{" +
            "integers=" + integers +
            '}';
  }

}
