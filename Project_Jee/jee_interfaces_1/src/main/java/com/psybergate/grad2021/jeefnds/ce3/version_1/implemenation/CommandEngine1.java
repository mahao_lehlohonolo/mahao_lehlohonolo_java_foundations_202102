package com.psybergate.grad2021.jeefnds.ce3.version_1.implemenation;

import com.psybergate.grad2021.jeefnds.ce3.version_1.standards.CommandEngine;

public class CommandEngine1 implements CommandEngine {

  /**
   * Based on my understanding, this is the controller class.
   */

  private CommandRequest1 request;

  public CommandEngine1(CommandRequest1 request) {
    this.request = request;
  }

  @Override
  public void processCommand(){
    CommandRequest1 commandRequest = new CommandRequest1(request);
    commandRequest.execute();
  }

}
