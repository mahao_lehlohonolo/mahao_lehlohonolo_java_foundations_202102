package com.psybergate.grad2021.jeefnds.ce2.implementation;

import com.psybergate.grad2021.jeefnds.ce2.implementation.MyConnection;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

public class MyDriver implements Driver {

  /**
   * This class will be registered during classloading.
   */
  static {
    try {
      DriverManager.deregisterDriver(new MyDriver());
    } catch (SQLException throwable) {
      throw new RuntimeException(throwable);
    }
  }

  @Override
  public MyConnection connect(String url, Properties info) throws SQLException {
    return null;
  }

  @Override
  public boolean acceptsURL(String url) throws SQLException {
    return false;
  }

  @Override
  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
    return new DriverPropertyInfo[0];
  }

  @Override
  public int getMajorVersion() {
    return 0;
  }

  @Override
  public int getMinorVersion() {
    return 0;
  }

  @Override
  public boolean jdbcCompliant() {
    return false;
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return null;
  }

}
