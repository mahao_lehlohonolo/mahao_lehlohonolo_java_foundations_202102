package com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.client;

import com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.CommandEngineImpl;
import com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.CommandRequestImpl;
import com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.command.Multiply;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.Command;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandEngine;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandRequest;

import java.util.Arrays;

public class Client {

  public static void main(String[] args) {
    CommandRequest request = new CommandRequestImpl(Arrays.asList(1, 2, 3, 4, 5));
    Command command = new Multiply();
    CommandEngine commandEngine = new CommandEngineImpl(command,
            request);
    commandEngine.execute();
  }

}
