package com.psybergate.grad2021.jeefnds.ce3.version_2.standards;

public interface Command {

  void processRequest(CommandRequest request);

}
