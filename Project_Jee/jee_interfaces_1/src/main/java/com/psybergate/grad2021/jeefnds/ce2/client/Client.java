package com.psybergate.grad2021.jeefnds.ce2.client;

import java.sql.*;

public class Client {

  public static void main(String[] args) {
    try {
      Class.forName("com.psybergate.grad2021.ce2.implementation.MyDriver");
      /**
       * the getConnection() method returns a Connection object. If I could have my own
       * implementation of DriverManger then for this method I would return MyConnection so that
       * I can use the implementations(i.e. MyConnection, MyDriver, MyResultSet, and MyStatement)
       */
      Connection connection = DriverManager.getConnection("mydb", "mahao", "1234");
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("select * from my_table");
    } catch (ClassNotFoundException | SQLException e) {
      throw new RuntimeException(e);
    }
  }

}
