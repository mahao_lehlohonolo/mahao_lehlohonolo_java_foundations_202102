package com.psybergate.grad2021.jeefnds.ce3.version_1.command;

import com.psybergate.grad2021.jeefnds.ce3.version_1.implemenation.CommandRequest1;

public class SumNumbers extends CommandRequest1 {

  private int num1;

  private int num2;

  public SumNumbers(int num1, int num2) {
    this.num1 = num1;
    this.num2 = num2;
  }

  public int getNum1() {
    return num1;
  }

  public int getNum2() {
    return num2;
  }

  @Override
  public int sum(){
    return this.num1 + this.num2;
  }

}
