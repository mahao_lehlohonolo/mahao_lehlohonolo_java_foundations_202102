package com.psybergate.grad2021.jeefnds.ce3.version_2.implementation.command;

import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.Command;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandRequest;

import java.util.List;

public class Multiply implements Command {

  public Multiply() {
  }

  public void processRequest(CommandRequest request) {
    System.out.println("Getting response...");
    int product = 1;
    List<Integer> integers = request.getRequest();
    for (Integer integer : integers) {
      product *= integer;
    }
    System.out.println("Product = " + product);
  }

}
