package com.psybergate.grad2021.jeefnds.ce1.implementation_2;

import com.psybergate.grad2021.jeefnds.ce1.standards.InvalidDateException;
import com.psybergate.grad2021.jeefnds.ce1.standards.Date;
import com.psybergate.grad2021.jeefnds.ce1.standards.DateFactory;

import java.util.Arrays;
import java.util.List;

public class DateImpl2 implements Date, DateFactory {

  /**
   * List of months ending with 31 days
   */
  private static final List<Integer> EXTENDED_MONTHS = Arrays.asList(1, 3, 5, 7, 8, 10, 12);

  /**
   * List of months ending with 30 days
   */
  private static final List<Integer> STANDARD_MONTHS = Arrays.asList(4, 6, 9, 11);

  private int day;

  private int month;

  private int year;

  private DateImpl2(int day, int month, int year) {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  public DateImpl2() {
  }

  @Override
  public Date createDate(int day, int month, int year) throws InvalidDateException {
    if (day > 31 || day < 1 || month > 12 || month < 1) {
      throw new InvalidDateException("Date invalid");
    }
    return new DateImpl2(day, month, year);
  }

  @Override
  public int getDay() {
    return day;
  }

  @Override
  public int getMonth() {
    return month;
  }

  @Override
  public int getYear() {
    return year;
  }

  @Override
  public boolean isLeapYear() {
    if (this.getYear() % 100 == 0 && this.getYear() % 400 == 0) {
      return true;
    }
    if (this.getYear() % 100 != 0 && this.getYear() % 4 == 0) {
      return true;
    }
    return false;
  }

  @Override
  public Date addDays(int numberOfDays) throws InvalidDateException {
    int day = this.getDay();
    int month = this.getMonth();
    int year = this.getYear();
    for (int count = 1; count <= numberOfDays; count++) {
      if (day == maxDaysOfMonth() && month < 12) {
        month += 1;
        day = 1;
        break;
      }
      if (day == maxDaysOfMonth() && month == 12) {
        year += 1;
        month = 1;
        day = 1;
        break;
      }
      day += 1;
    }
    return createDate(day, month, year);
  }

  public int maxDaysOfMonth() {
    if (this.isExtendedMonth()) {
      return 31;
    }
    if (this.isStandardMonth()) {
      return 30;
    }
    if (this.isLeapYear() && this.getMonth() == 2) {
      return 29;
    } else {
      return 28;
    }
  }

  /**
   * Standard month has 30 days
   *
   * @return true if is standard month
   */
  public boolean isStandardMonth() {
    return STANDARD_MONTHS.contains(this.getMonth());
  }

  /**
   * Extended month has 31 days
   *
   * @return true if is extended month
   */
  public boolean isExtendedMonth() {
    return EXTENDED_MONTHS.contains(this.getMonth());
  }

  @Override
  public String toString() {
    return day + "/" + month + "/" + year;
  }


}
