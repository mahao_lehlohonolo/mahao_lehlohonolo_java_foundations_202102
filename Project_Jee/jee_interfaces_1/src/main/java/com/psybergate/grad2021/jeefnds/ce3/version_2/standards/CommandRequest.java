package com.psybergate.grad2021.jeefnds.ce3.version_2.standards;

import java.util.List;

public interface CommandRequest {

  List<Integer> getRequest();

}
