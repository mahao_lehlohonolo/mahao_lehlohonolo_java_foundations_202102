package com.psybergate.grad2021.jeefnds.ce3.version_1.client;

import com.psybergate.grad2021.jeefnds.ce3.version_1.command.SumNumbers;
import com.psybergate.grad2021.jeefnds.ce3.version_1.implemenation.CommandEngine1;
import com.psybergate.grad2021.jeefnds.ce3.version_1.implemenation.CommandRequest1;

public class Client {

  public static void main(String[] args) {
    CommandRequest1 request = new SumNumbers(1, 2);
    CommandEngine1 commandEngine1 = new CommandEngine1(request);
    commandEngine1.processCommand();
  }

}
