package com.psybergate.grad2021.jeefnds.ce3.version_2.implementation;

import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.Command;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandEngine;
import com.psybergate.grad2021.jeefnds.ce3.version_2.standards.CommandRequest;

public class CommandEngineImpl implements CommandEngine {

  private Command command;

  private CommandRequest request;

  public CommandEngineImpl(Command command, CommandRequest request) {
    this.command = command;
    this.request = request;
  }

  @Override
  public void execute() {
    CommandResponseImpl commandResponse = new CommandResponseImpl(command, request);
    System.out.println("Executing...");
    commandResponse.getResponse();
  }

  @Override
  public Command getCommand() {
    return command;
  }

  @Override
  public CommandRequest getRequest() {
    return request;
  }

}
