<html>
<body>
<%@page import = "javax.servlet.ServletException, javax.servlet.annotation.WebServlet, javax.servlet.http.HttpServlet"%>
<%@page import = "javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.io.IOException"%>
<%@page import = "java.sql.Connection,java.sql.SQLException, java.sql.Statement, java.sql.ResultSet, java.time.LocalDate"%>
<%@page import = "com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.Customer"%>
<%@page import = "com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.DatabaseConnectionController"%>
<%@page import = "com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.DatabaseServicesController"%>
<%@page import = "com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.CustomerDatabaseController"%>
<%@page import = "com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.MyAnnotations.*"%>

<%
    Long customerNum = Long.valueOf(request.getParameter("custnum"));
    String name = request.getParameter("fname");
    String surname = request.getParameter("lname");
    LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dob"));
    Customer newCustomer = new Customer(customerNum, name, surname, dateOfBirth);
    DatabaseConnectionController databaseConnection = new DatabaseConnectionController();
    Connection connection = databaseConnection.dataBaseConnector("customerdb");
    DatabaseServicesController databaseController = new DatabaseServicesController();
    String sqlStatement = databaseController.insertCustomerToDB(newCustomer);
    Statement statement = connection.createStatement();
    try {
        Statement statement2 = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
        ResultSet resultSet = statement2.executeQuery("select exists(select true from " +
                  "servlets_customer_tbl where customer_num =" + customerNum +")");
        boolean first = resultSet.first();
        if(first == true){
%>
         <h1><%= "This customer number already exists in the database." %></h1>
<% }else{
    statement.executeUpdate(sqlStatement);
%>
    <h1><%= "New customer, " + name + " " + surname + "(CustomerNum: " + customerNum + "), added to database." %></h1>
<%  }
         } catch (SQLException throwable) {
           throw new RuntimeException(throwable);
           }
%>

</body>
</html>