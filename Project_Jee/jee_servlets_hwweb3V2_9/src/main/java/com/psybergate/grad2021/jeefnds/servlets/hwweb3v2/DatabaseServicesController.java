package com.psybergate.grad2021.jeefnds.servlets.hwweb3v2;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.MyAnnotations.DomainClass;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseServicesController {

  private static String createTableString = "CREATE TABLE ";

  public void createTable(Class cls) throws SQLException {
    Annotation[] classAnnotations = cls.getDeclaredAnnotations();
    DomainClass domainClass = (DomainClass) classAnnotations[0];
    String tableName = domainClass.name();
    Field[] fields = cls.getDeclaredFields();
    Connection connection = new DatabaseConnectionController().dataBaseConnector("customerdb");
    try {
      Statement statement = connection.createStatement();
      String sql = createTableString + tableName + createTableSQL(fields) + ";";
      statement.executeUpdate(sql);
      statement.close();
    } catch (Throwable throwable) {
      throw new RuntimeException(throwable);
    } finally {
      connection.close();
    }
  }

  private String createTableSQL(Field[] fields) {
    String sql = "(";
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      if (annotations[0].annotationType().getSimpleName().equals("DomainTransient")) {
        continue;
      }
      DomainAttribute domainAttribute = (DomainAttribute) annotations[0];
      sql += domainAttribute.name() + getFieldType(field) + primaryKey(field) + foreignKeyRef(field) + ",";
    }
    char[] array = sql.toCharArray();
    array[array.length - 1] = ')';
    sql = new String(array);
    return sql;
  }

  public String insertCustomerToDB(Customer customer) {
    Annotation[] classAnnotations = customer.getClass().getDeclaredAnnotations();
    DomainClass domainClass = (DomainClass) classAnnotations[0];
    String sql = "INSERT INTO " + domainClass.name() + " VALUES(" + customer.getCustomerNum() + ",'"
            + customer.getName() + "','" + customer.getSurname() + "','" + customer.getDateOfBirth() + "');";
    return sql;
  }

  private String foreignKeyRef(Field field) {
    String command = "";
    if (getFieldAnnotation(field).foreignKey() == true) {
      command = " References " + getFieldAnnotation(field).foreignKeyRef();
    }
    return command;
  }

  private String primaryKey(Field field) {
    String command = "";
    if (getFieldAnnotation(field).primaryKey() == true) {
      command = " Primary Key";
    }
    return command;
  }

  private String getFieldType(Field field) {
    String fieldType = "";
    if (getFieldAnnotation(field).type().equals("int")) {
      fieldType = " int";
    }
    if (getFieldAnnotation(field).type().equals("String")) {
      fieldType = " varchar";
    }
    return fieldType;
  }

  private DomainAttribute getFieldAnnotation(Field field) {
    Annotation[] annotations = field.getDeclaredAnnotations();
    DomainAttribute annotation = (DomainAttribute) annotations[0];
    return annotation;
  }

}
