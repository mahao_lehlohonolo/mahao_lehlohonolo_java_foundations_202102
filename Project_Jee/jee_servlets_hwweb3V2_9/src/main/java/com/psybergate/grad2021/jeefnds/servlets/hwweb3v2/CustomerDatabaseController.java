package com.psybergate.grad2021.jeefnds.servlets.hwweb3v2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
@Deprecated
public class CustomerDatabaseController {

  public String addCustomer(HttpServletRequest request, HttpServletResponse response) {
    Long customerNum = Long.valueOf(request.getParameter("customernum"));
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dateofbirth"));
    Customer newCustomer = new Customer(customerNum, name, surname, dateOfBirth);
    DatabaseConnectionController databaseConnectionController = new DatabaseConnectionController();
    Connection connection = databaseConnectionController.dataBaseConnector("customerdb");
    DatabaseServicesController databaseServicesController = new DatabaseServicesController();
    String sqlStatement = databaseServicesController.insertCustomerToDB(newCustomer);
    try {
      Statement statement = connection.createStatement();
      statement.executeUpdate(sqlStatement);
    } catch (SQLException throwable) {
      throw new RuntimeException(throwable);
    }
    return "New customer, " + name + " " + surname + "(CustomerNum: " + customerNum + ")" + ", " +
            "added to database.";
  }

}
