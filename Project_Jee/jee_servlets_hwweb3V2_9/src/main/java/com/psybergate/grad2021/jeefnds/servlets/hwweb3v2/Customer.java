package com.psybergate.grad2021.jeefnds.servlets.hwweb3v2;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3v2.MyAnnotations.DomainClass;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

@DomainClass(name = "servlets_customer_tbl")
public class Customer {

  @DomainAttribute(name = "customer_num", type = "int", primaryKey = true)
  private long customerNum;

  @DomainAttribute(name = "name")
  private String name;

  @DomainAttribute(name = "surname")
  private String surname;

  @DomainAttribute(name = "date_of_birth")
  private LocalDate dateOfBirth;

  public Customer(long customerNum, String name, String surname, LocalDate dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public Customer() {
  }

  public long getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public String addCustomer(HttpServletRequest request, HttpServletResponse response) {
    Long customerNum = Long.valueOf(request.getParameter("customernum"));
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dateofbirth"));
    Customer newCustomer = new Customer(customerNum, name, surname, dateOfBirth);
    DatabaseConnectionController databaseConnectionController = new DatabaseConnectionController();
    Connection connection = databaseConnectionController.dataBaseConnector("customerdb");
    DatabaseServicesController databaseServicesController = new DatabaseServicesController();
    String sqlStatement = databaseServicesController.insertCustomerToDB(newCustomer);
    try {
      Statement statement2 = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
              ResultSet.CONCUR_UPDATABLE);
      ResultSet resultSet = statement2.executeQuery("select exists(select true from " +
              "servlets_customer_tbl where customer_num =" + customerNum + ")");
      boolean first = resultSet.first();
      if (first = true) {
        return "This customer number already exists in database.";
      } else {
        Statement statement = connection.createStatement();
        statement.executeUpdate(sqlStatement);
        return "New customer, " + name + " " + surname + "(CustomerNum: " + customerNum + ")" +
                ", added to database.";
      }
    } catch (SQLException throwable) {
      throw new RuntimeException(throwable);
    }
  }

}
