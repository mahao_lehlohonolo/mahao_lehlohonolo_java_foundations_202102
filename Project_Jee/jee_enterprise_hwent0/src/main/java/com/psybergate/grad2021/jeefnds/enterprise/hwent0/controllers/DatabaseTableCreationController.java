package com.psybergate.grad2021.jeefnds.enterprise.hwent0.controllers;

import com.psybergate.grad2021.jeefnds.enterprise.hwent0.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.MyAnnotations.DomainClass;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;

public class DatabaseTableCreationController {

  private static String createTableString = "CREATE TABLE ";

  public void createTable(Class cls, String databaseName) throws SQLException {
    Annotation[] classAnnotations = cls.getDeclaredAnnotations();
    DomainClass domainClass = (DomainClass) classAnnotations[0];
    String tableName = domainClass.name();
    Field[] fields = cls.getDeclaredFields();
    Connection connection = new DatabaseConnectionController().dataBaseConnector(databaseName);
    try {
      Statement statement = connection.createStatement();
      String sql = createTableString + tableName + createTableSQL(fields) + ";";
      ResultSet resultSet = statement.executeQuery("select count(*) from " +
              "information_schema.tables where table_name = '" + tableName + "';");
      if (resultSet.next() == true) {
        if (resultSet.getInt("count") == 0) {
          System.out.println("Table: " + tableName + " ,was created in " + databaseName + ".");
          statement.executeUpdate(sql);
        } else {
          System.out.println("Table: " + tableName + " ,already exists in " + databaseName + ".");
        }
      }
      statement.close();
    } catch (Throwable throwable) {
      throw new RuntimeException(throwable);
    } finally {
      connection.close();
    }
  }

  private String createTableSQL(Field[] fields) {
    String sql = "(";
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      if (annotations[0].annotationType().getSimpleName().equals("DomainTransient")) {
        continue;
      }
      DomainAttribute domainAttribute = (DomainAttribute) annotations[0];
      sql += domainAttribute.name() + getFieldType(field) + primaryKey(field) + foreignKeyRef(field) + ",";
    }
    char[] array = sql.toCharArray();
    array[array.length - 1] = ')';
    sql = new String(array);
    return sql;
  }

  public String insertCustomerSQL(Customer customer) {
    Annotation[] classAnnotations = customer.getClass().getDeclaredAnnotations();
    DomainClass domainClass = (DomainClass) classAnnotations[0];
    String sql = "INSERT INTO " + domainClass.name() + " VALUES(" + customer.getCustomerNum() + ",'"
            + customer.getName() + "','" + customer.getSurname() + "','" + customer.getDateOfBirth() + "');";
    return sql;
  }

  private String foreignKeyRef(Field field) {
    String command = "";
    if (getFieldAnnotation(field).foreignKey() == true) {
      command = " References " + getFieldAnnotation(field).foreignKeyRef();
    }
    return command;
  }

  private String primaryKey(Field field) {
    String command = "";
    if (getFieldAnnotation(field).primaryKey() == true) {
      command = " Primary Key";
    }
    return command;
  }

  private String getFieldType(Field field) {
    String fieldType = "";
    if (getFieldAnnotation(field).type().equals("int")) {
      fieldType = " int";
    }
    if (getFieldAnnotation(field).type().equals("String")) {
      fieldType = " varchar";
    }
    return fieldType;
  }

  private DomainAttribute getFieldAnnotation(Field field) {
    Annotation[] annotations = field.getDeclaredAnnotations();
    DomainAttribute annotation = (DomainAttribute) annotations[0];
    return annotation;
  }

}
