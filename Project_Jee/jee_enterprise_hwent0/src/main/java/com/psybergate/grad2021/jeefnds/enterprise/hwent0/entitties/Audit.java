package com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties;

import com.psybergate.grad2021.jeefnds.enterprise.hwent0.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.MyAnnotations.DomainClass;

import java.time.LocalDate;

@DomainClass(name = "enterprise_hw0_audit_tbl")
public class Audit {

  @DomainAttribute(name = "date")
  private LocalDate date;

  @DomainAttribute(name = "task")
  private String task;

  @DomainAttribute(name = "id", type = "int", primaryKey = true)
  private int id;

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getTask() {
    return task;
  }

  public void setTask(String task) {
    this.task = task;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

}
