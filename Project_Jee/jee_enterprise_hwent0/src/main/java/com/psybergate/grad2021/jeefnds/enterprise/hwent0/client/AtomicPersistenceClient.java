package com.psybergate.grad2021.jeefnds.enterprise.hwent0.client;

import com.psybergate.grad2021.jeefnds.enterprise.hwent0.controllers.DatabaseConnectionController;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties.Customer;

import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;

public class AtomicPersistenceClient {

  public static void main(String[] args) {
    int id = 2;
    Connection connection = new DatabaseConnectionController().dataBaseConnector(
            "java_enterprise_db");

    Customer customer = new Customer();
    customer.setCustomerNum(2);
    customer.setName("Lehlohonolo");
    customer.setSurname("Mahao");
    customer.setDateOfBirth("1996/11/27");
    customer.setId(id);

    Audit audit = new Audit();
    audit.setDate(LocalDate.now());
    audit.setTask("New customer");
    audit.setId(id);
    try {
      connection.setAutoCommit(false);
      Statement statement = connection.createStatement();
      statement.execute(insertCustomerSql("enterprise_hw0_customer_tbl", customer));
      statement.execute(insertAuditSql("enterprise_hw0_audit_tbl", audit));
      connection.commit();
      connection.close();
    } catch (Throwable throwable) {
      throw new RuntimeException(throwable);
    }

  }

  public static String insertCustomerSql(String tableName, Customer customer) {
    String sql;
    sql = "INSERT INTO " + tableName + " VALUES(" + customer.getCustomerNum() + ",'" + customer.getName() + "','" +
            customer.getSurname() + "','" + customer.getDateOfBirth() + "'," + customer.getId() + ")";
    return sql;
  }

  public static String insertAuditSql(String tableName, Audit audit) {
    String sql;
    sql = "INSERT INTO " + tableName + " VALUES('" + audit.getDate() + "','" + audit.getTask() +
            "'," + audit.getId() + ")";
    return sql;
  }

}
