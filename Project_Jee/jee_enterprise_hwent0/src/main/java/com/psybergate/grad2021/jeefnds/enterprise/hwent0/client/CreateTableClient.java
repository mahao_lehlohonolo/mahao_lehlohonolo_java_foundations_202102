package com.psybergate.grad2021.jeefnds.enterprise.hwent0.client;

import com.psybergate.grad2021.jeefnds.enterprise.hwent0.controllers.DatabaseTableCreationController;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties.Customer;

public class CreateTableClient {

  public static void main(String[] args) {
    try {
      DatabaseTableCreationController dsc = new DatabaseTableCreationController();
      Class customerClass = Customer.class;
      Class auditClass = Audit.class;
      dsc.createTable(customerClass,"java_enterprise_db");
      dsc.createTable(auditClass,"java_enterprise_db");
    } catch (Throwable throwable) {
      throw new RuntimeException(throwable);
    }
  }

}
