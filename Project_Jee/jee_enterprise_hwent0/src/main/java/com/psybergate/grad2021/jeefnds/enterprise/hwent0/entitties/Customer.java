package com.psybergate.grad2021.jeefnds.enterprise.hwent0.entitties;

import com.psybergate.grad2021.jeefnds.enterprise.hwent0.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.jeefnds.enterprise.hwent0.MyAnnotations.DomainClass;

@DomainClass(name = "enterprise_hw0_customer_tbl")
public class Customer {

  @DomainAttribute(name = "customer_num", type = "int", primaryKey = true)
  private int customerNum;

  @DomainAttribute(name = "name")
  private String name;

  @DomainAttribute(name = "surname")
  private String surname;

  @DomainAttribute(name = "date_of_birth")
  private String dateOfBirth;

  @DomainAttribute(name = "id", type = "int")
  private int id;

  public int getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(int customerNum) {
    this.customerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

}
