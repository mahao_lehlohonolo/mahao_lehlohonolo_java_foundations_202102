package com.psybergate.grad2021.jeefnds.servlets.arch2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/dispatcher1/*")
public class DispatcherServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter out = resp.getWriter();
    String pathInfo = req.getPathInfo();
    //getPathInfo() does not work if the urlPattern for servlet mapping does not end with '/*'
    if (pathInfo.equals("/helloworld")) {
      String hello = new HelloWorldController().hello();
      out.println("<h1>" + hello + "<h1>");
    }
    if (pathInfo.equals("/getcurrentdate")) {
      String currentDate = new GetCurrentDateController().getCurrentDate();
      out.println("<h1> Current date: " + currentDate + "<h1>");
    }
  }

}
