package com.psybergate.grad2021.jeefnds.servlets.arch3.version_2;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Properties;
import java.util.StringTokenizer;

public class Test {

      private static Properties properties = new Properties();
  static {

      String filename = "resource.properties";
      InputStream inputStream = Test.class.getClassLoader().getResourceAsStream(filename);
    try {
      properties.load(inputStream);
    } catch (IOException ioException) {
      ioException.printStackTrace();
    }

  }

  public static void main(String[] args) {
    try{
      String property = properties.getProperty("goodbye");
      StringTokenizer stringTokenizer = new StringTokenizer(property,"#");
      String[] s = property.split("#");
      System.out.println(s[s.length - 1]);
      Enumeration<?> it = properties.propertyNames();
      for (Object o : properties.keySet()) {
        System.out.println(o);
      }
      Class cls = Class.forName(s[0]);
      System.out.println(cls.getSimpleName());
      Object cls2 = cls.newInstance();
      Method method = cls2.getClass().getMethod("goodbye", HttpServletRequest.class);
      System.out.println(property);

      System.out.println(method.getName());
      method.invoke(cls2,"hello");
//      String n = "asdfghjk";
//      System.out.println(n.substring(1));
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }

}
