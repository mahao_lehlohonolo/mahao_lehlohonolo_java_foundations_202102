package com.psybergate.grad2021.jeefnds.servlets.arch3.controller;

import java.time.LocalDate;

public class DateController {

  public String getCurrentDate() {
    return LocalDate.now() + "";
  }

  public String getTomorrowsDate() {
    LocalDate today = LocalDate.now();
    LocalDate tomorrow = today.plusDays(1);
    return tomorrow + "";
  }

}
