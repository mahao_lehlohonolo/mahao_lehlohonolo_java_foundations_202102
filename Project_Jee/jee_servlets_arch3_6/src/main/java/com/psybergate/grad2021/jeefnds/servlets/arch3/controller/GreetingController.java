package com.psybergate.grad2021.jeefnds.servlets.arch3.controller;

public class GreetingController {

  public String hello(String name) {
    return "Hello, " + name + "!";
  }

  public String goodbye(String name) {
    return "Goodbye, " + name + "!";
  }

}
