package com.psybergate.grad2021.jeefnds.servlets.arch3.framework;

import com.psybergate.grad2021.jeefnds.servlets.arch3.controller.DateController;
import com.psybergate.grad2021.jeefnds.servlets.arch3.controller.GreetingController;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/dispatcher2/*")
public class DispatcherServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

    PrintWriter out = resp.getWriter();

    String pathInfo = req.getPathInfo();

    if (pathInfo.equals("/helloworld")) {
      String hello = new GreetingController().hello(req.getParameter("name"));
      out.println("<h1>" + hello + "<h1>");
    }
    if (pathInfo.equals("/goodbye")) {
      String goodbye = new GreetingController().goodbye(req.getParameter("name"));
      out.println("<h1>" + goodbye + "<h1>");
    }
    if (pathInfo.equals("/getcurrentdate")) {
      String currentDate = new DateController().getCurrentDate();
      out.println("<h1> Current date: " + currentDate + "<h1>");
    }
    if (pathInfo.equals("/gettomorrowsdate")) {
      String tomorrowsDate = new DateController().getTomorrowsDate();
      out.println("<h1> Tomorrow's date: " + tomorrowsDate + "<h1>");
    }
  }

}
