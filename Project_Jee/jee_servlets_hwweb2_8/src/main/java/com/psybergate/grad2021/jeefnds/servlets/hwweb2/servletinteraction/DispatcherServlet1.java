package com.psybergate.grad2021.jeefnds.servlets.hwweb2.servletinteraction;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/dispatcher1")
public class DispatcherServlet1 extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    RequestDispatcher requestDispatcher = req.getRequestDispatcher("/httpinfoservlet");
    requestDispatcher.forward(req, resp);
  }

}
