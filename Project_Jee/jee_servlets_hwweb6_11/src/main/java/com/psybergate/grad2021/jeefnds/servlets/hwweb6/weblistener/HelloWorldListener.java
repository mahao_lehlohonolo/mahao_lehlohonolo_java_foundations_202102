package com.psybergate.grad2021.jeefnds.servlets.hwweb6.weblistener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Arrays;
import java.util.List;

@WebListener("/helloworld")
public class HelloWorldListener implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    System.out.println("HelloWorldListener initialized..");
    List<String> names = Arrays.asList("Nonny", "Mahao", "Tonic", "Noncedo", "Lehlohonolo", "John");
    ServletContext servletContext = sce.getServletContext();
    servletContext.setAttribute("names_collection", names);
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    System.out.println("HelloWorldListener instance destroyed..");
  }

}
