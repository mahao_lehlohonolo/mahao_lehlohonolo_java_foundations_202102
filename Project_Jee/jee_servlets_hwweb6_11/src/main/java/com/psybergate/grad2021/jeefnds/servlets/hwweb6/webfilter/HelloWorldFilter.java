package com.psybergate.grad2021.jeefnds.servlets.hwweb6.webfilter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebFilter(urlPatterns = "/helloworld")
public class HelloWorldFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    Filter.super.init(filterConfig);
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                       FilterChain filterChain) throws IOException, ServletException {
    ServletContext servletContext = servletRequest.getServletContext();
    List<String> names_collection = (List<String>) servletContext.getAttribute("names_collection");
    PrintWriter printWriter = servletResponse.getWriter();
    if (names_collection.contains("John")) {
      printWriter.println("The name 'John' is not allowed in initialized names collection!");
    } else {
      filterChain.doFilter(servletRequest, servletResponse);
    }
  }

  @Override
  public void destroy() {
    Filter.super.destroy();
  }

}
