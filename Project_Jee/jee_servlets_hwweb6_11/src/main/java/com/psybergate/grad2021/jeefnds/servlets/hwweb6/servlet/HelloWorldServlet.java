package com.psybergate.grad2021.jeefnds.servlets.hwweb6.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/helloworld")
public class HelloWorldServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    ServletContext servletContext = getServletContext();
    List<String> names_collection = (List<String>) servletContext.getAttribute("names_collection");
    resp.getWriter().println("Hello, " + names_collection.get((int) (Math.random() * names_collection.size())) + "!");
  }

}
