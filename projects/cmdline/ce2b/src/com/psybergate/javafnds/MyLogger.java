package com.psybergate.javafnds;

// import org.apache.log4j.Logger;


public class MyLogger {
	
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(MyLogger.class);
	
	public static void main(String[] args) {
		// Note : Cannot really use LOG as it requires some further setup - but not relevant to the exercise 
		// as we simply want to show that it the classes are being used.
		System.out.println("Yeah - i am logging");
	}

}
