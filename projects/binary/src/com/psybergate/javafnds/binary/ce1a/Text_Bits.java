package com.psybergate.javafnds.binary.ce1a;

import java.io.*;
import java.util.*;

import static java.lang.Integer.toHexString;

public class Text_Bits {
   public static void main(String[] args) {
      File text_file = new File("UserText.txt");
      try {
         Scanner FileText = new Scanner(text_file);
         String MyString = "";

         while (FileText.hasNextLine()) {
            MyString = FileText.nextLine();
            String HexString = "";
            for (int count = 0; count < MyString.length(); count++) {
               char letter = MyString.charAt(count);
               int ascii_num = letter;
               String HexOfLetter = Integer.toHexString(ascii_num);
               HexString = HexString + HexOfLetter + " ";
            }
            System.out.println(HexString);
         }
      } catch (FileNotFoundException e) {
         e.printStackTrace();
         System.out.println("Please create UserText.txt in the current working directory.");
      }
   }
}