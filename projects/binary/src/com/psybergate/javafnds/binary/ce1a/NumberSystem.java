package com.psybergate.javafnds.binary.ce1a;

public class NumberSystem {

  public static void main(String[] args) {
    //byte is 8 bit with 256 combinations
    byte byteMax = (byte) 0b1111111; //max
    System.out.println("byteMax = " + byteMax);
    byte byteMin = (byte) 0b10000000;//min
    System.out.println("byteMin = " + byteMin);

    //short is 16 bit with 65535 combinations
    short shortMax = (short) 0b111111111111111;//max
    System.out.println("shortMax = " + shortMax);
    short shortMin = (short) 0b1000000000000000;//min
    System.out.println("shortMin = " + shortMin);

    //int is 32 bit with 4294967296 combinations
    int intMax = 0b1111111111111111111111111111111; //max
    System.out.println("intMax = " + intMax);
    int intMin = 0b10000000000000000000000000000000;//min
    System.out.println("intMin = " + intMin);

    //long is 64 bit with 2^(64) combinations
    long longMax = 0b111111111111111111111111111111111111111111111111111111111111111L; //max
    System.out.println("longMax = " + longMax);
    long longMin = 0b1000000000000000000000000000000000000000000000000000000000000000L;//min
    System.out.println("longMin = " + longMin);

    char c = '\u1F60';
    System.out.println("c = " + c);
    String bear = "\ud83d\udc3b";
    System.out.println("bear = " + bear);
    System.out.println('\u263A');
  }

}
