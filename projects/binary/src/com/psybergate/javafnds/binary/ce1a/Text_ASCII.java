package com.psybergate.javafnds.binary.ce1a;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.File;

public class Text_ASCII {
   public static void main(String[] args) throws FileNotFoundException {
      String user_string = "The cat sat on the hat";
      String printed = "";
      for (int counter = 0; counter < user_string.length(); counter++) {
         char letter = user_string.charAt(counter);
         int int_value = letter;
         if (counter == user_string.length() - 1) {
            printed = printed + String.valueOf(int_value);
         } else {
            printed = printed + String.valueOf(int_value) + " ";
         }
      }
      PrintWriter textile = new PrintWriter(new File("OutputText.txt"));
      textile.write(printed);
      textile.flush();
      textile.close();
   }
}
