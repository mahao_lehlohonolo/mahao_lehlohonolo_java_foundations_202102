package com.psybergate.javafnds.binary;

public final class FinalModifier { //when the class has final we cannot extend it, that's all.
   static final int number = 10; //if the modifier was final then we would not be able the override it as shown in line 9 for myObject.
   //number can be accessed only by the objects created. It has to be assigned a literal if it has a final
   static final int somenumber1;
   static {
      somenumber1=10;
      System.out.println("Static initializer ran");
   }

   static final int myMethod(int number1){ //static method, uses the static variables initialized in the static initializer.
      return number1;
   }

   public static void main(String[] args) {
   FinalModifier myObject = new FinalModifier();
   FinalModifier myObject1 = new FinalModifier();
   System.out.println(myMethod(somenumber1));
      //myObject.number=15; //changes for myObject only does not change for every object created if the object variable is not declared final.
      System.out.println(myObject1.number); //number is the same as initialized in line 4.
      System.out.println(myObject.number); //number was changed to 15 for myObject.

   }

}
//class B extends FinalModifier{ //cannot inherit from a final Class.
//
//}
