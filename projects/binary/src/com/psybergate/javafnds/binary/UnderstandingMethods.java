package com.psybergate.javafnds.binary;

public class UnderstandingMethods {
   private static int number = 10; //static/class variable, belongs to the class.
//   static int number2 = 20; //static/class variable, belongs to the class.

   static { //runs only once, when the class is loaded
      System.out.println("Static initializer ran.");
      int number2 = 20; //static/class variable, belongs to the class.
      int number = 10; //static/class variable, belongs to the class.
      System.out.println(maxNum(number, number2));
   }

   String Name; //instance or object variable.
   int number3; //instance or object variable.
   int someNumber;

   {
      System.out.println("Instance Initializer called."); //called when an object is created
      someNumber = 200;
   }

   public UnderstandingMethods(String Name, int number3) { //a constructor(when you don't want to use setters for each instance variable), assigns values to the fields/instance variables when creating an object. You call the constructor when you create an object (see main)
      this.Name = Name;
      this.number3 = number3;
   }

   public static int getNumber() { // static variable called number is private in UnderstandingMethods and to access it you use a public getter in UnderstandingMethods and call the getter where you want it.
      return number;
   }

   public static int maxNum(int a, int b) { //static method, uses static/class variables as its parameters.
      int max;
      if (a < b) {
         max = b;
      } else {
         max = a;
      }
      return max;
   }

   public static void main(String[] args) {
      //System.out.println(maxNum(number, number2));
      //UnderstandingMethods MyInstance = new UnderstandingMethods(); //without a constructor
      // UnderstandingMethods MyInstance2 = new UnderstandingMethods(); //without a constructor
      UnderstandingMethods MyInstance2 = new UnderstandingMethods("Mahao", 24); //with the constructor
      UnderstandingMethods MyInstance = new UnderstandingMethods("Noncedo", 22); //with the constructor
//      MyInstance.setName("Mahao"); //without a constructor
//      MyInstance2.setName("Nonny"); //without a constructor
//      MyInstance.setNumber3(24); //without a constructor
//      MyInstance2.setNumber3(22); //without a constructor
      MyInstance.setSomeNumber();
      MyInstance2.setSomeNumber();

      MyInstance.printer(); //accessing the instance method that does not return anything
      MyInstance2.printer(); //accessing the instance method that does not return anything

      System.out.println(MyInstance.product()); //accessing the instance method that returns something
      System.out.println(MyInstance2.product()); //accessing the instance method that returns something
      UnderstandingModifiers.printName(); //to show that printName() can be accessed since the classes are in the same package
   }

   public int getNumber3() { //to isolate the field or instance or object variable we want for some purpose. Mostly used when we have a constructor. See main on how to call it.
      return number3;
   }

   public void setNumber3(int number3) {  //a setter(when you dont want to use constructors), sets the int value of number3 for the instance variables for each object created.
      this.number3 = number3;
   }

   public String getName() { //to isolate the field or instance or object variable we want for some purpose. Mostly used when we have a constructor. See main on how to call it.
      return Name;
   }

   public void setName(String Name) {     //a setter(when you dont want to use constructors), sets the string value of Name for the instance variables for each object created.
      this.Name = Name;
   }

   public void setSomeNumber() {
      this.someNumber = someNumber;
   }

   public int product() { //instance method that returns a result.
      int a = 10; //local variable of the method. Accessed only when the method is called.
      return a * number3;
   }

   public void printer() { //instance method that does not return anything.
      System.out.println("Name is " + Name);
      System.out.println("Number is " + number3);
      System.out.println("someNumber is " + someNumber);
   }
}