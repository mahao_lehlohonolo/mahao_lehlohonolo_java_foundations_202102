package com.psybergate.javafnds.binary.hw1a;
import java.util.*;
import java.io.*;
import java.lang.*;

public class ClassFileHex {
   public static void main(String[] args) {
      File text_file = new File("HelloWorld.java");
      try {
         Scanner FileText = new Scanner(text_file);
         String MyString = "";

         while(FileText.hasNextLine()){
            MyString = FileText.nextLine();
            String HexString = "";
            for(int count=0; count<MyString.length(); count++){
               char letter = MyString.charAt(count);
               int ascii_num = letter;
               String HexOfLetter = Integer.toHexString(ascii_num);

               HexString = HexString + HexOfLetter + " ";
            }
            System.out.println(HexString);
            //System.out.println(MyString);
         }
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }
   }
}
