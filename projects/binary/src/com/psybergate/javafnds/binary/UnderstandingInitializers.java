package com.psybergate.javafnds.binary;

public class UnderstandingInitializers {
   static {
      System.out.println("I am being called"); //runs only once no matter how many objects you create.
      int someNum = 25; //static variable
      int someNum1 = 20; //static variable
      System.out.println(maxNum(someNum, someNum1));
      String someString = "MyString";
      char someChar = 'A';
   }
   static int someMeth(int a){
      return a;
   }

   int someNum;
   int someNum1;

   {
      System.out.println("I will run as many times as objects are created.");
      someNum = 25; //instance variable
      someNum1 = 20; //instance variable
   }


   public static int maxNum(int a, int b) {    //static method, uses static/class variables as its parameters.
      int max;
      if (a < b) {
         max = b;
      } else {
         max = a;
      }
      return max;
   }

   public static void main(String[] args) {
      UnderstandingInitializers obj = new UnderstandingInitializers();
      UnderstandingInitializers obj1 = new UnderstandingInitializers();
      UnderstandingInitializers obj2 = new UnderstandingInitializers();
   }
}
