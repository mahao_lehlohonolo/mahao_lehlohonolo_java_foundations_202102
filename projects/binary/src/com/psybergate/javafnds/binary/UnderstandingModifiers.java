package com.psybergate.javafnds.binary;

public class UnderstandingModifiers {
   protected static void printName() { //if it is protected, only AnotherClass will be able to access the method because it extends this class.
      System.out.println("Mahao");
   }

   public static void main(String[] args) {
      printName();
   }
}