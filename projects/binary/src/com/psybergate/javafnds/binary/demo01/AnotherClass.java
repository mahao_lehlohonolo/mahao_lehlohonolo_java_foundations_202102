package com.psybergate.javafnds.binary.demo01;

import com.psybergate.javafnds.binary.UnderstandingMethods;
import com.psybergate.javafnds.binary.UnderstandingModifiers;

public class AnotherClass extends UnderstandingModifiers { ///inherits the properties of UnderstandingModifiers
   public static void main(String[] args) {
      UnderstandingModifiers.printName(); //To show that calling a protected method in UnderstandingModifiers is through this sub-class
      System.out.println(UnderstandingMethods.getNumber());  //number is private in UnderstandingMethods and to access it you use a public getter in UnderstandingMethods and call the getter where you want it.
   }
}