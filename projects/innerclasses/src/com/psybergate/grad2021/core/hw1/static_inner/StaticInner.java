package com.psybergate.grad2021.core.hw1.static_inner;

public class StaticInner {

  public static void main(String[] args) {
    new InnerClass().print();
    someMethod(new InnerClass() {

      @Override
      //the print() method in MyInnerClass returned void and someMethod() was expecting
      // MyInnerClass object to be passed in so thus the return type of the print method was
      // changed to MyInnerClass
      public InnerClass print() {
        System.out.println("Inside the MyInnerClass but overridden");
        return null;
      }
    }.print());
  }

  public static void someMethod(InnerClass innerClass) {
  }

  public static class InnerClass {

    public InnerClass print() {
      System.out.println("Inside the MyInnerClass");
      return null;
    }

  }

}
