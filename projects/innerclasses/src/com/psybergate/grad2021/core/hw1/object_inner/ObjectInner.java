package com.psybergate.grad2021.core.hw1.object_inner;


public class ObjectInner {

  public static void main(String[] args) {
    //the lines below show how to access the InnerClass using object semantics
    // and how to create an object of InnerClass
    ObjectInner objectInner = new ObjectInner();
    InnerClass innerClass = objectInner.new InnerClass();
    InnerClass innerClass1 = new ObjectInner().new InnerClass();

    innerClass1.print();
    Class cls = InnerClass.class;
    System.out.println(cls.getSuperclass().getSimpleName());
    Class cls1 = ObjectInner.class;

    Class[] classes = cls1.getClasses();//the get class method returns all public classes and
    // interfaces in the class you are in
    System.out.println(classes.length);
    for (Class aClass : classes) {
      System.out.println(aClass.getSimpleName());
    }

  }

  public class InnerClass {

    public void print() {
      System.out.println("I am here");
    }

  }

}
