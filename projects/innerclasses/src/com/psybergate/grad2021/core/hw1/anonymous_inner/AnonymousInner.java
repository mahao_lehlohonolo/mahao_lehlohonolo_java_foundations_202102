package com.psybergate.grad2021.core.hw1.anonymous_inner;

public class AnonymousInner {

  public static void main(String[] args) {
    //the object passed in the method is an anonymous class
    someMethod(new InnerClass() {

      @Override
      public InnerClass print() {
        System.out.println("Inside the MyInnerClass but overridden");
        return null;
      }
    }.print());

    //this is how the anonymous class actually looks
    class Anonymous1 extends InnerClass{
      @Override
      public InnerClass print() {
        System.out.println("Inside the child Anon");
        return null;
      }
    }
    //also know that the parentheses after Anonymous1 show that we're using the default
    // constructor in the class
    new Anonymous1().print();

    class Anonymous2 implements InnerInterface{

      @Override
      public void print() {
        System.out.println("In Annon2 print");
      }

    }

    new Anonymous2().print();
    //line 37 can be written using lambdas since InnerInterface is a functional interface(i.e it
    // has one method in the class)
    someMethod2(new InnerInterface() {

      @Override
      public void print() {
        InnerInterface.super.print();
      }
    });
    class Anonymous3 extends InnerAbstract{

      @Override
      public void print() {
        System.out.println("Inside Annon3");
      }

    }
    new Anonymous3().print();
  }

  public static void someMethod(InnerClass innerClass) {
  }
  public static void someMethod2(InnerInterface innerInterface) {
    innerInterface.print();
  }


}

class InnerClass {

  public InnerClass print() {
    System.out.println("Inside the MyInnerClass");
    return null;
  }

}

interface InnerInterface{
  public default void print(){
    System.out.println("In default");
  }
}

abstract class InnerAbstract{
public abstract void print();
}
