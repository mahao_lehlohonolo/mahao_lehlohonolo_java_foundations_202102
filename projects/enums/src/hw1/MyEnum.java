package hw1;

public enum MyEnum implements MyInterface {
  /**
   * It can implement an interface as shown, the method in the interface is overridden here. Based
   * on this enum, there are only seven enum constants, meaning you only have 7 objects of these
   * constants in the memory heap(JVM). If there is an abstract method declared in the enum(or the
   * enum implements an interface with a method), the compiler forces you to implement it in the
   * enum constants declarations as shown below.
   */

  MONDAY(1) {
    @Override
    public String howYouFeel() {
      return "I hate this day.";
    }
  },
  TUESDAY(2) {
    @Override
    public String howYouFeel() {
      return "I hate this one too.";
    }
  }, WEDNESDAY(3) {
    @Override
    public String howYouFeel() {
      return "Just okay with this one.";
    }
  }, THURSDAY(4) {
    @Override
    public String howYouFeel() {
      return "I like this one.";
    }
  }, FRIDAY(5) {
    @Override
    public String howYouFeel() {
      return "Oh yes!!!!";
    }
  }, SATURDAY(6) {
    @Override
    public String howYouFeel() {
      return "Another Oh yess!!!";
    }
  }, SUNDAY(7) {
    @Override
    public String howYouFeel() {
      return "mxm";
    }
  };

  private int num;

  MyEnum(int num) {
    this.num = num;
  }

  MyEnum() {
  }

  public int getNum() {
    return num;
  }


  public int method() {
    return getNum();
  }

//  abstract String howYouFeel(); //applied an interface to test implementation differences.
}
