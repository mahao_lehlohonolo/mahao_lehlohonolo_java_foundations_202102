package hw1;

public class MyClass {

  private static int MONDAY = 1;

  private static int TUESDAY = 2;

  private static int WEDNESDAY = 3;

  private static int THURSDAY = 4;

  private static int FRIDAY = 5;

  private static int SATURDAY = 6;

  private static int SUNDAY = 7;

  //these static variables above can be represented using an enum.

  public static void main(String[] args) {
    MyEnum day1 = MyEnum.TUESDAY;
    MyEnum day2 = MyEnum.TUESDAY;//seems like we're creating a new MyEnum type object reference
    // that points to the same object as 'day1'. Cannot use the 'new' keyword
    System.out.println(day1.howYouFeel());
    System.out.println(day1.name());//name() method returns the name of the enum constant created.
    System.out.println(day1.compareTo(MyEnum.FRIDAY));
    System.out.println(day1.getDeclaringClass());
    System.out.println(day1.equals(day2));//they are pointing to the same object in the heap.
    System.out.println(day1 == day2); //same memory address.

    MyEnum day3 = MyEnum.SUNDAY;

    int i = 5;
    switch (day3) {
      case MONDAY:
        System.out.println("Tuesday");
        break;
      /**
       * in some cases(unlike this one), you do not need breaks.
       * But its recommended that you always put them.
       */
      case TUESDAY:
        System.out.println("Monday");
        break;
      case WEDNESDAY:
        System.out.println("Wednesday");
        break;
      case THURSDAY:
        System.out.println("Thursday");
        break;
      case FRIDAY:
        System.out.println("Friday");
        break;
      case SATURDAY:
        System.out.println("Saturday");
        break;
      case SUNDAY:
        day3.howYouFeel();
        /**
         * To call a method in a switch case, you can put 'return day3.howYouFeel()'. The main
         * method will have to return a string but that's not allowed. So it will work in other
         * methods except the main.
         *
         * Or you have to put 'System.out.println(day3.howYouFeel())'
         */
        System.out.println("Sunday");
        break;
      default:
        System.out.println("In default case.");
    }

    for(MyEnum en : MyEnum.values()){
      System.out.println(en);
      System.out.println(en.howYouFeel());
    }
  }

}
