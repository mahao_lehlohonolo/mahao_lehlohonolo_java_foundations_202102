package com.psybergate.grad2021.core.oopart2.overriding.demo1a;

public class Triangle extends Shape {

  private int base;

  private int height;

  public Triangle(int side1, int side2) {
    super(side1, side2);
    this.base = side1;
    this.base = side2;
  }

  @Override
  public int getArea() {
    return (int) (0.5 * base * height);
  }

}
