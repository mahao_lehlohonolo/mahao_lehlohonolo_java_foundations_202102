package com.psybergate.grad2021.core.oopart2.hw5a_1;

import java.time.LocalDate;

public class Customer {

  private String name;

  private String surname;

  private String customerID;

  private boolean isLocal;

  private boolean isInternational;

  private int yearLogged = LocalDate.now().getYear();

  public Customer(String name, String surname, String customerID, boolean isLocal,
                  boolean isInternational) {
    this.name = name;
    this.surname = surname;
    this.customerID = customerID;
    this.isLocal = isLocal;
    this.isInternational = isInternational;
    this.yearLogged = yearLogged;
    validateCustomerType();
  }

  public void validateCustomerType() {
    if (isLocal() == true && isInternational() == true) {
      throw new RuntimeException("Customer can either be Local or International.");
    }
    if (isLocal() == false && isInternational() == false) {
      throw new RuntimeException("Customer can either be Local or International.");
    }
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getCustomerID() {
    return customerID;
  }

  public boolean isLocal() {
    return isLocal;
  }

  public boolean isInternational() {
    return isInternational;
  }

  public int getYearLogged() {
    return yearLogged;
  }

  public int yearsAsCustomer() {
    return getYearLogged() - LocalDate.now().getYear();
  }


}
