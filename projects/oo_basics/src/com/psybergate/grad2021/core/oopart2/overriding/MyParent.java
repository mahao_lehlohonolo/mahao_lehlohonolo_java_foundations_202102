package com.psybergate.grad2021.core.oopart2.overriding;

public class MyParent {

  private static final int MY_NUM = 100;

  private int someNum;

  public MyParent(int someNum) {
    this.someNum = someNum;
  }

  //cannot override a static method
  public static int product(Integer num) {
    return num * MY_NUM;
  }

  /*The scope of the print() is public, for the subclass inheriting and overriding this method,
  the scope must be the same or broader. It cannot be inherited or overridden if the scope is
  private.*/

  /*The return type must be the same for the method in the parent and the child class*/
  public void print() {
    System.out.println("");
  }
  /*any code after the throw is not reachable*/
  // System.out.println("I am the parent.");


}
