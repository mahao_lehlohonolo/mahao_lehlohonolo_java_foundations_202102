package com.psybergate.grad2021.core.oopart2.hw3a;

public class Company extends Customer {

  private String companyRegNum;

  private String companyName;

  public Company(String customerNum, String items, int quantity, int pricePerUnit,
                         String companyRegNum, String companyName) {
    super(customerNum, items, quantity, pricePerUnit);
    this.companyRegNum = companyRegNum;
    this.companyName = companyName;
  }

  public String getName() {
    return companyName;
  }

  public String getCompanyRegNum() {
    return companyRegNum;
  }



}
