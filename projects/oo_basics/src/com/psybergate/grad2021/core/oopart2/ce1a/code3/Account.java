package com.psybergate.grad2021.core.oopart2.ce1a.code3;

public class Account {

  private String customerName;

  private String customerSurname;

  public Account(String customerName, String customerSurname) {
    this.customerName = customerName;
    this.customerSurname = customerSurname;
    System.out.println(customerName);
    System.out.println(customerSurname);
  }

}
