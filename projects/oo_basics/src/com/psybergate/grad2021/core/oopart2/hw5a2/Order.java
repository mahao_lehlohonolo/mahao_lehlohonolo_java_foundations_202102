package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Order {

  private static final double IMPORT_DUTY;

  private static final int DISCOUNT_MARGIN1;

  private static final int DISCOUNT_MARGIN2;

  private static final double TOTAL_DISCOUNT_1;

  private static final double TOTAL_DISCOUNT_2;

  protected static List<Order> orderList = new ArrayList<>();

  private static double YEARS_DISCOUNT_1;

  private static double YEARS_DISCOUNT_2;

  private static double YEARS_DISCOUNT_3;

  static {
    IMPORT_DUTY = 0.045;
    DISCOUNT_MARGIN1 = 500000;
    DISCOUNT_MARGIN2 = 1000000;
    TOTAL_DISCOUNT_1 = 0.05;
    TOTAL_DISCOUNT_2 = 0.1;
    YEARS_DISCOUNT_1 = 0;
    YEARS_DISCOUNT_2 = 0.075;
    YEARS_DISCOUNT_3 = 0.125;
  }

  DecimalFormat formatter = new DecimalFormat("###.##");

  private String orderNum;

  private String orderItem;

  private double orderPricePerUnit;

  private int orderQuantity;

  private int numberOfYearsAsClient;

  public Order(String orderNum, String orderItem, double orderPricePerUnit,
               int orderQuantity, int numberOfYearsAsClient) {
    this(orderNum, orderItem, orderPricePerUnit, orderQuantity);
    this.numberOfYearsAsClient = numberOfYearsAsClient;
  }

  public Order(String orderNum, String orderItem, double orderPricePerUnit,
               int orderQuantity) {
    this.orderNum = orderNum;
    this.orderItem = orderItem;
    this.orderPricePerUnit = orderPricePerUnit;
    this.orderQuantity = orderQuantity;
  }

  public int getNumberOfYearsAsClient() {
    return numberOfYearsAsClient;
  }

  public String getName() {
    return "";
  }

  public String customerOrderTotalPrice(Order object) {
    double total = 0;
    for (Order order : orderList) {
      if (order.getName() == object.getName()) {
        total += order.orderTotalPrice();
      }
    }
    if (object instanceof Local == true) {
      if (this.numberOfYearsAsClient > 2 && this.numberOfYearsAsClient <= 5) {
        total = total * (1 - YEARS_DISCOUNT_2);
      }
      if (this.numberOfYearsAsClient > 5) {
        total = total * (1 - YEARS_DISCOUNT_3);
      }
    }

    if (object instanceof International == true) {
      if (total > DISCOUNT_MARGIN1 && total <= DISCOUNT_MARGIN2) {
        total = total * (1 - TOTAL_DISCOUNT_1);
      }
      if (total > DISCOUNT_MARGIN2) {
        total = total * (1 - TOTAL_DISCOUNT_2);
      }
      total = total * (IMPORT_DUTY + 1);
    }
    return "Total items price for " + object.getName() + " is " + formatter.format(total);
  }

  public String getOrderNum() {
    return orderNum;
  }

  public String getOrderItem() {
    return orderItem;
  }

  public double getOrderPricePerUnit() {
    return orderPricePerUnit;
  }

  public int getOrderQuantity() {
    return orderQuantity;
  }

  public double orderTotalPrice() {
    return getOrderQuantity() * getOrderPricePerUnit();
  }

  public String print(Order customer) {
    System.out.println("Customer name: " + customer.getName());
    System.out.println();

    for (Order order : orderList) {
      if (order.getName() == customer.getName()) {
        System.out.println("Order number: " + order.getOrderNum() + "\nItem: " + order.getOrderItem() +
                "\nPrice Per Unit: " + order.getOrderPricePerUnit() + "\nQuantity: " + order.getOrderQuantity() +
                "\nTotal price: " + order.orderTotalPrice());
        System.out.println();
      }
    }
    return "";
  }

}
