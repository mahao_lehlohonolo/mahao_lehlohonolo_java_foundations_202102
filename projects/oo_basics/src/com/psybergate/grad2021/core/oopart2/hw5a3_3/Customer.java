package com.psybergate.grad2021.core.oopart2.hw5a3_3;

import java.time.LocalDate;

public class Customer {

  private static String LOCAL_CUSTOMER = "Local";

  private static String INTERNATIONAL_CUSTOMER = "International";

  private String customerID;

  private String name;

  private String surname;

  private String customerType; //International or Local

  private int yearLogged = LocalDate.now().getYear();

  public Customer(String name, String surname, String customerID, String customerType) {
    validateCustomerType();
    this.name = name;
    this.surname = surname;
    this.customerID = customerID;
    this.customerType = customerType;
    this.yearLogged = yearLogged;
  }

  public String getCustomerType() {
    return customerType;
  }

  public static String getLOCAL_CUSTOMER() {
    return LOCAL_CUSTOMER;
  }

  public static String getINTERNATIONAL_CUSTOMER() {
    return INTERNATIONAL_CUSTOMER;
  }

  public void validateCustomerType() {
    if ( !getCustomerType().equalsIgnoreCase(getLOCAL_CUSTOMER()) && !getCustomerType().equalsIgnoreCase(getINTERNATIONAL_CUSTOMER())) {
      throw new RuntimeException(getName() + " " + getSurname() +" can either be Local or" +
              " International.");
    }
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getCustomerID() {
    return customerID;
  }

  public int getYearLogged() {
    return yearLogged;
  }

  public int yearsAsCustomer() {
    return getYearLogged() - LocalDate.now().getYear();
  }

}

