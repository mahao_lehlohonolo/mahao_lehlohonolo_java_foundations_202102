package com.psybergate.grad2021.core.oopart2.hw5a2_1;

public class InternationalOrder extends Order {

  public InternationalOrder(Customer customer, String orderNum, Item item, int quantity) {
    super(customer, orderNum, item, quantity);

    if (customer.isInternational() != true) {
      throw new RuntimeException("Customer must be international for international Orders");
    }
  }

}