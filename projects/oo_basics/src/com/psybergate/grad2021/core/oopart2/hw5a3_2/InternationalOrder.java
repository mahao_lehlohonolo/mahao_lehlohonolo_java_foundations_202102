package com.psybergate.grad2021.core.oopart2.hw5a3_2;

public class InternationalOrder extends Order {

  public InternationalOrder(Customer customer, String orderNum, Item item, int quantity,
                            boolean yearsBasedDiscount, boolean totalValueBasedDiscount) {
    super(customer, orderNum, item, quantity, yearsBasedDiscount, totalValueBasedDiscount);

    if (customer.isInternational() != true) {
      throw new RuntimeException("Customer must be international for international Orders");
    }
  }

}