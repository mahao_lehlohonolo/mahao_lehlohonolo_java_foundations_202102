package com.psybergate.grad2021.core.oopart2.ce1a.code3;

public class CurrentAccount extends Account {

  private String accNum;

  private double accBal;

  public CurrentAccount(String customerName, String customerSurname, String accNum, double accBal) {
    super(customerName, customerSurname);
    this.accNum = accNum;
    this.accBal = accBal;
  }

//  public Account(String customerName, String customerSurname) {
//    this.customerName = customerName;
//    this.customerSurname = customerSurname;
//    System.out.println(customerName);
//    System.out.println(customerSurname);
//  }

  //For code3: when I try to inherit the parent constructor, it forces me to convert it to a void
  // method. Thus showing only methods can be inherited not constructors.




}
