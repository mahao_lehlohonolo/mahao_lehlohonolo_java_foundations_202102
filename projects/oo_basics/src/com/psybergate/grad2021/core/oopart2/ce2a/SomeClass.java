package com.psybergate.grad2021.core.oopart2.ce2a;

public class SomeClass {

  private int someNum1;


  public SomeClass(int someNum1) {
    this.someNum1 = someNum1;
  }

  public static void swap(SomeClass a, SomeClass b) {
    SomeClass c = a;
    a = b;
    b = c;
  }

  public static void main(String[] args) {
    SomeClass a = new SomeClass(2);
    SomeClass b = new SomeClass(4);
    SomeClass c = a;
    c.setSomeNum1(3);
    System.out.println(c);
    System.out.println(a);
    c = new SomeClass(5);
    System.out.println(c);
    System.out.println(a);
    //print out shows that object references are passed by value, objects are not passed around
    // in java.

//    System.out.println("Before swap: " + "a.getNum1 = " + a.someNum1 + " ,b.getSomeNum1 = " + b.someNum1);
//    swap(a, b);
//    System.out.println("After swap: " + "a.getNum1 = " + a.someNum1 + " ,b.getSomeNum1 = " + b.someNum1);
  }


  public int getSomeNum1() {
    return someNum1;
  }

  public void setSomeNum1(int someNum1) {
    this.someNum1 = someNum1;
  }

  @Override
  public String toString() {
    return "SomeClass{" +
            "someNum1=" + someNum1 +
            '}';
  }

}
