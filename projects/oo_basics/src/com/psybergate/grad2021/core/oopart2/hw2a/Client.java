package com.psybergate.grad2021.core.oopart2.hw2a;

public class Client {

    public static void equivalenceTest(Rectangle obj1, Rectangle obj2){
      if (obj1.getLength() == obj2.getLength() && obj1.getWidth() == obj1.getWidth()) {
        System.out.println("Objects are equivalent");
      }else{
        System.out.println("Objects are not equivalent");
      }
    }

  public static void main(String[] args) {
    Rectangle rect1 = new Rectangle(150, 200);
    Rectangle rect2 = new Rectangle(150, 200);
    Rectangle rect3 = rect2;

    //identity test, focuses on the objects.
    System.out.println("rect2.equals(rect3) = " + rect2.equals(rect3));
    System.out.println("rect1.equals(rect2) = " + rect1.equals(rect2));

    //equivalence test, focuses on the states of the objects.
    //if objects are identical, then that makes them equivalent.
    equivalenceTest(rect1,rect2);
    equivalenceTest(rect3,rect2);

  }

}
