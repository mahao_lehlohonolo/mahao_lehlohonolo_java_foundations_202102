package com.psybergate.grad2021.core.oopart2.hw5a3_3;

public class DiscountPolicy {

  private static double YEARS_BASED_DISCOUNT_RATE1;

  private static double YEARS_BASED_DISCOUNT_RATE2;

  private static int YEARS_BASED_DISCOUNT_MARGIN1;

  private static int YEARS_BASED_DISCOUNT_MARGIN2;

  private static int TOTAL_BASED_DISCOUNT_MARGIN1;

  private static int TOTAL_BASED_DISCOUNT_MARGIN2;

  private static double TOTAL_BASED_DISCOUNT_RATE1;

  private static double TOTAL_BASED_DISCOUNT_RATE2;

  private static double IMPORT_DUTY_RATE;

  static {
    IMPORT_DUTY_RATE = 0.045;
    YEARS_BASED_DISCOUNT_RATE1 = 0.075;
    YEARS_BASED_DISCOUNT_RATE2 = 0.125;
    TOTAL_BASED_DISCOUNT_RATE1 = 0.05;
    TOTAL_BASED_DISCOUNT_RATE2 = 0.1;
    YEARS_BASED_DISCOUNT_MARGIN1 = 2;
    YEARS_BASED_DISCOUNT_MARGIN2 = 5;
    TOTAL_BASED_DISCOUNT_MARGIN1 = 500_000;
    TOTAL_BASED_DISCOUNT_MARGIN2 = 1_000_000;
  }

//  public static double calculateDiscountedTotal(double total){
//
//    if (order.isTotalValueBasedDiscount() == true) {
//      if (total > getTotalBasedDiscountMargin1() && total <= getTotalBasedDiscountMargin2()) {
//        total = total * (1 - getTotalBasedDiscountRate1());
//      }
//      if (total > getTotalBasedDiscountMargin2()) {
//        total = total * (1 - getTotalBasedDiscountRate2());
//      }
//    }
//    if (order.isYearsBasedDiscount() == true) {
//      int numOfYearsASClient = customer.yearsAsCustomer();
//      if (numOfYearsASClient > getYearsBasedDiscountMargin1() && numOfYearsASClient <=
//              getYearsBasedDiscountMargin2()) {
//        total = total * (1 - getYearsBasedDiscountRate1());
//      }
//      if (numOfYearsASClient > getYearsBasedDiscountMargin2()) {
//        total = total * (1 - getYearsBasedDiscountRate2());
//      }
//    }
//
//
//  }
  public static double getYearsBasedDiscountRate1() {
    return YEARS_BASED_DISCOUNT_RATE1;
  }

  public static double getYearsBasedDiscountRate2() {
    return YEARS_BASED_DISCOUNT_RATE2;
  }

  public static int getYearsBasedDiscountMargin1() {
    return YEARS_BASED_DISCOUNT_MARGIN1;
  }

  public static int getYearsBasedDiscountMargin2() {
    return YEARS_BASED_DISCOUNT_MARGIN2;
  }

  public static int getTotalBasedDiscountMargin1() {
    return TOTAL_BASED_DISCOUNT_MARGIN1;
  }

  public static int getTotalBasedDiscountMargin2() {
    return TOTAL_BASED_DISCOUNT_MARGIN2;
  }

  public static double getTotalBasedDiscountRate1() {
    return TOTAL_BASED_DISCOUNT_RATE1;
  }

  public static double getTotalBasedDiscountRate2() {
    return TOTAL_BASED_DISCOUNT_RATE2;
  }

  public static double getImportDutyRate() {
    return IMPORT_DUTY_RATE;
  }

}
