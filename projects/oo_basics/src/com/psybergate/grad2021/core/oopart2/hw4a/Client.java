package com.psybergate.grad2021.core.oopart2.hw4a;

public class Client {

  public static void main(String[] args) {

    MyChild c1 = new MyChild();
    MyChild p1 = null;
    p1.someMethod();
    c1.someMethod(); //runs the parent method not the MyChild method, so it is not invoked
    // polymorphically. If the method was non static then this would run the child's method.
  }

}
