package com.psybergate.grad2021.core.oopart2.hw5a;

import java.util.ArrayList;
import java.util.List;

public class OrderSystem {

  List<Customer> orderList = new ArrayList<>();

  private String orderNum;

  private String orderItem;

  private double orderPricePerUnit;

  private int orderQuantity;

  public OrderSystem(String orderNum, String orderItem, double orderPricePerUnit,
                     int orderQuantity) {
    this.orderNum = orderNum;
    this.orderItem = orderItem;
    this.orderPricePerUnit = orderPricePerUnit;
    this.orderQuantity = orderQuantity;
  }

  public String getCustomerName() {
    return "";
  }

  public String getOrderNum() {
    return orderNum;
  }

  public String getOrderItem() {
    return orderItem;
  }

  public double getOrderPricePerUnit() {
    return orderPricePerUnit;
  }

  public int getOrderQuantity() {
    return orderQuantity;
  }

  public double orderTotalPrice() {
    return getOrderQuantity() * getOrderPricePerUnit();
  }

  public String print() {
    return "Customer Name: "+getCustomerName()+"\nOrder number: " + getOrderNum() + "\nItem: " + getOrderItem() +
            "\nQuantity: " + getOrderQuantity() + "\nTotal price: " + orderTotalPrice();
  }

}
