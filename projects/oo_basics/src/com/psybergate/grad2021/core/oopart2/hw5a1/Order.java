package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected static List<Order> orderList = new ArrayList<>();

  DecimalFormat formatter = new DecimalFormat("###.##");

  private String orderNum;

  private String orderItem;

  private double orderPricePerUnit;

  private int orderQuantity;

  public Order(String orderNum, String orderItem, double orderPricePerUnit,
               int orderQuantity) {
    this.orderNum = orderNum;
    this.orderItem = orderItem;
    this.orderPricePerUnit = orderPricePerUnit;
    this.orderQuantity = orderQuantity;
  }

  public String getName() {
    return "";
  }

  public String customerOrderTotalPrice(Order object) {
    double total = 0;
    for (Order order : orderList) {
      if (order.getName() == object.getName()) {
        total += order.orderTotalPrice();
        System.out.println(total);
      }
    }
    return "Total items price for " + object.getName() + " is " + formatter.format(total);
  }

  public String getOrderNum() {
    return orderNum;
  }

  public String getOrderItem() {
    return orderItem;
  }

  public double getOrderPricePerUnit() {
    return orderPricePerUnit;
  }

  public int getOrderQuantity() {
    return orderQuantity;
  }

  public double orderTotalPrice() {
    return getOrderQuantity() * getOrderPricePerUnit();
  }

  public String print(Order customer) {
    System.out.println("Customer name: " + customer.getName());
    System.out.println();
    for (Order order : orderList) {
      if (order.getName() == customer.getName()) {
        System.out.println("Order number: " + order.getOrderNum() + "\nItem: " + order.getOrderItem() +
                "\nQuantity: " + order.getOrderQuantity() +"\nPrice Per Unit: "+ order.getOrderPricePerUnit() +
                "\nTotal price: " + order.orderTotalPrice());
        System.out.println();
      }
    }
    return "";
  }

}
