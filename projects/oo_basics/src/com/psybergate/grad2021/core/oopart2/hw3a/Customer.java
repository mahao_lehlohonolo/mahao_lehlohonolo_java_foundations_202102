package com.psybergate.grad2021.core.oopart2.hw3a;

public class Customer {

  private static int MIN_CUSTOMER_AGE = 18;

  private String customerNum;

  private String items;

  private int quantity;

  private int pricePerUnit;

  public Customer(String customerNum, String items, int quantity, int pricePerUnit) {
    this.customerNum = customerNum;
    this.items = items;
    this.quantity = quantity;
    this.pricePerUnit = pricePerUnit;
  }

  public static int getMinCustomerAge() {
    return MIN_CUSTOMER_AGE;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public boolean equals(Customer customer){
    if(this == customer){ //reflexivity
      return  true;
    }
    if(this.customerNum == customer.customerNum){ //
      return true;
    }
    if (this.getClass() != customer.getClass()) {//type equivalence
      return false;
    }
    if(customer == null){ //non-nullability
      return false;
    }
    return false;
  }

  public int getPricePerUnit() {
    return pricePerUnit;
  }

  public int getQuantity() {
    return quantity;
  }

  public String getItems() {
    return items;
  }

  public int getPriceTotal() {
    return quantity * pricePerUnit;
  }

  public String getName() {
    return customerNum;
  }

}
