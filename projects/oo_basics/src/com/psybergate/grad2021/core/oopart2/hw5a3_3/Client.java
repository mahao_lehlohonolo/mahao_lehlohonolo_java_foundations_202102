package com.psybergate.grad2021.core.oopart2.hw5a3_3;

import java.util.ArrayList;
import java.util.List;

import static com.psybergate.grad2021.core.oopart2.hw5a3_3.Order.*;

public class Client {
  public static void main(String[] args) {

    //Create customers
    Customer c1 = new Customer("Tendo", "Pain", "001", "Local");
    Customer c2 = new Customer("Kakashi", "Hatake", "002", "International");
    Customer c3 = new Customer("John", "Cena", "003", "International");

    //Create items
    Item item1 = new Item("Shoes", 1_499.99);
    Item item2 = new Item("Shirt", 499.99);
    Item item3 = new Item("Pants", 759.99);
    Item item4 = new Item("Glasses", 199_999.99);
    Item item5 = new Item("Something expensive", 1_299_999.99);

    //create the order items
    OrderItem orderItem1 = new OrderItem(item1,2);
    OrderItem orderItem2 = new OrderItem(item2,2);
    OrderItem orderItem3 = new OrderItem(item3,1);
    OrderItem orderItem4 = new OrderItem(item4,1);
    OrderItem orderItem5 = new OrderItem(item5,1);

    //create a customer cart
    //create an abstraction for the cart
    List<OrderItem> c1Cart = new ArrayList<>();
    List<OrderItem> c2Cart = new ArrayList<>();
    List<OrderItem> c3Cart = new ArrayList<>();

    //add the orderItems in customer's carts
    c1Cart.add(orderItem1);
    c1Cart.add(orderItem2);
    c1Cart.add(orderItem3);

    c2Cart.add(orderItem2);
    c2Cart.add(orderItem3);

    c3Cart.add(orderItem4);
    c3Cart.add(orderItem5);

    //Create the type of orders
    LocalOrder l1 = new LocalOrder(c1,"00001", c1Cart,false, true);
    InternationalOrder l2 = new InternationalOrder(c2, "00002", c2Cart, true, false);
    InternationalOrder l3 = new InternationalOrder(c3, "00003", c3Cart, false, true);

    //Store the orders in the orders list
    ordersList.add(l1);
    ordersList.add(l2);
    ordersList.add(l3);

    //lets print out the order details and totals
    System.out.println("customerOrdersTotal(c1) = " + customerOrdersTotal(c1));
    System.out.println("customerOrdersTotal(c2) = " + customerOrdersTotal(c2));
    System.out.println("customerOrdersTotal(c3) = " + customerOrdersTotal(c3));
    customerOrdersPrint(c1);
    customerOrdersPrint(c2);
    customerOrdersPrint(c3);
  }

}
