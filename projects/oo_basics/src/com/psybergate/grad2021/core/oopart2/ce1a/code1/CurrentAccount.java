package com.psybergate.grad2021.core.oopart2.ce1a.code1;

public class CurrentAccount extends Account {

  //For code1: CurrentAccount does not compile because it extends Account and as such its default
  // constructor is no longer automatically provided. So a constructor from the parent class must
  // be brought forth by super.

  //

//  public CurrentAccount(){
//  }

  public CurrentAccount(String accountNum) {
    super(accountNum);
  }


}
