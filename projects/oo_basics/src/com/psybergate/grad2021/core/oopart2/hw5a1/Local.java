package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.text.DecimalFormat;

public class Local extends Order {

  private static double DISCOUNT;

  private static double DISCOUNT_MARGIN;

  static {
    DISCOUNT = 0.05;
    DISCOUNT_MARGIN = 2000;
  }

  DecimalFormat formatter = new DecimalFormat("###.##");

  private String localName;

  public Local(String orderNum, String orderItem, double orderPricePerUnit, int orderQuantity,
               String localName) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity);
    this.localName = localName;
  }

  public void addLocalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                            int orderQuantity) {
    orderList.add(new Local(orderNum, orderItem, orderPricePerUnit,
            orderQuantity, this.localName));
  }

  @Override
  public String getName() {
    return localName;
  }

  @Override
  public String customerOrderTotalPrice(Order object) {
    double total = 0;
    for (Order order : orderList) {
      if (order.getName() == object.getName()) {
        total += order.orderTotalPrice();
      }
    }
    if (total > DISCOUNT_MARGIN) {
      total = total * (0.95);
    }
    return "Total items price for " + object.getName() + " is " + formatter.format(total);
  }


}
