package com.psybergate.grad2021.core.oopart2.nulltype;

public class MyNull {

  public static void main(String[] args) {
    //lets test if there is only one Null.
    MyNull obj1 = null;
    MyNull obj2 = null;
    MyNull obj3 = null;

    if (obj1 == obj2 & obj3 == obj1 & obj2 == obj3) {
      System.out.println("Only one null.");
    }

    //lets test null type compatibility


    MyNull obj4 = null;
    String obj5 = null;
    Object obj6 = null;

    if (((Object) obj4) == obj5 & obj6 == obj5 & obj6 == obj4) {
      System.out.println("Null is type compatible.");
    }


  }

}
