package com.psybergate.grad2021.core.oopart2.overriding;

public class MyChild extends MyParent {

  private final int MY_NUM2 = 200;

  private int age;

  public MyChild(int someNum, Integer age) {
    super(someNum);
    this.age = age;
  }

  public int getAge() {
    return age;
  }

  public void handler() throws NullPointerException {
  }

  /*the override keyword helps with telling the compiler that whatever method I am going to put
  is overridden from the parent class. If it happens that I mistakenly change the name for
  instance, the compiler warns me that "hey you said you are overriding this method" , without it
   I would be making another method by mistake.*/
  @Override
  public void print() {
    System.out.println("");
  }


}
