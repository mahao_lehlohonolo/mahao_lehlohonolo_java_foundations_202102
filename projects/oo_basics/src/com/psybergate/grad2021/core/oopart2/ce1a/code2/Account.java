package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class Account {

  private String customerName;

  private String customerSurname;

  public Account() {
    System.out.println("I am a parent");
  }

  public Account(String customerName, String customerSurname) {
    this.customerName = customerName;
    this.customerSurname = customerSurname;
    System.out.println(customerName);
    System.out.println(customerSurname);
  }

  //For code2: The sequence of prints starts with the parent class first then goes to the child
  // class. This is because on the first line of the child constructor we have super() which
  // calls the parent constructor and executes it. The super keyword is always first in the child
  // class. If you move it down the body of the child constructor, it will not compile.
}
