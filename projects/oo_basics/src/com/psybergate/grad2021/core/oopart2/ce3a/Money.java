package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public final class Money {

  private BigDecimal myVar;

  public Money(BigDecimal myVar) {
    this.myVar = myVar;
    BigDecimal value = new BigDecimal(String.valueOf(myVar));
  }

  public Money(int amount) {
    myVar = new BigDecimal(amount);
  }

  public Money(double amount) {
    myVar = new BigDecimal(amount);
  }

  public static void main(String[] args) {
    Money m1 = new Money(new BigDecimal("20"));
    Money m2 = new Money(2);
    System.out.println("m1.addMoney(m1) = " + m2.addMoney(m2));
  }

  public Money addMoney(Money object) {
    return new Money(object.myVar.add(object.myVar));
  }

  public BigDecimal getMyVar() {
    return myVar;
  }

}
