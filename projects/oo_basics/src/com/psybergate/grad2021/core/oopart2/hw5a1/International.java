package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.text.DecimalFormat;

public class International extends Order {

  DecimalFormat formatter = new DecimalFormat("###.##");

  private static final double IMPORT_DUTY;

  static {
    IMPORT_DUTY = 0.045;
  }

  private String internationalName;

  public International(String orderNum, String orderItem, double orderPricePerUnit,
                       int orderQuantity, String internationalName) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity);
    this.internationalName = internationalName;
  }

  public void addInternationalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                            int orderQuantity) {
    orderList.add(new International(orderNum, orderItem, orderPricePerUnit,
            orderQuantity, this.internationalName));
  }

  @Override
  public String getName() {
    return internationalName;
  }

  @Override
  public String customerOrderTotalPrice(Order object) {
    double total = 0;
    for (Order order : orderList) {
      if(order.getName() == object.getName()){
        total += order.orderTotalPrice();
      }
    }
    total = total * (1 + IMPORT_DUTY);
    return "Total items price for " + object.getName() + " is " + formatter.format(total);
  }

}
