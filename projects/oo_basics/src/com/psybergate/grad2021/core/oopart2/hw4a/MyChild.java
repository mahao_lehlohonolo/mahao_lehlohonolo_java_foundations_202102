package com.psybergate.grad2021.core.oopart2.hw4a;

public class MyChild extends MyParent{

  public MyChild() {
    super();
  }

  //cannot override static method.
//  @Override
//  public static void someMethod(){
//    System.out.println("Child's someMethod");
//  }

}
