package com.psybergate.grad2021.core.oopart2.hw5a2;

public class International extends Order {

  private String internationalName;

  public International(String internationalName, String orderNum, String orderItem,
                       double orderPricePerUnit,
                       int orderQuantity) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity);
    this.internationalName = internationalName;
  }

  public void addInternationalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                                    int orderQuantity) {
    orderList.add(new International(this.internationalName, orderNum, orderItem, orderPricePerUnit
            , orderQuantity));
  }

  @Override
  public String getName() {
    return internationalName;
  }

}
