package com.psybergate.grad2021.core.oopart2.hw5a3_1;

public class Local extends Order {

  private String localName;

  public Local(String orderNum, String orderItem, double orderPricePerUnit, int orderQuantity,
               String localName, int numberOfYearsAsClient, String discountPolicy) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity, numberOfYearsAsClient,
            discountPolicy);
    this.localName = localName;
  }

  public void addLocalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                            int orderQuantity) {
    orderList.add(new Local(orderNum, orderItem, orderPricePerUnit, orderQuantity, this.localName,
            getNumberOfYearsAsClient(), getDiscountPolicy()));
  }

  @Override
  public String getName() {
    return localName;
  }

}
