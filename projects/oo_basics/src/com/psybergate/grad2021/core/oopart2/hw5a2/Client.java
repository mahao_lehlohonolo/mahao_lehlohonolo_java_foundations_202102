package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.util.Collections;

import static com.psybergate.grad2021.core.oopart2.hw5a2.Order.orderList;

public class Client {

  public static void main(String[] args) {

    Local customer1 = new Local("MAHAO","01", "SHIRT", 550, 2, 3);
    Local customer2 = new Local("MANONZ","04", "WINE", 350, 3, 6);
    International customer3 = new International("SOME FACTORY", "07", "CUTTING MACHINE",10000, 2);
    International customer4 = new International("JEWELLER", "09", "GOLD",1001.5, 40);

    //lets add the customers in the ArrayList
    Collections.addAll(orderList, customer1, customer2, customer3, customer4);

    //lets add new orders for the customers
    customer1.addLocalOrder("02", "PANTS", 800, 3);
    customer1.addLocalOrder("03", "SOCKS", 200, 1);

    customer2.addLocalOrder("05", "BISCUITS", 35, 6);
    customer2.addLocalOrder("06", "SNACKS", 25, 8);

    customer3.addInternationalOrder("08", "STEEL", 501.2, 100);
    customer3.addInternationalOrder("07", "COAL", 741.89, 200);

    customer4.addInternationalOrder("10", "PLATINUM", 2003.4, 45);
    customer4.addInternationalOrder("11", "GOLD", 1253.4, 100);

    //lets print the total price for the orders of the customers
    System.out.println(customer1.customerOrderTotalPrice(customer1));
    System.out.println(customer2.customerOrderTotalPrice(customer2));
    System.out.println(customer3.customerOrderTotalPrice(customer3));
    System.out.println(customer4.customerOrderTotalPrice(customer4));

    //lets print the customer orders in a list
    System.out.println(customer1.print(customer1));
    System.out.println(customer2.print(customer2));
    System.out.println(customer3.print(customer3));
    System.out.println(customer4.print(customer4));

  }

}
