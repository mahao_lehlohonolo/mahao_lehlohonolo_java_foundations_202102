package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.util.Collections;

import static com.psybergate.grad2021.core.oopart2.hw5a3.Order.orderList;

public class Client {

  public static void main(String[] args) {
    Local c1 = new Local("01", "Books", 1500, 2, "Mahao",
            4, "Years based");
    International c2 = new International("04", "JUICE", 30, 250,
            "Some Guy", 3, "Years based");
    International c3 = new International("07", "Car", 500600, 1, "Joshua Doore", 3, "Years based");

    //lets store these customers in an ArrayList(orderList)
    Collections.addAll(orderList, c1, c2,c3);

    //add new orders for the customers in the same ArrayList(orderList)
    c1.addLocalOrder("02", "Pencils", 15, 5);
    c1.addLocalOrder("03", "Crystal Pen", 30, 2);
    c2.addInternationalOrder("05", "FRUITS", 15, 600);
    c2.addInternationalOrder("06", "PEANUTS", 20, 500);
    c3.addInternationalOrder("08","Tyres",60500,8);
    c3.addInternationalOrder("09","Spoiler",110541,2);

    //lets print the total prices for the orders of the customers.
    System.out.println(c1.customerOrderTotalPrice(c1));
    System.out.println(c2.customerOrderTotalPrice(c2));
    System.out.println(c3.customerOrderTotalPrice(c3));

    //lets print customers detailed order list
    System.out.println(c1.print(c1));
    System.out.println(c2.print(c2));
    System.out.println(c3.print(c3));
  }
}
