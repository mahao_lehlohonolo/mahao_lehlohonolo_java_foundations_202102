package com.psybergate.grad2021.core.oopart2.hw5a2_1;

public class LocalOrder extends Order {

  public LocalOrder(Customer customer, String orderNum, Item item, int quantity) {
    super(customer, orderNum, item, quantity);

    if (customer.isLocal() != true) {
      throw new RuntimeException("Customer must be local for local orders");
    }
  }
}