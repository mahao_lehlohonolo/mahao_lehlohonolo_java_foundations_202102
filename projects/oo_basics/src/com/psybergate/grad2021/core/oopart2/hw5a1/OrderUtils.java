package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.util.Collections;

import static com.psybergate.grad2021.core.oopart2.hw5a1.Order.orderList;

public class OrderUtils {

  public static void main(String[] args) {

    Local customer1 = new Local("01", "BMW M4", 120000, 1, "Mahao");
    International customer2 = new International("05", "GOLD", 1200.98, 100, "Some jeweller");
    Local customer3 = new Local("09", "SNEAKERS", 1400, 2, "Nonny");
    International customer4 = new International("12", "WATER", 105.36, 10000, "WATER ORG");

    //add the customers in the ArrayList
    Collections.addAll(orderList, customer1, customer2, customer3, customer4);

    //add new orders for the customers
    customer1.addLocalOrder("02", "DODGE RAM", 160000, 1);
    customer1.addLocalOrder("03", "HELLCAT", 100000, 1);

    customer2.addInternationalOrder("06", "DIAMONDS", 1500.65, 50);
    customer2.addInternationalOrder("07", "PLATINUM", 995.51, 25);
    customer2.addInternationalOrder("08", "SILVER", 705.21, 10);

    customer3.addLocalOrder("10", "DRESS", 4300, 1);
    customer3.addLocalOrder("11", "SKIRT", 700, 1);

    customer4.addInternationalOrder("13", "FILTER", 5500, 6);
    customer4.addInternationalOrder("14", "BOLTS", 2500, 5);

    //lets print the total price of the orders for the customers
    System.out.println(customer1.customerOrderTotalPrice(customer1));
    System.out.println(customer2.customerOrderTotalPrice(customer2));
    System.out.println(customer3.customerOrderTotalPrice(customer3));
    System.out.println(customer4.customerOrderTotalPrice(customer4));

    //lets print the detailed customer orders in a list
    System.out.println(customer1.print(customer1));
    System.out.println(customer2.print(customer2));
    System.out.println(customer3.print(customer3));

  }

}
