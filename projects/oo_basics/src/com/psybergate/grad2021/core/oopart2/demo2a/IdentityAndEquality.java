package com.psybergate.grad2021.core.oopart2.demo2a;

public class IdentityAndEquality {

  private int someNum;

  private int someNum2;

  public IdentityAndEquality(int someNum, int someNum2) {
    this.someNum = someNum;
    this.someNum2 = someNum2;
  }

  public static void main(String[] args) {

    int a = 1;

    int b = 1;

    int c = Integer.valueOf(1);

    int d = new Integer(1);

    int e = new Integer(1);

    String s1 = "String1";

    String s2 = "String1";

    String s3 = new String("String1");

    System.out.println("(s1==s2) " + (s1 == s2));
    System.out.println("(s2==s3) " + (s2 == s3));
    System.out.println("(a==b) = " + (a == b));
    System.out.println("(c==b) = " + (c == b));
    System.out.println("(c==d) = " + (c == d));
    System.out.println("(e==d) = " + (e == d));

    IdentityAndEquality object = new IdentityAndEquality(25,30);
    IdentityAndEquality object1 = new IdentityAndEquality(25,30);
    System.out.println("object1==object " + (object1==object));
    System.out.println("object1.equals(object) = " + object1.equals(object));
//    System.out.println("object.hashCode() = " + object.hashCode());
//    System.out.println("System.identityHashCode(object) = " + System.identityHashCode(object));
//    System.out.println("System.identityHashCode(object) = " + System.identityHashCode(object1));
  }

}
