package com.psybergate.grad2021.core.oopart2.hw5a_1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected static List<Order> ordersList = new ArrayList<>();

  static DecimalFormat df = new DecimalFormat("###.##");

  private Customer customer;

  private String orderNum;

  private Item item;

  private int quantity;

  public Order(Customer customer, String orderNum, Item item, int quantity) {
    this.customer = customer;
    this.orderNum = orderNum;
    this.item = item;
    this.quantity = quantity;
  }

  public static double customerOrdersTotal(Customer obj) {
    double total = 0;
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        total += ordersList.get(count).getItem().getPricePerUnit() * ordersList.get(count).getQuantity();
      }
    }
    return Double.parseDouble(df.format(total));
  }

  public static void customerOrdersPrint(Customer obj) {
    System.out.println("Orders of " + obj.getName() + " " + obj.getSurname() + "(" + "Customer " +
            "ID:" + obj.getCustomerID() + ")");
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        System.out.println("Item:" + ordersList.get(count).getItem().getItemName() + " |" + " " +
                "Price Per Unit:" + ordersList.get(count).getItem().getPricePerUnit() + " |" + " " +
                "Quantity:" + ordersList.get(count).getQuantity());
      }
    }
  }

  public String getOrderNum() {
    return orderNum;
  }

  public Customer getCustomer() {
    return customer;
  }

  public Item getItem() {
    return item;
  }

  public int getQuantity() {
    return quantity;
  }

}
