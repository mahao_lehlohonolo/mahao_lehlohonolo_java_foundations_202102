package com.psybergate.grad2021.core.oopart2.hw3a;

import java.util.ArrayList;
import java.util.List;

public class Client {

  static List<Customer> myList = new ArrayList<>();

  public static void customerUniquenessTest(Customer object) {
    myloop:
    for (Customer myObj : myList) {
      if (object.getCustomerNum() == myObj.getCustomerNum()) {
        System.out.println("Customer not unique.");
        break myloop;
      } else {
        System.out.println("Customer unique.");
      }

    }
  }

  public static void equality(Customer obj1, Customer obj2){
    if(obj1 == obj2){
      System.out.println("Objects are equal.");
    }
  }

  public static void equivalenceTest(Customer obj1, Customer objt2) {
    if (obj1.getCustomerNum() == objt2.getCustomerNum() && obj1.getItems() == objt2.getItems() &&
            obj1.getQuantity() == objt2.getQuantity() && obj1.getPricePerUnit() == objt2.getPricePerUnit()) {
      System.out.println("Objects equivalent.");
    }
  }

  public static void main(String[] args) {

    Customer cust1 = new Customer("12547", "Something", 10, 100);
    Customer cust2 = new Customer("12547", "Something", 10, 100);
    myList.add(cust1);
    myList.add(cust2);
    customerUniquenessTest(cust2);

    //equality demonstration(object centric)
    Customer cust3 = new Customer("1001", "Some product", 50, 600);
    Customer cust4 = cust3;
    equality(cust4,cust3);
    System.out.println("cust3.equals(cust3) = " + cust3.equals(cust3));
//    System.out.println("(cust4==cust3) = " + (cust4 == cust3));

    //equivalence demonstration(states centric)
    Customer cust5 = new Customer("4578", "Some machine", 16, 1000);
    Customer cust6 = new Customer("4578", "Some machine", 16, 1000);
    equivalenceTest(cust6, cust5);

  }

}
