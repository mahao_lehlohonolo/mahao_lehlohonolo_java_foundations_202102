package com.psybergate.grad2021.core.oopart2.hw5a3_1;

public class International extends Order{

  private String internationalName;

  public International(String orderNum, String orderItem, double orderPricePerUnit,
                       int orderQuantity, String internationalName, int numberOfYearsAsClient,
                       String discountPolicy) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity, numberOfYearsAsClient, discountPolicy);
    this.internationalName = internationalName;
  }

  public void addInternationalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                                    int orderQuantity) {
    orderList.add(new International(orderNum, orderItem, orderPricePerUnit, orderQuantity,
            this.internationalName, getNumberOfYearsAsClient(), getDiscountPolicy()));
  }

  @Override
  public String getName() {
    return internationalName;
  }
}
