package com.psybergate.grad2021.core.oopart2.hw5a;

public class Customer extends OrderSystem{

  private String customerName;

  public Customer(String orderNum, String orderItem, double orderPricePerUnit, int orderQuantity,
                  String customerName) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity);
    this.customerName = customerName;
  }

  @Override
  public String getCustomerName() {
    return customerName;
  }

}
