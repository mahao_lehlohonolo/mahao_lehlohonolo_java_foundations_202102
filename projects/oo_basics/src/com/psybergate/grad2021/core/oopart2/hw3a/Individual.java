package com.psybergate.grad2021.core.oopart2.hw3a;

public class Individual extends Customer{

  private String name;

  private int age;

  public Individual(String customerNum, String items, int quantity, int pricePerUnit,
                        String name, int age) {
    super(customerNum, items, quantity, pricePerUnit);
    this.name = name;
    this.age = age;
    customerValidity();
  }

  public String getName() {
    return name;
  }

  public void customerValidity() {
    if(age<getMinCustomerAge()){
      throw new RuntimeException("Customer age is below " + getMinCustomerAge());
    }
  }

}
