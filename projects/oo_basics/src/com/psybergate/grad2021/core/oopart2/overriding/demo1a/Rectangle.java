package com.psybergate.grad2021.core.oopart2.overriding.demo1a;

public class Rectangle extends Shape {

  private int length;

  private int width;

  public Rectangle(int side1, int side2) {
    super(side1, side2);
    this.length = side1;
    this.width = side2;
  }

/*Static methods are those which belong to the class.They do not belong to the object and in
overriding, object decides which method is to be called. Method overriding occurs dynamically(run
 time) that means the method which is to be executed is decided at run time according to object used
  for calling while static methods are looked up statically(compile time). */

//  @Override
//  public static void someStaticMethod(){
//    System.out.println("Static method");
//  }


  /*Cannot override the Shape constructor, the compiler forces you to put void or change the name
  (Tries to obey the rule of the constructor having the same name as the class it is in)*/
//  public Shape(int side1, int side2) {
//    this.side1 = side1;
//    this.side2 = side2;
//  }

  @Override
  public int getArea() {
    return length * width;
  }

}


