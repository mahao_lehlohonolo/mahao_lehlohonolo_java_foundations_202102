package com.psybergate.grad2021.core.oopart2.hw5a1_1;

public class Item {

  private String itemName;

  private double pricePerUnit;

  public Item(String itemName, double pricePerUnit) {
    this.itemName = itemName;
    this.pricePerUnit = pricePerUnit;
  }

  public double getPricePerUnit() {
    return pricePerUnit;
  }

  public String getItemName() {
    return itemName;
  }

}
