package com.psybergate.grad2021.core.oopart2.hw5a_1;

import static com.psybergate.grad2021.core.oopart2.hw5a_1.Order.*;

public class Client {

  public static void main(String[] args) {

    //Create customers
    Customer c1 = new Customer("Lehlohonolo", "Mahao", "001", true, false);
    Customer c2 = new Customer("Noncedo", "Mahao", "002", false, true);

    //Create items
    Item item1 = new Item("Shoes", 1499.99);
    Item item2 = new Item("Shoes", 1499.99);
    Item item3 = new Item("Pants", 759.99);

    //Create the type of orders
    LocalOrder l1 = new LocalOrder(c1, "01", item1,2);
    LocalOrder l3 = new LocalOrder(c1, "03", item2,1);
    InternationalOrder l2 = new InternationalOrder(c2, "02", item1, 2);
    InternationalOrder l4 = new InternationalOrder(c2, "04", item3,2);

    //Store the orders in the orders list
    ordersList.add(l1);
    ordersList.add(l3);
    ordersList.add(l2);
    ordersList.add(l4);

    //lets print out the order details and totals
    System.out.println("customerOrdersTotal(c1) = " + customerOrdersTotal(c1));
    System.out.println("customerOrdersTotal(c2) = " + customerOrdersTotal(c2));
    customerOrdersPrint(c1);
    customerOrdersPrint(c2);

  }

}
