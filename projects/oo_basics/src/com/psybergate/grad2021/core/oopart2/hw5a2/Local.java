package com.psybergate.grad2021.core.oopart2.hw5a2;

public class Local extends Order {

  private String localName;

  public Local(String localName, String orderNum, String orderItem, double orderPricePerUnit,
               int orderQuantity,
               int numberOfYearsAsClient) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity, numberOfYearsAsClient);
    this.localName = localName;
  }

  public void addLocalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                            int orderQuantity) {
    orderList.add(new Local(this.localName, orderNum, orderItem, orderPricePerUnit, orderQuantity,
            getNumberOfYearsAsClient()));
  }

  @Override
  public String getName() {
    return localName;
  }

}
