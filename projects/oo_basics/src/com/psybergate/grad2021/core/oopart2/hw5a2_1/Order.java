package com.psybergate.grad2021.core.oopart2.hw5a2_1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected static List<Order> ordersList = new ArrayList<>();

  static DecimalFormat df = new DecimalFormat("###.##");

  private static double LOCAL_DISCOUNT_RATE1;

  private static double LOCAL_DISCOUNT_RATE2;

  private static int LOCAL_DISCOUNT_MARGIN1;//year based

  private static int LOCAL_DISCOUNT_MARGIN2;//year based

  private static int INT_DISCOUNT_MARGIN1;//total based

  private static int INT_DISCOUNT_MARGIN2;//total based

  private static double INT_DISCOUNT_RATE1;

  private static double INT_DISCOUNT_RATE2;

  private static double IMPORT_DUTY_RATE;

  static {
    IMPORT_DUTY_RATE = 0.045;
    LOCAL_DISCOUNT_RATE1 = 0.075;
    LOCAL_DISCOUNT_RATE2 = 0.125;
    INT_DISCOUNT_RATE1 = 0.05;
    INT_DISCOUNT_RATE2 = 0.1;
    LOCAL_DISCOUNT_MARGIN1 = 2;
    LOCAL_DISCOUNT_MARGIN2 = 5;
    INT_DISCOUNT_MARGIN1 = 500_000;
    INT_DISCOUNT_MARGIN2 = 1_000_000;
  }

  private Customer customer;

  private String orderNum;

  private Item item;

  private int quantity;

  public Order(Customer customer, String orderNum, Item item, int quantity) {
    this.customer = customer;
    this.orderNum = orderNum;
    this.item = item;
    this.quantity = quantity;
  }

  public static double customerOrdersTotal(Customer obj) {
    double total = 0;
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        total += ordersList.get(count).getItem().getPricePerUnit() * ordersList.get(count).getQuantity();
      }
    }
    if (obj.isInternational() == true) {
      if (total > getIntDiscountMargin1() && total <= getIntDiscountMargin2()) {
        total = total * (1 - getIntDiscountRate1());
      }
      if (total > getIntDiscountMargin2()) {
        total = total * (1 - getIntDiscountRate2());
      }
      total = total * (1 + getImportDutyRate());
    }
    if (obj.isLocal() == true) {
      int numOfYearsASClient = obj.yearsAsCustomer();
      if (numOfYearsASClient > getLocalDiscountMargin1() && numOfYearsASClient <= getLocalDiscountMargin2()) {
        total = total * (1 - getLocalDiscountRate1());
      }
      if (numOfYearsASClient > getLocalDiscountMargin2()) {
        total = total * (1 - getLocalDiscountRate2());
      }
    }
    return Double.parseDouble(df.format(total));
  }

  public static void customerOrdersPrint(Customer obj) {
    System.out.println("Orders of " + obj.getName() + " " + obj.getSurname() + "(" + "Customer " +
            "ID:" + obj.getCustomerID() + ")");
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        System.out.println("Item:" + ordersList.get(count).getItem().getItemName() + " |" + " " +
                "Price Per Unit:" + ordersList.get(count).getItem().getPricePerUnit() + " |" + " " +
                "Quantity:" + ordersList.get(count).getQuantity());
      }
    }
  }

  public static double getImportDutyRate() {
    return IMPORT_DUTY_RATE;
  }

  public static double getLocalDiscountRate1() {
    return LOCAL_DISCOUNT_RATE1;
  }

  public static double getLocalDiscountRate2() {
    return LOCAL_DISCOUNT_RATE2;
  }

  public static int getLocalDiscountMargin1() {
    return LOCAL_DISCOUNT_MARGIN1;
  }

  public static int getLocalDiscountMargin2() {
    return LOCAL_DISCOUNT_MARGIN2;
  }

  public static int getIntDiscountMargin1() {
    return INT_DISCOUNT_MARGIN1;
  }

  public static int getIntDiscountMargin2() {
    return INT_DISCOUNT_MARGIN2;
  }

  public static double getIntDiscountRate1() {
    return INT_DISCOUNT_RATE1;
  }

  public static double getIntDiscountRate2() {
    return INT_DISCOUNT_RATE2;
  }

  public String getOrderNum() {
    return orderNum;
  }

  public Customer getCustomer() {
    return customer;
  }

  public Item getItem() {
    return item;
  }

  public int getQuantity() {
    return quantity;
  }


}