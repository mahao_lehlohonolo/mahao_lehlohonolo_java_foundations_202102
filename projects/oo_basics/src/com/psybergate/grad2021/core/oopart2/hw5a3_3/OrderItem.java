package com.psybergate.grad2021.core.oopart2.hw5a3_3;

public class OrderItem {

  private Item item;

  private int quantity;

  public OrderItem(Item item, int quantity) {
    this.item = item;
    this.quantity = quantity;
  }

  public double getItemTotal(){
    return item.getPricePerUnit() * getQuantity();
  }

  public int getQuantity() {
    return quantity;
  }

  public Item getItem() {
    return item;
  }

}
