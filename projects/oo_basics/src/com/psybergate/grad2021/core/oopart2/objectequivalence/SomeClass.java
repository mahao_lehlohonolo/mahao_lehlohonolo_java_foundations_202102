package com.psybergate.grad2021.core.oopart2.objectequivalence;

public class SomeClass {

  public static void main(String[] args) {
    SomeClass obj1 = new SomeClass();
    SomeClass obj2 = new SomeClass();
    SomeClass obj3 = obj2;

    System.out.println(obj1==obj2);
    System.out.println(obj2==obj3);
    System.out.println(obj1.equals(obj2));
  }

}
