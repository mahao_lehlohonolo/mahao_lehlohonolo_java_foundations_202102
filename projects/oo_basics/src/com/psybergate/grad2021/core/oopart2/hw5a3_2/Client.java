package com.psybergate.grad2021.core.oopart2.hw5a3_2;

import static com.psybergate.grad2021.core.oopart2.hw5a3_2.Order.*;

public class Client {

  public static void main(String[] args) {

    //Create customers
    Customer c1 = new Customer("Nagato", "Uzumaki", "001", true, false);
    Customer c2 = new Customer("Kakashi", "Hatake", "002", false, true);
    Customer c3 = new Customer("John", "Cena", "003", false, true);

    //Create items
    Item item1 = new Item("Shoes", 1_499.99);
    Item item2 = new Item("Shirt", 499.99);
    Item item3 = new Item("Pants", 759.99);
    Item item4 = new Item("Glasses", 199_999.99);
    Item item5 = new Item("Watch", 1_299_999.99);

    //Create the type of orders
    LocalOrder l1 = new LocalOrder(c1, "01", item1, 1, true, false);
    LocalOrder l3 = new LocalOrder(c1, "03", item2, 1, false, true);
    InternationalOrder l2 = new InternationalOrder(c2, "02", item1, 2, false, true);
    InternationalOrder l4 = new InternationalOrder(c2, "04", item3, 2, false, true);
    InternationalOrder l5 = new InternationalOrder(c3, "05", item4, 1, false, true);
    InternationalOrder l6 = new InternationalOrder(c3, "06", item5, 1, false, true);

    //Store the orders in the orders list
    ordersList.add(l1);
    ordersList.add(l3);
    ordersList.add(l2);
    ordersList.add(l4);
    ordersList.add(l5);
    ordersList.add(l6);

    //lets print out the order details and totals
    System.out.println("customerOrdersTotal(c1) = " + customerOrdersTotal(c1));
    System.out.println("customerOrdersTotal(c2) = " + customerOrdersTotal(c2));
    System.out.println("customerOrdersTotal(c3) = " + customerOrdersTotal(c3));
    customerOrdersPrint(c1);
    customerOrdersPrint(c2);
    customerOrdersPrint(c3);

  }

}
