package com.psybergate.grad2021.core.oopart2.demo2a;

import java.util.ArrayList;
import java.util.List;

public class CurrentAccount extends Account {

  public CurrentAccount() {
    super();
  }

  public static void main(String[] args) {
    List<CurrentAccount> myList = new ArrayList<>();

    CurrentAccount acc1 = new CurrentAccount();
    CurrentAccount acc2 = new CurrentAccount();
    CurrentAccount acc3 = new CurrentAccount();
    CurrentAccount acc4 = new CurrentAccount();
    CurrentAccount acc5 = new CurrentAccount();

    myList.add(acc1);
    myList.add(acc2);
    myList.add(acc3);
    myList.add(acc4);
    myList.add(acc5);

    System.out.println("myList.contains(acc4) = " + myList.contains(acc4));

    for (int count = 0; count < myList.size(); count++) {
      System.out.println("(acc4==myList(count)) = " + (acc4 == myList.get(count)));
      System.out.println("(acc4.equals(myList(count))) = " + (acc4.equals(myList.get(count))));
    }

  }

}
