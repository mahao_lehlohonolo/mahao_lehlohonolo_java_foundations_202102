package com.psybergate.grad2021.core.oopart2.hw5a3;


import java.text.DecimalFormat;

public class International extends Order {

  private static final double IMPORT_DUTY;

  private static final int DISCOUNT_MARGIN1;

  private static final int DISCOUNT_MARGIN2;

  private static final double TOTAL_DISCOUNT_1;

  private static final double TOTAL_DISCOUNT_2;

  private static double YEARS_DISCOUNT_1;

  private static double YEARS_DISCOUNT_2;

  private static double YEARS_DISCOUNT_3;

  private static String YEARS_BASED_DISCOUNT;

  private static String TOTAL_BASED_DISCOUNT;


  static {
    YEARS_BASED_DISCOUNT = "YEARS BASED";
    TOTAL_BASED_DISCOUNT = "TOTAL BASED";
    YEARS_DISCOUNT_1 = 0;
    YEARS_DISCOUNT_2 = 0.075;
    YEARS_DISCOUNT_3 = 0.125;
    IMPORT_DUTY = 0.045;
    DISCOUNT_MARGIN1 = 500000;
    DISCOUNT_MARGIN2 = 1000000;
    TOTAL_DISCOUNT_1 = 0.05;
    TOTAL_DISCOUNT_2 = 0.1;
  }

  DecimalFormat formatter = new DecimalFormat("###.##");

  private String internationalName;

  private String discountPolicy;

  private int numberOfYearsAsClient;

  public International(String orderNum, String orderItem, double orderPricePerUnit,
                       int orderQuantity, String internationalName, int numberOfYearsAsClient,
                       String discountPolicy) {
    super(orderNum, orderItem, orderPricePerUnit, orderQuantity);
    this.internationalName = internationalName;
    this.discountPolicy = discountPolicy;
    this.numberOfYearsAsClient = numberOfYearsAsClient;
    discountPolicyValidation();
  }

  @Override
  public void discountPolicyValidation() {
    if (discountPolicy.equalsIgnoreCase(TOTAL_BASED_DISCOUNT) == false & discountPolicy.equalsIgnoreCase(YEARS_BASED_DISCOUNT) == false) {
      throw new RuntimeException("Discount Policy must be either 'Years based' or 'Total based'");
    }
  }


  public void addInternationalOrder(String orderNum, String orderItem, double orderPricePerUnit,
                                    int orderQuantity) {
    orderList.add(new International(orderNum, orderItem, orderPricePerUnit, orderQuantity,
            this.internationalName, this.numberOfYearsAsClient, this.discountPolicy));
  }

  @Override
  public String getName() {
    return internationalName;
  }

  public String customerOrderTotalPrice(Order object) {
    double total = 0;
    for (Order order : orderList) {
      if (order.getName() == object.getName()) {
        total += order.orderTotalPrice();
      }
    }
    if (discountPolicy.equalsIgnoreCase(TOTAL_BASED_DISCOUNT)) {
      if (total > DISCOUNT_MARGIN1 && total <= DISCOUNT_MARGIN2) {
        total = total * (1 - TOTAL_DISCOUNT_1);
      }
      if (total > DISCOUNT_MARGIN2) {
        total = total * (1 - TOTAL_DISCOUNT_2);
      }
    }
    if (discountPolicy.equalsIgnoreCase(YEARS_BASED_DISCOUNT)) {
      if (this.numberOfYearsAsClient > 2 && this.numberOfYearsAsClient <= 5) {
        total = total * (1 - YEARS_DISCOUNT_2);
      }
      if (this.numberOfYearsAsClient > 5) {
        total = total * (1 - YEARS_DISCOUNT_3);
      }
    }
    total = total * (IMPORT_DUTY + 1);
    return "Total items price for " + object.getName() + " is " + formatter.format(total);
  }
}
