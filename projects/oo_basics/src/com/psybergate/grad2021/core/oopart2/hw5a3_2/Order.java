package com.psybergate.grad2021.core.oopart2.hw5a3_2;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected static List<Order> ordersList = new ArrayList<>();

  static DecimalFormat df = new DecimalFormat("###.##");

  private static double YEARS_BASED_DISCOUNT_RATE1;

  private static double YEARS_BASED_DISCOUNT_RATE2;

  private static int YEARS_BASED_DISCOUNT_MARGIN1;

  private static int YEARS_BASED_DISCOUNT_MARGIN2;

  private static int TOTAL_BASED_DISCOUNT_MARGIN1;

  private static int TOTAL_BASED_DISCOUNT_MARGIN2;

  private static double TOTAL_BASED_DISCOUNT_RATE1;

  private static double TOTAL_BASED_DISCOUNT_RATE2;

  private static double IMPORT_DUTY_RATE;

  static {
    IMPORT_DUTY_RATE = 0.045;
    YEARS_BASED_DISCOUNT_RATE1 = 0.075;
    YEARS_BASED_DISCOUNT_RATE2 = 0.125;
    TOTAL_BASED_DISCOUNT_RATE1 = 0.05;
    TOTAL_BASED_DISCOUNT_RATE2 = 0.1;
    YEARS_BASED_DISCOUNT_MARGIN1 = 2;
    YEARS_BASED_DISCOUNT_MARGIN2 = 5;
    TOTAL_BASED_DISCOUNT_MARGIN1 = 500_000;
    TOTAL_BASED_DISCOUNT_MARGIN2 = 1_000_000;
  }

  private boolean yearsBasedDiscount;

  private boolean totalValueBasedDiscount;

  private Customer customer;

  private String orderNum;

  private Item item;

  private int quantity;

  public Order(Customer customer, String orderNum, Item item, int quantity,
               boolean yearsBasedDiscount, boolean totalValueBasedDiscount) {
    this.customer = customer;
    this.orderNum = orderNum;
    this.item = item;
    this.quantity = quantity;
    this.yearsBasedDiscount = yearsBasedDiscount;
    this.totalValueBasedDiscount = totalValueBasedDiscount;
    validateOrderDiscountPolicy();
  }

  public static double customerOrdersTotal(Customer obj) {
    double total = 0;
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        if (ordersList.get(count).isTotalValueBasedDiscount() == true) {
          if (orderTotal(count) > getTotalBasedDiscountMargin1() && orderTotal(count) <= getTotalBasedDiscountMargin2()) {
            total += orderTotal(count) * (1 - getTotalBasedDiscountRate1());
          }
          if (orderTotal(count) > getTotalBasedDiscountMargin2()) {
            total += orderTotal(count) * (1 - getTotalBasedDiscountRate2());
          } else {
            total += orderTotal(count);
          }
        }
        if (ordersList.get(count).isYearsBasedDiscount() == true) {
          int numOfYearsASClient = obj.yearsAsCustomer();
          if (numOfYearsASClient > getYearsBasedDiscountMargin1() && numOfYearsASClient <=
                  getYearsBasedDiscountMargin2()) {
            total += orderTotal(count) * (1 - getYearsBasedDiscountRate1());
          }
          if (numOfYearsASClient > getYearsBasedDiscountMargin2()) {
            total += orderTotal(count) * (1 - getYearsBasedDiscountRate2());
          } else {
            total += orderTotal(count);
          }
        }
      }
    }
    if (obj.isInternational() == true) {
      total = total * (1 + getImportDutyRate());
    }
    return Double.parseDouble(df.format(total));
  }

  public static double orderTotal(int num) {
    return ordersList.get(num).getItem().getPricePerUnit() * ordersList.get(num).getQuantity();
  }

  public static void customerOrdersPrint(Customer obj) {
    System.out.println("Orders of " + obj.getName() + " " + obj.getSurname() + "(" + "Customer " +
            "ID:" + obj.getCustomerID() + ")");
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        System.out.println("Item:" + ordersList.get(count).getItem().getItemName() + " |" + " " +
                "Price Per Unit:" + ordersList.get(count).getItem().getPricePerUnit() + " |" + " " +
                "Quantity:" + ordersList.get(count).getQuantity());
      }
    }
  }

  public static double getImportDutyRate() {
    return IMPORT_DUTY_RATE;
  }

  public static double getYearsBasedDiscountRate1() {
    return YEARS_BASED_DISCOUNT_RATE1;
  }

  public static double getYearsBasedDiscountRate2() {
    return YEARS_BASED_DISCOUNT_RATE2;
  }

  public static int getYearsBasedDiscountMargin1() {
    return YEARS_BASED_DISCOUNT_MARGIN1;
  }

  public static int getYearsBasedDiscountMargin2() {
    return YEARS_BASED_DISCOUNT_MARGIN2;
  }

  public static int getTotalBasedDiscountMargin1() {
    return TOTAL_BASED_DISCOUNT_MARGIN1;
  }

  public static int getTotalBasedDiscountMargin2() {
    return TOTAL_BASED_DISCOUNT_MARGIN2;
  }

  public static double getTotalBasedDiscountRate1() {
    return TOTAL_BASED_DISCOUNT_RATE1;
  }

  public static double getTotalBasedDiscountRate2() {
    return TOTAL_BASED_DISCOUNT_RATE2;
  }

  public void validateOrderDiscountPolicy() {
    if (isYearsBasedDiscount() == true && isTotalValueBasedDiscount() == true) {
      throw new RuntimeException("Only one discount policy must be chosen");
    }
    if (isYearsBasedDiscount() == false && isTotalValueBasedDiscount() == false) {
      throw new RuntimeException("Only one discount policy must be chosen");
    }
  }

  public String getOrderNum() {
    return orderNum;
  }

  public Customer getCustomer() {
    return customer;
  }

  public Item getItem() {
    return item;
  }

  public int getQuantity() {
    return quantity;
  }

  public boolean isYearsBasedDiscount() {
    return yearsBasedDiscount;
  }

  public boolean isTotalValueBasedDiscount() {
    return totalValueBasedDiscount;
  }

}