package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class CurrentAccount extends Account {

  private String accNum;

  private double accBal;

  public CurrentAccount(){
    System.out.println("I am child");
  }

  public CurrentAccount(String customerName, String customerSurname, String accNum, double accBal) {
    super(customerName, customerSurname);
    this.accNum = accNum;
    this.accBal = accBal;
    System.out.println(accBal);
    System.out.println(accNum);
  }

}
