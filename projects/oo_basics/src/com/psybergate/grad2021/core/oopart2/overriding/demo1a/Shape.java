package com.psybergate.grad2021.core.oopart2.overriding.demo1a;

public class Shape {

  /*All shapes have at least two sides that describe them*/
  private int side1;

  private int side2;


  public Shape(int side1, int side2) {
    this.side1 = side1;
    this.side2 = side2;
  }

  public int getArea() {
    int area = 0;
    return area;
  }

  public static void someStaticMethod(){
    System.out.println("Static method");
  }


}
