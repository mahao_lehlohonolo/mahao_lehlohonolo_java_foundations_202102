package com.psybergate.grad2021.core.oopart2.hw5a3_2;

public class LocalOrder extends Order {

  public LocalOrder(Customer customer, String orderNum, Item item, int quantity,
                    boolean yearsBasedDiscount, boolean totalValueBasedDiscount) {
    super(customer, orderNum, item, quantity,yearsBasedDiscount,totalValueBasedDiscount);

    if (customer.isLocal() != true) {
      throw new RuntimeException("Customer must be local for local orders");
    }
  }

}