package com.psybergate.grad2021.core.oopart2.hw5a3_3;

import java.util.List;

import static com.psybergate.grad2021.core.oopart2.hw5a3_3.Customer.getLOCAL_CUSTOMER;

public class LocalOrder extends Order {

  public LocalOrder(Customer customer, String orderNum, List<OrderItem> customerCart,
                    boolean yearsBasedDiscount, boolean totalValueBasedDiscount) {
    super(customer, orderNum, customerCart, yearsBasedDiscount, totalValueBasedDiscount);

    if (! customer.getCustomerType().equalsIgnoreCase(getLOCAL_CUSTOMER())) {
      throw new RuntimeException(customer.getName() + " " + customer.getSurname() + " is " +
              "eligible for local orders only");
    }
  }

}