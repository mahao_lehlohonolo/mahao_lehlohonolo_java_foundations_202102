package com.psybergate.grad2021.core.oopart2.hw5a3_3;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected static List<Order> ordersList = new ArrayList<>(); //should be under customer.

  static DecimalFormat df = new DecimalFormat("###.##");

  //create an abstraction for the discount policies

  private static double YEARS_BASED_DISCOUNT_RATE1;

  private static double YEARS_BASED_DISCOUNT_RATE2;

  private static int YEARS_BASED_DISCOUNT_MARGIN1;

  private static int YEARS_BASED_DISCOUNT_MARGIN2;

  private static int TOTAL_BASED_DISCOUNT_MARGIN1;

  private static int TOTAL_BASED_DISCOUNT_MARGIN2;

  private static double TOTAL_BASED_DISCOUNT_RATE1;

  private static double TOTAL_BASED_DISCOUNT_RATE2;

  private static double IMPORT_DUTY_RATE;

  static {
    IMPORT_DUTY_RATE = 0.045;
    YEARS_BASED_DISCOUNT_RATE1 = 0.075;
    YEARS_BASED_DISCOUNT_RATE2 = 0.125;
    TOTAL_BASED_DISCOUNT_RATE1 = 0.05;
    TOTAL_BASED_DISCOUNT_RATE2 = 0.1;
    YEARS_BASED_DISCOUNT_MARGIN1 = 2;
    YEARS_BASED_DISCOUNT_MARGIN2 = 5;
    TOTAL_BASED_DISCOUNT_MARGIN1 = 500_000;
    TOTAL_BASED_DISCOUNT_MARGIN2 = 1_000_000;
  }

  protected List<OrderItem> customerCart;

  private String orderNum;

  private Customer customer;

  private boolean yearsBasedDiscount;

  private boolean totalValueBasedDiscount;


  public Order(Customer customer, String orderNum, List<OrderItem> customerCart,
               boolean yearsBasedDiscount, boolean totalValueBasedDiscount) {
    this.customer = customer;
    this.orderNum = orderNum;
    this.customerCart = customerCart;
    this.yearsBasedDiscount = yearsBasedDiscount;
    this.totalValueBasedDiscount = totalValueBasedDiscount;
    validateOrderDiscountPolicy();//do this before assignments
  }

  public static double customerOrdersTotal(Customer customer) {
    double total = 0;
    for (int count1 = 0; count1 < ordersList.size(); count1++) {
      Order order = ordersList.get(count1);
      if (order.getCustomer() == customer) {
        for (int count2 = 0; count2 < order.getCustomerCart().size(); count2++) {
          total += order.getCustomerCart().get(count2).getItemTotal();
        }

        if (order.isTotalValueBasedDiscount() == true) {
          if (total > getTotalBasedDiscountMargin1() && total <= getTotalBasedDiscountMargin2()) {
            total = total * (1 - getTotalBasedDiscountRate1());
          }
          if (total > getTotalBasedDiscountMargin2()) {
            total = total * (1 - getTotalBasedDiscountRate2());
          }
        }
        if (order.isYearsBasedDiscount() == true) {
          int numOfYearsASClient = customer.yearsAsCustomer();
          if (numOfYearsASClient > getYearsBasedDiscountMargin1() && numOfYearsASClient <=
                  getYearsBasedDiscountMargin2()) {
            total = total * (1 - getYearsBasedDiscountRate1());
          }
          if (numOfYearsASClient > getYearsBasedDiscountMargin2()) {
            total = total * (1 - getYearsBasedDiscountRate2());
          }
        }
      }
    }
    if (customer.getCustomerType().equalsIgnoreCase(customer.getINTERNATIONAL_CUSTOMER())) {
      total = total * (1 + getImportDutyRate());
    }
    return Double.parseDouble(df.format(total));
  }

  public static void customerOrdersPrint(Customer obj) {
    System.out.println();
    System.out.println("Orders of " + obj.getName() + " " + obj.getSurname() + "(" + "Customer " +
            "ID:" + obj.getCustomerID() + ")");
    for (int count = 0; count < ordersList.size(); count++) {
      if (ordersList.get(count).getCustomer() == obj) {
        for (int counter = 0; counter < ordersList.get(count).customerCart.size(); counter++) {
          System.out.println("Item:" + ordersList.get(count).customerCart.get(counter).getItem().getItemName() + " |" +
                  " " +
                  "Price Per Unit:" + ordersList.get(count).customerCart.get(counter).getItem().getPricePerUnit() + " |" + " " +
                  "Quantity:" + ordersList.get(count).customerCart.get(counter).getQuantity());
        }
      }
    }
  }

  public static double getImportDutyRate() {
    return IMPORT_DUTY_RATE;
  }

  public static double getYearsBasedDiscountRate1() {
    return YEARS_BASED_DISCOUNT_RATE1;
  }

  public static double getYearsBasedDiscountRate2() {
    return YEARS_BASED_DISCOUNT_RATE2;
  }

  public static int getYearsBasedDiscountMargin1() {
    return YEARS_BASED_DISCOUNT_MARGIN1;
  }

  public static int getYearsBasedDiscountMargin2() {
    return YEARS_BASED_DISCOUNT_MARGIN2;
  }

  public static int getTotalBasedDiscountMargin1() {
    return TOTAL_BASED_DISCOUNT_MARGIN1;
  }

  public static int getTotalBasedDiscountMargin2() {
    return TOTAL_BASED_DISCOUNT_MARGIN2;
  }

  public static double getTotalBasedDiscountRate1() {
    return TOTAL_BASED_DISCOUNT_RATE1;
  }

  public static double getTotalBasedDiscountRate2() {
    return TOTAL_BASED_DISCOUNT_RATE2;
  }

  public void validateOrderDiscountPolicy() {
    if (isYearsBasedDiscount() == true && isTotalValueBasedDiscount() == true) {
      throw new RuntimeException("Only one discount policy must be chosen");
    }
    if (isYearsBasedDiscount() == false && isTotalValueBasedDiscount() == false) {
      throw new RuntimeException("Only one discount policy must be chosen");
    }
  }

  public String getOrderNum() {
    return orderNum;
  }

  public Customer getCustomer() {
    return customer;
  }

  public boolean isYearsBasedDiscount() {
    return yearsBasedDiscount;
  }

  public boolean isTotalValueBasedDiscount() {
    return totalValueBasedDiscount;
  }

  public List<OrderItem> getCustomerCart() {
    return customerCart;
  }

}