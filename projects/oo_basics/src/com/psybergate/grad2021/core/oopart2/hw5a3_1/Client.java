package com.psybergate.grad2021.core.oopart2.hw5a3_1;

import java.util.Collections;

import static com.psybergate.grad2021.core.oopart2.hw5a3_1.Order.orderList;

public class Client {

  public static void main(String[] args) {
    Local customer1 = new Local("01","TV",25000,2,"Mahao",4,"Years based");
    International customer2 = new International("02","BED",5500,1,"Some Guy",2,"Total based");

    //lets add the customers in an ArrayList
    Collections.addAll(orderList,customer1,customer2);

    //lets add new orders for the customers
    customer1.addLocalOrder("03","CHAIRS",450,6);
    customer1.addLocalOrder("04","TABLES",1500,2);
    customer2.addInternationalOrder("05","SHEETS",15000,3);
    customer2.addInternationalOrder("06","PILLOWS",400,4);

    //lets print the customer's total price
    System.out.println(customer1.customerOrderTotalPrice(customer1));
    System.out.println(customer2.customerOrderTotalPrice(customer2));

    //lets print a detailed order list for each customer
    System.out.println(customer1.print(customer1));
    System.out.println(customer2.print(customer2));

  }

}
