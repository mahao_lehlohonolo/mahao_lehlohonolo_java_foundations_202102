package com.psybergate.grad2021.core.oopart2.hw2a;

import com.psybergate.grad2021.core.oopart2.hw3a.Customer;

public class Rectangle {

  private static int MAX_LENGTH = 200;

  private static int MAX_WIDTH = 100;

  private static int MAX_AREA = 15000;

  private int length;

  private int width;

  public Rectangle(int length, int width) {
    this.length = length;
    this.width = width;
  }

  public int getLength() {
    return length;
  }

  public int getWidth() {
    return width;
  }

  public int getArea() {
    return length * width;
  }

  public boolean equals(Rectangle rect) {
    if (this == rect) { //reflexivity
      return true;
    }
    if (this.getClass() != rect.getClass()) { //type equivalence
      return false;
    }
    if (rect == null) { //non-nullability
      return false;
    }
//    if (this.length == rect.length && this.width == rect.width) { //
//      return true;
//    }
    return false;
  }


  public void validateRect() {
    if (getLength() > MAX_LENGTH) {
      throw new RuntimeException("Length greater than constraint.");
    }
    if (getWidth() > MAX_WIDTH) {
      throw new RuntimeException("Width greater than constraint.");
    }
    if (getArea() > MAX_AREA) {
      throw new RuntimeException("Area greater than constraint.");
    }

  }

}
