package com.psybergate.grad2021.core.oopart1.ce4a2;

public class Client {

  public static void main(String[] args) {

    SavingsAccount myacc = new SavingsAccount("Tonic", "Mahao", 2547896,1500);
    SavingsAccount myacc1 = new SavingsAccount("Lehlohonolo", "Mahao", 236987,5500);

    CurrentAccount myacc2 = new CurrentAccount("L","Mahao", 2587456,-3625);
    CurrentAccount myacc3 = new CurrentAccount("T","Mahao", 8587456,5625);

    myacc.print();
    myacc1.print();
    myacc2.print();
    myacc3.print();

  }

}
