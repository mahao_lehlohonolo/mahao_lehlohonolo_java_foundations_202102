package com.psybergate.grad2021.core.oopart1.hw4a1;

public class CompanyCustomer extends Customer {

  private String companyRegNum;

  private String companyName;

  public CompanyCustomer(String customerNum, String items, int quantity, int pricePerUnit,
                         String companyRegNum, String companyName) {
    super(customerNum, items, quantity, pricePerUnit);
    this.companyRegNum = companyRegNum;
    this.companyName = companyName;
  }

  public String getName() {
    return companyName;
  }

  public String getCompanyRegNum() {
    return companyRegNum;
  }

}
