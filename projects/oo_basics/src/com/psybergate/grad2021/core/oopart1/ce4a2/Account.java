package com.psybergate.grad2021.core.oopart1.ce4a2;

public class Account {

  private String name;

  private String surname;


  public Account(String name, String surname) {
    this.name = name;
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

}
