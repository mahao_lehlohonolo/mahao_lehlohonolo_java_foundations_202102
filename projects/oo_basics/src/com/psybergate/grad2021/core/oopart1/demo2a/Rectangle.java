package com.psybergate.grad2021.core.oopart1.demo2a;

public class Rectangle extends Shape {

  private double length;

  private double width;

  public Rectangle(double length, double width) {
    this.length = length;
    this.width = width;
  }

  @Override
  public double getArea() {
    return length * width;
  }

  @Override
  public void print() {
    System.out.println("Length: " + length + " Width: " + width + " Area: " + getArea());
  }

}
