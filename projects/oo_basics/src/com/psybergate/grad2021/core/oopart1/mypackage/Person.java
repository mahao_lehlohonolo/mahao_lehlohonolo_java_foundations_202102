package com.psybergate.grad2021.core.oopart1.mypackage;

public class Person {
  private int Age;
  private String Name;
  private String Race;

  public Person(int age, String name, String race) {
    Age = age;
    Name = name;
    Race = race;
  }

  //a static method can also work on object methods
//  public static void aMethod(){
//    System.out.println(new Person(26,"Mahao", "Black").add(2,3));
//  }

  //the add object method is overloaded. How early binding/compile-time binding/static
  // polymorphism works
  public int add(int a, int b) {
    return a + b;
  }

  public int add(int a, int b, int c) {
    return a + b + c;
  }

  public double add(int a, int b, double c) {
    return a - b + c;
  }

  public String getName() {
    return Name;
  }

  public int Salary() {
    return 1000;  //inherited salary if not overridden in a sub-class
  }

  public int AnnualSalary(int monthly_salary) {
    return 12 * monthly_salary;
  }
}
