package com.psybergate.grad2021.core.oopart1.hw5a;

public class CurrentAccount extends CustomerAccounts {

  public CurrentAccount(String customerName, String customerSurname, String currentAccNum,
                        double currentAccBal, String customerNum) {
    super(customerName, customerSurname, currentAccNum, currentAccBal, customerNum);
    addCurrentAccount();
  }

}
