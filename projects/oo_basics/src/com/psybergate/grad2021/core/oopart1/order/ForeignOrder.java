package com.psybergate.grad2021.core.oopart1.order;

import java.time.LocalDate;

public class ForeignOrder extends Order {

  private String whichCountry;

  public ForeignOrder() {

  }

  public ForeignOrder(final String orderNum, final LocalDate orderDate, final String whichCountry) {
    super(orderNum, orderDate);
    this.whichCountry = whichCountry;
  }

  @Override
  public int calcTotal() {
    return 300;
  }

//  public String print() {
//    return "The ForeignOrder with the orderNum = " + getOrderNum() + " has calcTotal() = " + calcTotal();
//  }

  public String orderType() {
    return "ForeignOrder";
  }


}
