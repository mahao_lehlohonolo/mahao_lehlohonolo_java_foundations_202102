package com.psybergate.grad2021.core.oopart1.ce1a;

public class CustomerUtils {

  public static void main(String[] args) {

    Customer obj = new Customer(1121, "Some weirdo", "This weirdo's address");
    obj.addCurrentAccount(100001010, 5680.36);
    System.out.println(obj.totalBalance());

    Customer obj2 = new Customer(2823, "Another weirdo", "The weirdo's address");
    obj2.addCurrentAccount(2555000, 4520);
    obj2.addCurrentAccount(25680230, 8560.25);
    System.out.println(obj2.totalBalance());

    Customer obj3 = new Customer(3254,"Mahao","Address");
    obj3.addCurrentAccount(5647895,1236.67);
    obj3.addCurrentAccount(1457852,2540.01);
    obj3.addCurrentAccount(154789,6584562.99);
    System.out.println(obj3.totalBalance());
  }

}
