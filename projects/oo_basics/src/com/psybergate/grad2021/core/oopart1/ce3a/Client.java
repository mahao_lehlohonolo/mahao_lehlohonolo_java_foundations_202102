package com.psybergate.grad2021.core.oopart1.ce3a;

import java.util.ArrayList;
import java.util.List;

public class Client {

  public static void main(String[] args) {

    //showing type compatibility
    CurrentAccount object1 = new CurrentAccount("Name", "Surname", 1000, 5685246);
    object1.isOverdrawn(); //runs since object type(CurrentAccount) is compatible with the object
    // reference type(Account). Follows the rules of inheritance. it will still work if the
    // object reference type was CurrentAccount, as shown below.

//    CurrentAccount object1 = new CurrentAccount("Name","Surname",1500,564756);

    //seems like object1 cannot change its type as shown in line 15.
    List<CurrentAccount> mylist = new ArrayList<>();
    //If object reference mylist is of type Account, and object1 reference is of type
    // CurrentAccount. Based on inheritance rules, they are type compatible. But if the object
    // reference of mylist is of type CurrentAccount and the reference object1 was of type
    // Account then they are not compatible, wont compile when you invoke List methods.
    //The object references must be of the same type at least.
    mylist.add(object1);

    object1.setName("Nonny");

    System.out.println(object1.getName());

    System.out.println(mylist.size());

  }

}
