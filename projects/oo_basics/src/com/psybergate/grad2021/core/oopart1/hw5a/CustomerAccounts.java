package com.psybergate.grad2021.core.oopart1.hw5a;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomerAccounts {

  List<Double> savingsAccountsBal = new ArrayList<>();

  List<String> savingsAccountsNums = new ArrayList<>();

  List<Double> currentAccountsBal = new ArrayList<>();

  List<String> currentAccountsNums = new ArrayList<>();

  DecimalFormat doubleResultFormat = new DecimalFormat("###.##");

  private String customerName;

  private String customerSurname;

  private String customerNum;

  private String savingsAccNum;

  private int savingsAccBal;

  private String currentAccNum;

  private double currentAccBal;

  public CustomerAccounts(String customerName, String customerSurname, String currentAccNum,
                          double currentAccBal, String customerNum) {
    this(customerName, customerSurname, customerNum);
    this.currentAccNum = currentAccNum;
    this.currentAccBal = currentAccBal;
  }

  public CustomerAccounts(String customerName, String customerSurname, String savingsAccNum,
                          int savingsAccBal, String customerNum) {
    this(customerName, customerSurname, customerNum);
    this.savingsAccNum = savingsAccNum;
    this.savingsAccBal = savingsAccBal;
  }

  public CustomerAccounts(String customerName, String customerSurname,
                          String customerNum) {
    this.customerName = customerName;
    this.customerSurname = customerSurname;
    this.customerNum = customerNum;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void addSavingsAccount() {
    savingsAccountsBal.add((double) savingsAccBal);
    savingsAccountsNums.add(savingsAccNum);
  }

  public void addSavingsAccount(String savingsAccNum, double savingsAccBal) {
    savingsAccountsBal.add(savingsAccBal);
    savingsAccountsNums.add(savingsAccNum);
  }

  public void totalBalance() {
    System.out.println(savingsAccTotalBalance());
    System.out.println(currentAccTotalBalance());
  }

  public String savingsAccTotalBalance() {
    double total = 0;
    for (int count = 0; count < savingsAccountsBal.size(); count++) {
      total += savingsAccountsBal.get(count);
    }
    return "Total savings account/s balance for " + getCustomerName() + " is " + doubleResultFormat.format(total);
  }

  public void addCurrentAccount() {
    currentAccountsBal.add(currentAccBal);
    currentAccountsNums.add(currentAccNum);
  }

  public void addCurrentAccount(String currentAccNum, double currentAccBal) {
    currentAccountsBal.add(currentAccBal);
    currentAccountsNums.add(currentAccNum);
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public void detailedAccPrint() {
    for (int count = 0; count < currentAccountsBal.size(); count++) {
      System.out.println("Customer number: " + getCustomerNum() + " Account number: " + currentAccountsNums.get(count) + " Balance: " + currentAccountsBal.get(count));
      System.out.println("Customer number: " + getCustomerNum() + " Account number: " + savingsAccountsNums.get(count) + " Balance: " + savingsAccountsBal.get(count));
    }
  }

  public String currentAccTotalBalance() {
    double total = 0;
    for (int count = 0; count < currentAccountsBal.size(); count++) {
      total += currentAccountsBal.get(count);
    }
    return "Total current account/s balance for " + getCustomerName() + " is " + doubleResultFormat.format(total);
  }

}
