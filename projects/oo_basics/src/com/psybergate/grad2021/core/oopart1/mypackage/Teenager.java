package com.psybergate.grad2021.core.oopart1.mypackage;

public class Teenager extends Person{
  private String faveSport;

  public int Salary(){
    return 2000;
  }

  public Teenager(int age, String name, String race, String faveSport) {
    super(age, name, race);
    this.faveSport = faveSport;
  }
}
