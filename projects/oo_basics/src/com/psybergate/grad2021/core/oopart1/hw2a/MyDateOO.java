package com.psybergate.grad2021.core.oopart1.hw2a;

public class MyDateOO {

  private int mydate;

  public MyDateOO(int mydate) {
    this.mydate = mydate;
  }

  public static void main(String[] args) {
    MyDateOO mydate1 = new MyDateOO(14022021);
    mydate1.isValid();
    System.out.println(mydate1.addDays(50));
    System.out.println(mydate1.Months());
  }

  public int Days() {
    String someString = String.valueOf(getMydate());
    int days =
            Integer.parseInt(String.valueOf(someString.charAt(0)) + String.valueOf(someString.charAt(1)));
    return days;
  }

  public int Months() {
    String someString = String.valueOf(mydate);
    int months =
            Integer.parseInt(String.valueOf(someString.charAt(2)) + String.valueOf(someString.charAt(3)));
    return months;
  }

  public int Years() {
    String someString = String.valueOf(mydate);
    int years =
            Integer.parseInt(String.valueOf(someString.charAt(4)) + String.valueOf(someString.charAt(5)) + String.valueOf(someString.charAt(6)) + String.valueOf(someString.charAt(7)));
    return years;
  }

  public int getMydate() {
    return mydate;
  }

  public void isValid() {
    int days = Days();
    int months = Months();
    int years = Years();

    if (days > 31 || days == 0) {
      System.out.println("Day is invalid.");
    }
    if (months > 12 || months == 0) {
      System.out.println("Month is invalid.");
    }
    if (years == 0000) {
      System.out.println("Year is invalid.");
    }
    else{
      System.out.println("Date valid.");
    }
  }

  public String addDays(int numberOfDays) {
    int days = Days();
    int months = Months();
    int years = Years();

    int daysChange = days + numberOfDays;
    if (daysChange > 31 && daysChange < 365) {
      while (daysChange > 31) {
        months += 1;
        daysChange -= 31;
      }
      days = daysChange;
    }

    if (daysChange > 365) {
      while (daysChange > 365) {
        years += 1;
        daysChange -= 365;
      }
      days = daysChange;
    }
    return String.valueOf(days) + String.valueOf(months) + String.valueOf(years);
  }

}
