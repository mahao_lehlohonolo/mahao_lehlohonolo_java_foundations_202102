package com.psybergate.grad2021.core.oopart1.order;

import java.time.LocalDate;

public class IndividualOrder extends Order {

  private String name;

  public IndividualOrder() {

  }

  public IndividualOrder(final String orderNum, final LocalDate orderDate, final String name) {
    super(orderNum, orderDate);
    this.name = name;
  }

//  public String print() {
//    return "The IndividualOrder with the orderNum = " + getOrderNum() + " has calcTotal() = " + calcTotal();
//  }

  public String orderType() {
    return "IndividualOrder";
  }
}
