package com.psybergate.grad2021.core.oopart1;

public class Student extends Person {

  private int studentNum;

  private int results;

  public Student(final String name, final int studentNum) {
//    super(name);
    this.name = name;
    this.studentNum = studentNum;
  }



  public int getStudentNum() { // accessor
    return studentNum;
  }

  public void setStudentNum(final int studentNum) { // mutator
    this.studentNum = studentNum;
  }

  private int computeResults() { // mutator
    // some calc
    results = 55;
    return 0;
  }

  public boolean hasPassed() {
    return getResults() > 50;
  }

  private int getResults() {
    return computeResults();
  }
}
