package com.psybergate.grad2021.core.oopart1.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class CurrentAccount extends Customer{

  List<CurrentAccount> currentAccList = new ArrayList<>();

  private final double MAX_OVERDRAFT = 50000;

  private String currentAccNum;

  private double currentAccBal;

  public CurrentAccount(String name, String surname, String customerNum, String currentAccNum,
                        double currentAccBal) {
    super(name, surname, customerNum);
    this.currentAccNum = currentAccNum;
    this.currentAccBal = currentAccBal;
  }

  public String getCurrentAccNum() {
    return currentAccNum;
  }

  public double getCurrentAccBal() {
    return currentAccBal;
  }

  public String printDetails(){
   return getCustomerNum() + getCurrentAccNum() + getCurrentAccBal();
  }


}
