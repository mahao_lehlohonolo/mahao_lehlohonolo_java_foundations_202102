package com.psybergate.grad2021.core.oopart1.mypackage;

public class Child extends Person{

  private int SchoolGrade;

  public int Salary(){ //overriding the Salary method. Also regarded as dynamic polymorphism.
    return 0;
  }

  public Child(int age, String name, String race, int schoolGrade) {
    super(age, name, race);
    SchoolGrade = schoolGrade;
  }
}
