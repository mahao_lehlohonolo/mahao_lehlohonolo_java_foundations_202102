package com.psybergate.grad2021.core.oopart1.mypackage;

public class Adult extends Person{

  private String Occupation;

  public int Salary(){ //overriding the Salary method. Also regarded as dynamic polymorphism.
    return 25000;
  }

  public Adult(int age, String name, String race, String occupation) {
    super(age, name, race);
    Occupation = occupation;
  }
}
