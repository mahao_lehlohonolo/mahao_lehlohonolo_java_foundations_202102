package com.psybergate.grad2021.core.oopart1.demo2a;

public class Client {

  public static void main(String[] args) {
    Circle circrle1 = new Circle(12.0);
    Rectangle rect1 = new Rectangle(25, 34);
    Triangle triangle1 = new Triangle(25, 26);
    rect1.print();
    circrle1.print();
    triangle1.print();
  }

}

//Not sure if the domain was modelled as required but with what I have done, the point was to
// show that overriding a method in the parent class enables some polymorphism to some extent.
