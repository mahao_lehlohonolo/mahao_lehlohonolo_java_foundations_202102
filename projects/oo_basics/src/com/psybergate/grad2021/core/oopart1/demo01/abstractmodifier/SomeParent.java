package com.psybergate.grad2021.core.oopart1.demo01.abstractmodifier;

public abstract class SomeParent { //the class has to be abstract if there is an abstract method.

  protected int someNum;

  public SomeParent(int someNum) {
    this.someNum = someNum;
  }

  public abstract void someMethod(); //an abstract method must have no body.

}
