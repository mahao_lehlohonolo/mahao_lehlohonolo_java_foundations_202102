package com.psybergate.grad2021.core.oopart1.hw4a2;

public class Customer1 {

  private static int MIN_CUSTOMER_AGE = 18;

  private String customerNum;

  private String items;

  private int quantity;

  private int pricePerUnit;

  public Customer1(String customerNum, String items, int quantity, int pricePerUnit) {
    this.customerNum = customerNum;
    this.items = items;
    this.quantity = quantity;
    this.pricePerUnit = pricePerUnit;
  }

  public static int getMinCustomerAge() {
    return MIN_CUSTOMER_AGE;
  }

  public int getPricePerUnit() {
    return pricePerUnit;
  }

  public int getQuantity() {
    return quantity;
  }

  public String getItems() {
    return items;
  }

  public int getPriceTotal() {
    return quantity * pricePerUnit;
  }

  public String getName() {
    return customerNum;
  }

  public String printMethod() {
    return "Total price for " + getQuantity() + " " + getItems() + " for " + getName() + " is " +
            getPriceTotal();
  }

}
