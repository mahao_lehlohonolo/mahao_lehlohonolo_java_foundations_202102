package com.psybergate.grad2021.core.oopart1.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class SavingsAccount extends Customer{

  List<SavingsAccount> savingsAccountList = new ArrayList<>();

  private final double MIN_BALANCE = 5000;

  private String savingsAccNum;

  private double savingsAccBal;

  public SavingsAccount(String name, String surname, String customerNum, String savingsAccNum,
                        double savingsAccBal) {
    super(name, surname, customerNum);
    this.savingsAccNum = savingsAccNum;
    this.savingsAccBal = savingsAccBal;
  }

  public double getSavingsAccBal() {
    return savingsAccBal;
  }

  public String getSavingsAccNum() {
    return savingsAccNum;
  }

  public void addSavingsAcc(){
  }



}
