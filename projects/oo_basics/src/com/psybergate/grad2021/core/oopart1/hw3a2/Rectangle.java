package com.psybergate.grad2021.core.oopart1.hw3a2;

public class Rectangle {

  private static int MAX_LENGTH = 250;

  private static int MAX_WIDTH = 150;

  private static int MAX_AREA = 15000;

  private int length;

  private int width;

  public Rectangle(int length, int width) {
    this.length = length;
    this.width = width;
  }

  public int getLength() {
    return length;
  }

  public int getWidth() {
    return width;
  }

  public int getArea() {
    return length * width;
  }


  public void validateRect() {
    if (getLength() > MAX_LENGTH) {
      throw new RuntimeException("Length greater than constraint.");
    }
    if (getWidth() > MAX_WIDTH) {
      throw new RuntimeException("Width greater than constraint.");
    }
    if (getArea() > MAX_AREA) {
      throw new RuntimeException("Area greater than constraint.");
    }

  }

}
