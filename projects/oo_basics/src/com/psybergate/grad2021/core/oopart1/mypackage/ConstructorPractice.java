package com.psybergate.grad2021.core.oopart1.mypackage;

public class ConstructorPractice {

  private int Age;
  private String Name;

  public ConstructorPractice(int age, String name) {
    Age = age;
    Name = name;
  }

  public String myMethod(int a){
    return "Age is " + a;
  }

  public int setAge(int age) {
    Age = age;
    return age;
  }

  public void setName(String name) {
    Name = name;
  }

  public int getAge() {
    return Age;
  }

  public static void main(String[] args) {
  ConstructorPractice obj = new ConstructorPractice(24,"Mahao");
  System.out.println(obj.getClass().getSimpleName());
  }
}
