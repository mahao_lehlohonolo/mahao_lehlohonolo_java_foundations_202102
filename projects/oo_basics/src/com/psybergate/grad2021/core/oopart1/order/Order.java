package com.psybergate.grad2021.core.oopart1.order;

import java.time.LocalDate;

public abstract class Order {

  private String orderNum;

  private LocalDate orderDate;

  public Order() {
  }

  public Order(final String orderNum, final LocalDate orderDate) {
    this.orderNum = orderNum;
    this.orderDate = orderDate;
  }

  public int calcTotal() {
    return 100;
  }

  public String getOrderNum() {
    return orderNum;
  }

  public String print() { // template pattern
    return "For type: " + orderType() + " with the " + getOrderNum() + " has calcTotal() = " + calcTotal();
  }

  //instead of method orderType() you can use getClass().getSimpleName() to get the name of a
  // class, which is the type of the order we are working with.

  public String orderType() {
    return "Order";
  }

  public static void anyMethod() {
    System.out.println("in static anyMethod");
  }
}
