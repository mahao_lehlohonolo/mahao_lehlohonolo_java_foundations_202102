package com.psybergate.grad2021.core.oopart1.order;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class OrderUtils {

  public static void main(String[] args) {

    IndividualOrder io1 = new IndividualOrder("001",null, null);
    io1.orderType();
      printTotals(Arrays.asList(new IndividualOrder("001", LocalDate.now(), "john"),
          new CompanyOrder("002", LocalDate.now(), "jane"),
          new ForeignOrder("003", LocalDate.now(), "jenny")));
//    printTotal(new IndividualOrder());
  }

  public static void printTotal(Order order) {
    System.out.println(order.print());
  }

  //  public static void printTotal(Order order) {
//    if (order instanceof CompanyOrder) {
//      System.out.println("CompanyOrder with orderNum = " + order.getOrderNum() + " has calcTotal() = " + order.calcTotal());
//    }
//    if (order instanceof IndividualOrder) {
//      System.out.println("IndividualOrder with orderNum = " + order.getOrderNum() + " has calcTotal() = " + order.calcTotal());
//    }
//    if (order instanceof ForeignOrder) {
//      System.out.println("ForeignOrder with orderNum = " + order.getOrderNum() + " has calcTotal() = " + order.calcTotal());
//    }
//  }

  public static void printTotals(List<Order> orders) {
    for (Order order : orders) {
      printTotal(order);
    }
  }

  //  public static void printTotal(IndividualOrder order) {
//    System.out.println("order.calcTotal() = " + order.calcTotal());
//  }
//
//  public static void printTotal(CompanyOrder order) {
//    System.out.println("order.calcTotal() = " + order.calcTotal());
//  }
//
//  public static void printTotal(ForeignOrder order) {
//    System.out.println("order.calcTotal() = " + order.calcTotal());

//  }


}
