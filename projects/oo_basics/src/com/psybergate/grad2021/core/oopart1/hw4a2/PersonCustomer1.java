package com.psybergate.grad2021.core.oopart1.hw4a2;

public class PersonCustomer1 extends Customer1 {


  private String name;

  private int age;

  public PersonCustomer1(String customerNum, String items, int quantity, int pricePerUnit,
                        String name, int age) {
    super(customerNum, items, quantity, pricePerUnit);
    this.name = name;
    this.age = age;
    customerValidity();
  }

  public String getName() {
    return name;
  }


  public void customerValidity() {
    if(age<getMinCustomerAge()){
      throw new RuntimeException("Customer age is below " + getMinCustomerAge());
    }
  }

}
