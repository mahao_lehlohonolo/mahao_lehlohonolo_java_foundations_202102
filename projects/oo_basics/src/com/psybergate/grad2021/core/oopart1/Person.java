package com.psybergate.grad2021.core.oopart1;

public class Person {

  protected String name;

  public Person() {
  }

  public Person(final String name) {
    this.name = name;
  }

  public static void main(String[] args) {
    new Person("john");
  }
}
