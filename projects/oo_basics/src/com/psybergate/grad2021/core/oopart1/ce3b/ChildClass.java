package com.psybergate.grad2021.core.oopart1.ce3b;

public class ChildClass extends ParentClass{

  private int number1;

  public ChildClass(int number, String name, int number1) {
    super(number, name); //referring to the parent's object variables.
    this.number1 = number1;//referring to the object variable number1 in the scope.
  }

  public int someMethod(){
    return super.someMethod(45);//calls the toAString method in the parent
  }

  public String toAString(){
    return toString(25);//calls the toString method in the parent.Cannot call the toString method
    // that is from java.lang.Object directly.
  }

  public int getNumber1() {
    return number1;
  }

}
