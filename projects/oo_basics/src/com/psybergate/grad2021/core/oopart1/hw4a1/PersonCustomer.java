package com.psybergate.grad2021.core.oopart1.hw4a1;

public class PersonCustomer extends Customer {

  private String name;

  private int age;

  public PersonCustomer(String customerNum, String items, int quantity, int pricePerUnit,
                        String name, int age) {
    super(customerNum, items, quantity, pricePerUnit);
    this.name = name;
    this.age = age;
    customerValidity();
  }

  public String getName() {
    return name;
  }


  public void customerValidity() {
    if(age<getMinCustomerAge()){
      throw new RuntimeException("Customer age is below " + getMinCustomerAge());
    }
  }

}
