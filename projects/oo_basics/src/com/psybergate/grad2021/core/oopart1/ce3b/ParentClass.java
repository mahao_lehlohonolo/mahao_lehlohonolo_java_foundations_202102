package com.psybergate.grad2021.core.oopart1.ce3b;

public class ParentClass {

  private int number;
  private String name;
  private int somenumber;

  public ParentClass(int number, String name,int somenumber) {
    this(number,name);//delegates to the other constructor
    this.somenumber = somenumber;
  }

  public ParentClass(int number, String name) {
    this.number = number;
    this.name = name;
  }

  public int someMethod(int num){
    return num;
  }

  public String toString(int number2){
    return toString(number2);
  }

}
