package com.psybergate.grad2021.core.oopart1.ce1a;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  List<Double> currentAccounts = new ArrayList<>();

  DecimalFormat doubleResultFormat = new DecimalFormat("###.##");

  private int customerNum;

  private String name;

  private String address;

  private int accountNum;

  private double balance;

  public Customer(int customerNum, String name, String address) {
    this.customerNum = customerNum;
    this.name = name;
    this.address = address;
  }

  public void addCurrent(int accountNum, double balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public String getName() {
    return name;
  }

  public void addCurrentAccount(int accountNum, double balance) {
    currentAccounts.add((double) accountNum);
    currentAccounts.add(balance);
  }

  public String totalBalance() {
    double total = 0;
    for (int count = 1; count < currentAccounts.size(); count += 2) {
      total += currentAccounts.get(count);
    }
    return "Total current account/s balance for " + getName() + " is " + doubleResultFormat.format(total);
  }

//  public static void main(String[] args) {
//    testMethod();
//  }

  //  public static void testMethod() {
//    CustomerUtils.main();
//  }

}
