package com.psybergate.grad2021.core.oopart1.ce2a;

public class Account {

  private static int MAX_OVERDRAFT = 100000;

  private static int MAX_OVERDRAFT_BALANCE = - MAX_OVERDRAFT;

  private String name;

  private String surname;

  private int balance;

  public Account(String name, String surname, int balance) {
    this.name = name;
    this.surname = surname;
    this.balance = balance;
  }

  public void isOverdrawn() {
    boolean result;
    if (balance < 0) {
      result = true;
    } else {
      result = false;
    }
    System.out.println(needsToBeReviewed());
  }

  public String needsToBeReviewed() {
    boolean results;
    if (balance < - 0.2 * MAX_OVERDRAFT || balance <= - 50000) {
      results = true;
    } else {
      results = false;
    }
    return "Account needs to be reviewed? " + results;
  }

  public String getAccountType(){
    return getClass().getSimpleName();
  }


}
