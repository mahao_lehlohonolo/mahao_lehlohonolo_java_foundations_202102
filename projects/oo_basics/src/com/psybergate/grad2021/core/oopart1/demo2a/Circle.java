package com.psybergate.grad2021.core.oopart1.demo2a;

public class Circle extends Shape {

  private double radius;

  public Circle(double radius) {
    this.radius = radius;
  }

  @Override
  public double getArea() {
    return Math.PI * radius * radius;
  }

  @Override
  public void print() {
    System.out.println("Radius: " + radius + " Area: " + getArea());
  }

}
