package com.psybergate.grad2021.core.oopart1.ce4a2;

public class CurrentAccount extends Account {

  private static final int MAX_OVERDRAFT = 50000;

  private static int MAX_OVERDRAFT_BALANCE = - MAX_OVERDRAFT;

  private int currentAccNum;

  private int currentAccBal;

  public CurrentAccount(String name, String surname, int currentAccNum, int currentAccBal) {
    super(name, surname);
    this.currentAccNum = currentAccNum;
    this.currentAccBal = currentAccBal;
  }

  public int getCurrentAccBal() {
    return currentAccBal;
  }

  public int getCurrentAccNum() {
    return currentAccNum;
  }

  public boolean needsToBeReviewed() {
    boolean results;
    if (currentAccBal < - 0.2 * MAX_OVERDRAFT || currentAccBal <= MAX_OVERDRAFT_BALANCE) {
      results = true;
    } else {
      results = false;
    }
    return results;
  }

  public boolean isOverdrawn() {
    boolean result;
    if (currentAccBal < 0) {
      result = true;
    } else {
      result = false;
    }
    return result;
  }

  public void print() {
    System.out.println("isOverdrawn? " + isOverdrawn() + " Needs" +
            "ToBe" +
            "Reviewed? " + needsToBeReviewed());
  }

}
