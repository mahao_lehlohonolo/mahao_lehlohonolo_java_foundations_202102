package com.psybergate.grad2021.core.oopart1.order;

import java.time.LocalDate;

public class CompanyOrder extends Order {

  private String companyRegNum = "a001";

  public CompanyOrder() {

  }

  public CompanyOrder(final String orderNum, final LocalDate orderDate, final String companyRegNum) {
    super(orderNum, orderDate);
    this.companyRegNum = companyRegNum;
  }

  public int calcTotal() {
    return 50;
  }

  public String getCompanyRegNum() {
    return companyRegNum;
  }

//  public String print() {
//    return "The CompanyOrder with the orderNum = " + getOrderNum() + " has calcTotal() = " + calcTotal();
//  }

  public String orderType() {
    return "CompanyOrder";
  }
}
