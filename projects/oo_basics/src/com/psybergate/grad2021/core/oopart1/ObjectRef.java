package com.psybergate.grad2021.core.oopart1;

public class ObjectRef {

  public static void main(String[] args) {
    int i = 0;
    int j = 5 / i;

    String s = null;
    s.concat("def"); // s is a String

    Person p = new Student("john",1); // is this a valid assignment
//    Person p1 = new String("abc"); // // does not compile as you cannot assign a String to a Person
//    p.getStudentNum(); // does not compile as p is of type Person as far as the compiler is concerned
  }

}
