package com.psybergate.grad2021.core.oopart1.hw4a2;

public class CompanyCustomer1 extends Customer1 {

  private String companyRegNum;

  private String companyName;

  public CompanyCustomer1(String customerNum, String items, int quantity, int pricePerUnit,
                         String companyRegNum, String companyName) {
    super(customerNum, items, quantity, pricePerUnit);
    this.companyRegNum = companyRegNum;
    this.companyName = companyName;
  }

  public String getName() {
    return companyName;
  }

  public String getCompanyRegNum() {
    return companyRegNum;
  }

}
