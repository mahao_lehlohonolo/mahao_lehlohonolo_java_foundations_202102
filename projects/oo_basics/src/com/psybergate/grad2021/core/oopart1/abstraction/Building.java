package com.psybergate.grad2021.core.oopart1.abstraction;

public class Building {

  /**
   * unique identifier
   */
  private int buildingNum;

  private int height;

  public Building(final int buildingNum, final int height) {
    this.buildingNum = buildingNum;
    this.height = height;
  }

  public static void main(String[] args) {
    new Building(1, 10); // store in a db
    new Building(1, 100); // store in a db - fail
  }
}
