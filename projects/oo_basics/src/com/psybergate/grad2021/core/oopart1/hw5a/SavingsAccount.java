package com.psybergate.grad2021.core.oopart1.hw5a;

public class SavingsAccount extends CustomerAccounts {

//  private static int MIN_BALANCE = 5000;


  public SavingsAccount(String customerName, String customerSurname, String savingsAccNum,
                        double savingsAccBal, String customerNum) {
    super(customerName, customerSurname, savingsAccNum, savingsAccBal, customerNum);
    addSavingsAccount();
  }

}
