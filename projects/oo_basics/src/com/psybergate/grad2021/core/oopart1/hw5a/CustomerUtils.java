package com.psybergate.grad2021.core.oopart1.hw5a;

public class CustomerUtils {

  public static void main(String[] args) {

    CustomerAccounts customer1 = new SavingsAccount("nonny","mahao","584785",536.12,"4587");
//    detailedPrinter(customer1);
    printBalances(customer1);

//    CustomerAccounts customer2 = new CurrentAccount("Name2", "Mahao", "000", 0.0, "1547", 1254.36,
//            "5687");
//    customer2.addSavingsAccount("125478", 23654.1);
//    customer2.addSavingsAccount("125478", 56234);
//    customer2.addCurrentAccount("125478", 56234);
//    detailedPrinter(customer2);
//    printBalances(customer2);

  }

  public static void printBalances(CustomerAccounts customer) {
    customer.totalBalance();
  }

  public static void detailedPrinter(CustomerAccounts customer) {
    customer.detailedAccPrint();
  }

}
