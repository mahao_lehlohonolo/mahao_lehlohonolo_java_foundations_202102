package com.psybergate.grad2021.core.oopart1.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  List<Customer> customerList = new ArrayList<>();

  private String name;

  private String surname;

  private String customerNum;

  public Customer(String name, String surname, String customerNum) {
    this.name = name;
    this.surname = surname;
    this.customerNum = customerNum;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

}
