package com.psybergate.grad2021.core.oopart1;

public class Employee  extends Person {

  private String employeeNum;

  public Employee(final String name, final String employeeNum) {
    super(name);
    this.employeeNum = employeeNum;
  }
}
