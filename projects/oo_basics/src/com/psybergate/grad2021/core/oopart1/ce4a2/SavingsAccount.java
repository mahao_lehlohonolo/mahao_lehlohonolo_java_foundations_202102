package com.psybergate.grad2021.core.oopart1.ce4a2;

public class SavingsAccount extends Account {

  private final int MIN_SAVINGS_BALANCE = 5000;

  private final int MIN_SAVINGS_BALANCE2 = 2000;

  private int savingsAccNum;

  private int savingsAccBal;


  public SavingsAccount(String name, String surname, int savingsAccNum, int savingsAccBal) {
    super(name, surname);
    this.savingsAccNum = savingsAccNum;
    this.savingsAccBal = savingsAccBal;
  }

  public int getSavingsAccBal() {
    return savingsAccBal;
  }

  public int getSavingsAccNum() {
    return savingsAccNum;
  }

  public boolean isOverdrawn() {
    boolean result = false;
    if (savingsAccBal < MIN_SAVINGS_BALANCE) {
      result = true;
    }
    return result;
  }

  public boolean needsToBeReviewed() {
    boolean result1 = false;
    isOverdrawn();
    if (savingsAccBal < MIN_SAVINGS_BALANCE2) {
      result1 = true;
    }
    return result1;
  }

  public void print() {
    System.out.println("isOverdrawn? " + isOverdrawn() + " Needs" +
            "ToBe" +
            "Reviewed? " + needsToBeReviewed());
  }

}
