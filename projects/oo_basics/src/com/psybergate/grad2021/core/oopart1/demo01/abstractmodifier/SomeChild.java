package com.psybergate.grad2021.core.oopart1.demo01.abstractmodifier;

public class SomeChild extends SomeParent {

  public SomeChild(int someNum) {
    super(someNum);
  }

  @Override
  public void someMethod() {
    System.out.println(someNum);
    //the abstract method in the parent class is overridden in the child class. It must be
    // completed.
  }

}
