package com.psybergate.grad2021.core.oopart1.ce3a;


public class CurrentAccount extends Account {

  private int accountNum;

  public CurrentAccount(String name, String surname, int balance, int accountNum) {
    super(name, surname, balance);
    this.accountNum = accountNum;
  }

}
