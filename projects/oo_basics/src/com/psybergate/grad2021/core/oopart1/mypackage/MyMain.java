package com.psybergate.grad2021.core.oopart1.mypackage;

import java.util.Arrays;
import java.util.List;

//import static com.psybergate.grad2021.core.core.oopart1.mypackage.Person.aMethod;

public class MyMain {
  public static void main(String[] args) {

    List<Person> object = Arrays.asList(new Child(10,"Noncedo","Black",2),new Adult(30,"Mahao",
            "African","Worker"),new Teenager(13,"Lesedi","Blacky","Soccer"));

    for(Person counter:object){
      if(counter instanceof Adult){
      System.out.println(counter.getName() + " is an " + counter.getClass().getSimpleName() + " " +
              "with " + "annual salary of " + counter.AnnualSalary(counter.Salary()));
      }else{
        System.out.println(counter.getName() + " is a " + counter.getClass().getSimpleName() + " " +
                "with " + "annual salary of " + counter.AnnualSalary(counter.Salary()));
      }
    }



/**
    Person child1 = new Child(10, "Lesedi", "Black", 2);
    System.out.println(child1.Salary());
    System.out.println(child1.getName() + " is a " + child1.getClass().getSimpleName() + " with " +
            "annual salary of " + child1.AnnualSalary(child1.Salary()));
    System.out.println(child1.add(2, 3, 5.4)); //static polymorphism illustration, see the add
    // method in Person class. The method is overloaded.

    Adult adult1 = new Adult(25, "MaMahao", "Black", "Employment");
    System.out.println(adult1.getName() + " is an " + adult1.getClass().getSimpleName() + " with " +
            "annual salary of " + adult1.AnnualSalary(adult1.Salary()));
*/
  }
}
