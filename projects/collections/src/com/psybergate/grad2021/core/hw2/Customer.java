package com.psybergate.grad2021.core.hw2;

import java.util.Objects;

public class Customer implements Comparable {

  private int customerNum;

  private String name;

  private String surname;

  public Customer(int customerNum, String name, String surname) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return this.customerNum == customer.customerNum;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.customerNum);
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum=" + customerNum +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            '}'+"hashcode:"+hashCode();
  }

  public String print() {
    return toString();
  }

  @Override
  public int compareTo(Object o) {
    Customer c = (Customer) o;
    return (this.customerNum - c.customerNum);
  }

}
