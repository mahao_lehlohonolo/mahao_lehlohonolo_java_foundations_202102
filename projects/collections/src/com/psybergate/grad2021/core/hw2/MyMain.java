package com.psybergate.grad2021.core.hw2;

import java.util.HashMap;
import java.util.Map;

public class MyMain {

  public static void main(String[] args) {
    Map<Customer, Integer> m1 = new HashMap<Customer, Integer>();
    Customer c1 = new Customer(01, "Nonny", "Mahao");
    Customer c2 = new Customer(01, "Tonic", "Mahao");
    Customer c3 = new Customer(03, "MaNonz", "Mahao");
    Customer c4 = new Customer(04, "MaTonz", "Mahao");

    //the customer number is used as the key, its used to calc the hashcode.
    //the customer objects are used
    m1.put(c1,c1.getCustomerNum());
    m1.put(c2,c2.getCustomerNum());
    m1.put(c3,c3.getCustomerNum());
    m1.put(c4,c4.getCustomerNum());

    //returns the values in the map
    System.out.println("m1.values() = " + m1.values());
    //returns the keySet in the map
    System.out.println("m1.keySet() = " + m1.keySet());

    for (Customer customer : m1.keySet()) {
      System.out.println("customer.print() = " + customer.print());

    }

  }


}
