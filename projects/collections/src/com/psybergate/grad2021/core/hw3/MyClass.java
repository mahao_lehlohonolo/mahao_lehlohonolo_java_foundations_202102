package com.psybergate.grad2021.core.hw3;

import java.util.SortedSet;
import java.util.TreeSet;

public class MyClass {

  public static void main(String[] args) {
    String s1 = "Mahao";
    String s2 = "Tonic";
    String s3 = "Nonny";
    String s4 = "Nonny";

    SortedSet set = new TreeSet(new MyComparator());
    set.add(s1);
    set.add(s2);
    set.add(s3);
    set.add(s4);//duplicates not allowed in a Set

    System.out.println("s4.hashCode() = " + s4.hashCode());
    for (Object o : set) {
      String s = (String) o;
      System.out.println("hashcode(" + s.hashCode() +") "+ s);
    }
  }

}
