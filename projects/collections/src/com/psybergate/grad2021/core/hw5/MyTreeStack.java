package com.psybergate.grad2021.core.hw5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.EmptyStackException;
import java.util.Iterator;

public class MySortedStack extends MyTreeStack {

  private static int DEFAULT_SIZE;

  private static int INDEX;

  static {
    DEFAULT_SIZE = 100;
    INDEX = 0;
  }

  private Object[] elements;

  public MySortedStack(Comparator comparator) {
    super(comparator);
    elements = new Object[DEFAULT_SIZE];
  }

  public MySortedStack() {
    super();
    elements = new Object[DEFAULT_SIZE];
  }

  public Object push(Object object) {
    add(object);
    return object;
  }

  public boolean add(Object object) {
    if (INDEX < DEFAULT_SIZE) {
      elements[INDEX++] = object;
      return true;
    }
    if (INDEX >= DEFAULT_SIZE) {
      Object[] elementsNew = new Object[DEFAULT_SIZE + 1];
      int newIndex = 0;
      for (Object element : elements) {
        if (element != null) {
          elementsNew[newIndex++] = element;
        }
      }
      elements = elementsNew;
      elements[size() - 1] = object;
      return true;
    }
    return false;
  }

  public Object pop() {
    Object object = elements[size() - 1];
    elements[size() - 1] = null;
    return object;
  }

  public Object get(int index) {
    return elements[index];
  }

  public Object peek() {
    if (size() == 0) {
      throw new EmptyStackException();
    }
    return elements[size() - 1];
  }

  public boolean contains(Object object) {
    for (Object element : elements) {
      if (element == object) {
        return true;
      }
    }
    return false;
  }

  public int size() {
    int size = 0;
    Iterator iterator = Arrays.stream(elements).iterator();
    while (iterator.next() != null) {
      size++;
    }
    return size;
  }

  public Iterator iterator() {
    return new MyIterator(this.elements);
  }

}
