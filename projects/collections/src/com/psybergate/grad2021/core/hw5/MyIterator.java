package com.psybergate.grad2021.core.ce1;

import java.util.Arrays;
import java.util.Iterator;

public class MyIterator implements Iterator {

  Object[] objects;

  int index;

  public MyIterator(Object[] objects) {
    this.objects = objects;
    index = size() - 1;
  }

  public int size() {
    int size = 0;
    Iterator iterator = Arrays.stream(objects).iterator();
    while (iterator.next() != null) {
      size++;
    }
    return size;
  }

  @Override
  public boolean hasNext() {

    if (index >= 0 && index <= objects.length - 1) {
      return true;
    }
    return false;
  }

  @Override
  public Object next() {
    if (hasNext() == true) {
        return objects[index--];
    }
    return null;
  }

}
