package com.psybergate.grad2021.core.ce1;

import java.util.Iterator;

public class MyStackClient {

  public static void main(String[] args) {
    MyStack s1 = new MyStack();
    Object o1 = "a";
    Object o2 = "b";
    Object o3 = "c";
    Object o4 = "d";
    Object o5 = "e";
    Object o6 = "f";
    Object o7 = "g";
    Object o8 = "h";
    Object o9 = "i";
    Object o10 = "j";
    Object o11 = "k";
    Object o12 = "l";
    Object o13 = "m";

    s1.push(o1);
    s1.push(o2);
    s1.push(o3);
    s1.push(o4);
    s1.push(o5);
    s1.push(o6);
    s1.push(o7);
    s1.push(o8);
    s1.push(o9);
    s1.push(o10);
    s1.push(o11);
    s1.push(o12);
    s1.push(o13);

//    System.out.println("stack1.contains(o12) = " + s1.contains(o12));
//    System.out.println("stack1.contains(o11) = " + stack1.contains(o11));
//    System.out.println("stack1.contains(o13) = " + stack1.contains(o13));
    System.out.println("stack1.size() = " + s1.size());
    for (Iterator iterator = s1.iterator(); iterator.hasNext(); ) {
      Object next =  iterator.next();
      System.out.println(next);
    }
//    for (Object object : s1) {
//      System.out.println(object);
//    }
  }
}
