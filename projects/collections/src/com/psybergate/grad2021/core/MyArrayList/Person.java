package com.psybergate.grad2021.core.MyArrayList;

public class Person {

  private String name;

  private int age;

  public Person(String name, int age) {
    this.name = name;
    this.age = age;
  }

  @Override
  public String toString() {
    return "MyClass{" +
            "name='" + name + '\'' +
            ", num=" + age +
            '}';
  }

}
