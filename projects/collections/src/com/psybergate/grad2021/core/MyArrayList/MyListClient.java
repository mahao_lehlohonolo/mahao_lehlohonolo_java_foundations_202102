package com.psybergate.grad2021.core.MyArrayList;

public class MyListClient {

  public static void main(String[] args) {
    MyList l1 = new MyList();
    l1.myAdd(1);
    l1.myAdd(2);
    l1.myAdd(3);
    l1.myAdd("Mahao");
    l1.myAdd('c');
    l1.myAdd(8,new Person("Mahao",24));
    l1.myAdd(2,15);
    l1.myAdd(6,'M');
    l1.myAdd(0, "Nonny");
    l1.myAdd(new Person("Ncedo",22));
    System.out.println(l1.size());

    //if possible, I need to find out how I can modify a for-each loop.
    //for-each loop is implemented because we implemented Iterable interface.
    Object[] myArray = l1.getMyArray();

    for (Object obj : myArray) {
      if (obj != null) {
        System.out.println("obj = " + obj);
      }
    }
  }

}

