package com.psybergate.grad2021.core.MyArrayList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * Mimicking how an ArrayList works, made it take any type by using the Object class array.
 */

public class MyList implements Iterable {

  private static int DEFAULT_SIZE;

  private static int ARR_INDEX;

  static {
    DEFAULT_SIZE = 10;
    ARR_INDEX = 0;
  }

  private int newSize = DEFAULT_SIZE;

  private Object[] MyArray = new Object[DEFAULT_SIZE];

  public MyList() {
  }

  public Object[] getMyArray() {
    return MyArray;
  }

  //adding at an int at a specific position
  public void myAdd(int index, int integer) {
    newSize++;
    Object[] anotherMyArray = new Object[(int) (newSize)];
    MyFor:
    for (int i = 0; i < anotherMyArray.length; i++) {
      if (i == index) {
        anotherMyArray[index] = integer;
        for (int n = i; n < MyArray.length; n++) {
          anotherMyArray[n + 1] = MyArray[n];
        }
        break MyFor;
      }
      anotherMyArray[i] = MyArray[i];
    }
    MyArray = anotherMyArray;
  }

  //adding at a String at a specific position
  public void myAdd(int index, String string) {
    newSize++;
    Object[] anotherMyArray = new Object[(int) (newSize)];
    MyFor:
    for (int i = 0; i < anotherMyArray.length; i++) {
      if (i == index) {
        anotherMyArray[index] = string;
        for (int n = i; n < MyArray.length; n++) {
          anotherMyArray[n + 1] = MyArray[n];
        }
        break MyFor;
      }
      anotherMyArray[i] = MyArray[i];
    }
    MyArray = anotherMyArray;
  }

  //adding at a char at a specific position
  public void myAdd(int index, char character) {
    newSize++;
    Object[] anotherMyArray = new Object[(int) (newSize)];
    MyFor:
    for (int i = 0; i < anotherMyArray.length; i++) {
      if (i == index) {
        anotherMyArray[index] = character;
        for (int n = i; n < MyArray.length; n++) {
          anotherMyArray[n + 1] = MyArray[n];
        }
        break MyFor;
      }
      anotherMyArray[i] = MyArray[i];
    }
    MyArray = anotherMyArray;
  }

  //adding at an Object at a specific position
  public void myAdd(int index, Object object) {
    newSize++;
    Object[] anotherMyArray = new Object[(int) (newSize)];
    MyFor:
    for (int i = 0; i < anotherMyArray.length; i++) {
      if (i == index) {
        anotherMyArray[index] = object;
        for (int n = i; n < MyArray.length; n++) {
          anotherMyArray[n + 1] = MyArray[n];
        }
        break MyFor;
      }
      anotherMyArray[i] = MyArray[i];
    }
    MyArray = anotherMyArray;
  }

  public boolean myAdd(Object obj) {
    MyArray[ARR_INDEX++] = obj;
    return true;
  }

  public boolean myAdd(int integer) {
    MyArray[ARR_INDEX++] = integer;
    return true;
  }

  public boolean myAdd(String string) {
    MyArray[ARR_INDEX++] = string;
    return true;
  }

  public boolean myAdd(char character) {
    MyArray[ARR_INDEX++] = character;
    return true;
  }

  public int size() {
    int s = 0;
    for (int i = 0; i < MyArray.length; i++) {
      if (MyArray[i] != null) {
        s++;
      }
    }
    return s;
  }

  @Override
  public String toString() {
    return "MyList{" +
            "MyArray=" + Arrays.toString(MyArray) +
            '}';
  }

  @Override
  public Iterator<Integer> iterator() {
    return null;
  }

  @Override
  public void forEach(Consumer action) {
  }

}
