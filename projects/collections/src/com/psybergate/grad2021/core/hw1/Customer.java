package com.psybergate.grad2021.core.hw1;

public class Customer implements Comparable {

  private int customerNum;

  private String name;

  private String surname;

  public Customer(int customerNum, String name, String surname) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum=" + customerNum +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            '}';
  }

  public String print() {
    return toString();
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  @Override
  public int compareTo(Object o) {
    Customer c = (Customer) o;
    return (this.customerNum - c.customerNum);
  }

}
