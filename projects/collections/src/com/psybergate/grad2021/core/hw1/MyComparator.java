package com.psybergate.grad2021.core.hw1;

import java.util.Comparator;

public class MyComparator implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {
    Customer c1 = (Customer) o1;
    Customer c2 = (Customer) o2;
    return c1.compareTo(c2);
  }

}
