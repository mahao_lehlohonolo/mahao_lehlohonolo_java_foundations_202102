package com.psybergate.grad2021.core.hw1;

import java.util.SortedSet;
import java.util.TreeSet;

public class MyMain {

  public static void main(String[] args) {

    SortedSet s1 = new TreeSet(new MyComparator());
    Customer c1 = new Customer(01,"Nonny","Mahao");
    Customer c2 = new Customer(02,"Tonic","Mahao");
    Customer c3 = new Customer(03,"MaNonz","Mahao");
    Customer c4 = new Customer(04,"MaTonz","Mahao");

    s1.add(c1);
    s1.add(c2);
    s1.add(c3);
    s1.add(c4);

    for (Object object : s1) {
      Customer c = (Customer) object;
      System.out.println("c.print() = " + c.print());
    }
  }

}
