package com.psybergate.grad2021.core.hw4.singlyLinkedList;

public class NextField {

  //the NextField contains the next node in the list
  private Node nextNode;

  private static String node = "node";

  public NextField() {
  }

  public NextField(Node nextNode) {
    this.nextNode = nextNode;
  }

  @Override
  public String toString() {
    return "NextField{" +
            "nextNode=" + nextNode +
            '}';
  }

}
