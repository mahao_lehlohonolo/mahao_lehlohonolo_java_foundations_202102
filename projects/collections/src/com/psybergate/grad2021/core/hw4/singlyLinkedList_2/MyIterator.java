package com.psybergate.grad2021.core.hw4.singlyLinkedList_2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyIterator implements Iterator {

  private Node currentNode;

//  private MyLinkedList list;

  //can pass in a list  as well.
  public MyIterator(Node head) {
//    this.list = list;
    this.currentNode = head; //tells it where to begin its iteration in the list
  }

  @Override
  public boolean hasNext() {
    if (currentNode != null && currentNode.getNextNode() != null) {
      return true;
    }
    return false;
  }

  @Override
  public Object next() {
    if (hasNext() == false) {
      throw new NoSuchElementException();
    }
    Node node = currentNode.getNextNode();
    currentNode = node;
    return currentNode.getData();
  }

}
