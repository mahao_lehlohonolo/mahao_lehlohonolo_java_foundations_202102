package com.psybergate.grad2021.core.hw4.singlyLinkedList;

import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.function.Consumer;

public class Node extends AbstractSequentialList implements Iterable {

  //a node contains a dataField and a nextField
  private int dataField;

  //I can make the nextField show the actual next node chain.
  private String nextField;

  public Node(int dataField, String nextField) {
    this.dataField = dataField;
    this.nextField = nextField;
  }

  public void setNextField(String nextField) {
    this.nextField = nextField;
  }

  public int getDataField() {
    return dataField;
  }

  @Override
  public String toString() {
    return "Node{" +
            "Data=" + dataField +
            ", NextNode=" + nextField +
            '}';
  }

  @Override
  public Iterator iterator() {
    return null;
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public ListIterator listIterator(int index) {
    return null;
  }

  @Override
  public void forEach(Consumer action) {

  }

}
