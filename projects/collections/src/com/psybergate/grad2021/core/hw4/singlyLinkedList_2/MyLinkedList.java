package com.psybergate.grad2021.core.hw4.singlyLinkedList_2;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;

public class MyLinkedList implements List {

  Node head;

  Node currentNode;

  public MyLinkedList() {
  }

  @Override
  public boolean add(Object object) {
    if (head == null) {
      head = new Node(null, null);
      currentNode = head;
      Node newNode = new Node(object, null);
      currentNode.setNextNode(newNode);
      currentNode = newNode;
    } else {
      Node newNode = new Node(object, null);
      currentNode.setNextNode(newNode);
      currentNode = newNode;
    }
    return true;
  }

  @Override
  public void add(int index, Object element) {
    if (index > this.size() - 1 || index < 0) {
      throw new IndexOutOfBoundsException("index = " + index + " is not valid");
    }
    if (element == null) {
      throw new IllegalArgumentException("Null element not allowed");
    }
    int count = - 1;
    Node currentNode = this.head;
    Node node = null;
    Node leftNode = null;
    Node rightNode = null;
//    for (Iterator i = this.iterator(); i.hasNext(); ) {
//      Object o = i.next();
//      Object o1 = new Node(null,null);
//      Object o1 = new String(" ");
//      Node node = (Node) o1;
//    }
    Iterator iterator = this.iterator();
    while (iterator.hasNext()) {
      node = currentNode.getNextNode();
      currentNode = node;
      count++;
      if (index == 0 && count == index) {
        leftNode = this.head;
        rightNode = currentNode;
      }
      if (count == index - 1) {
        leftNode = currentNode;
        System.out.println("leftNode.getData() = " + leftNode.getData());
      }
      if (count == index) {
        rightNode = currentNode;
        System.out.println("rightNode.getData() = " + rightNode.getData());
        break;
      }
    }
    Node newNode = new Node(element, null);
    leftNode.setNextNode(newNode);
    newNode.setNextNode(rightNode);
  }

  @Override
  public Object set(int index, Object element) {
    if (this.isEmpty() == true) {
      throw new RuntimeException("List is empty");
    }
    if (index > this.size() - 1 || index < 0) {
      throw new IndexOutOfBoundsException("index = " + index + " is not valid");
    }
    Node node = null;
    Node currentNode = this.head;
    Node leftNode;
    Node rightNode;
    Node oldNode = null;
    int count = - 1;
    Iterator iterator = this.iterator();
    while (iterator.hasNext()) {
      leftNode = node;
      node = currentNode.getNextNode();
      currentNode = node;
      count++;
      if (count == index && index > 0 && index <= this.size() - 2) {
        oldNode = leftNode.getNextNode();
        rightNode = leftNode.getNextNode().getNextNode();
        Node newNode = new Node(element, null);
        leftNode.setNextNode(newNode);
        newNode.setNextNode(rightNode);
        break;
      }
      if (index == 0 && count == index) {
        Node newNode = new Node(element, null);
        oldNode = head.getNextNode();
        head.setNextNode(newNode);
        newNode.setNextNode(oldNode.getNextNode());
        break;
      }
      if (index == this.size() - 1 && count == index) {
        oldNode = leftNode.getNextNode();
        Node newNode = new Node(element, null);
        leftNode.setNextNode(newNode);
        break;
      }
    }
    Object oldElement = oldNode.getData();
    return oldElement;
  }

  @Override
  public Object get(int index) {
    if (index > this.size() - 1 || index < 0) {
      throw new IndexOutOfBoundsException("Index = " + index + " is out of bounds.");
    }
    Node node;
    Node currentNode = this.head;
    int count = - 1;
    Iterator iterator = this.iterator();
    while (iterator.hasNext()) {
      node = currentNode.getNextNode();
      currentNode = node;
      count++;
      if (count == index) {
        return currentNode.getData();
      }
    }
    return null;
  }

  @Override
  public int indexOf(Object element) {
    if (element == null) {
      throw new IllegalArgumentException("Null element not allowed");
    }
    int count = - 1;
    for (Object object : this) {
      count++;
      if (object == element) {
        return count;
      }
    }
    return 0;
  }

  @Override
  public boolean isEmpty() {
    if (this.head == null) {
      return true;
    }
    return false;
  }

  @Override
  public boolean contains(Object o) {
    if (o == null) {
      throw new IllegalArgumentException("Null element not allowed");
    }
    for (Object object : this) {
      if (object == o) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean remove(Object o) {
    Node leftNode = null;
    Node rightNode = null;
    Node node = null;
    Node currentNode = this.head;
    if (o == null) {
      throw new IllegalArgumentException("Null element not allowed");
    }
    if (this.isEmpty() == true) {
      throw new NullPointerException("Your LinkedList is null");
    }
    if (head.getNextNode().getData() == o) {
      rightNode = head.getNextNode().getNextNode();
      head.setNextNode(rightNode);
      return true;
    }
    if (! this.isEmpty() && head.getNextNode().getData() != o) {
      Iterator iterator = this.iterator();
      while (iterator.hasNext()) {
        node = currentNode.getNextNode();
        currentNode = node;
        leftNode = node;
        if (node.getNextNode().getData() == o) {
          rightNode = node.getNextNode().getNextNode();
          break;
        }
      }
      leftNode.setNextNode(rightNode);
      return true;
    }
    return false;
  }

  @Override
  public Object remove(int index) {
    if (index > this.size() - 1 && index < - 1) {
      throw new IndexOutOfBoundsException("index = " + index + " is out of bounds.");
    }
    Object object = this.get(index);
    remove(get(index));
    return object;
  }

  @Override
  public int size() {
    int size = 0;
    if (! this.isEmpty()) {
      Node node = head;
      while (node.getNextNode() != null) {
        size++;
        node = node.getNextNode();
      }
      return size;
    }
    return size;
  }

  @Override
  public void clear() {
    this.head = null;
    this.currentNode = null;
  }

  @Override
  public Iterator iterator() {
    return new MyIterator(this.head);
  }

  @Override
  public ListIterator listIterator() {
    return null;
  }


  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }


  @Override
  public int lastIndexOf(Object o) {
    return 0;
  }

  @Override
  public ListIterator listIterator(int index) {
    return null;
  }

  @Override
  public List subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public void forEach(Consumer action) {
  }

}
