package com.psybergate.grad2021.core.hw4.singlyLinkedList;

/**
 * This is a mimic of a singly LinkedList
 */
public class MyLinkedList {

  private static int DEFAULT_SIZE;

  static {
    DEFAULT_SIZE = 10;
  }

  private Node[] nodeArray;

  public MyLinkedList() {
  }

  public boolean add(int num) {
    if (nodeArray == null) {
      nodeArray = new Node[DEFAULT_SIZE];
      Node node = new Node(num, null);
      nodeArray[0] = node;
    } else {
      this.addLast(num);
    }
    return true;
  }

  public void add(int index, int num) {
    if (nodeArray != null && index > this.size() - 1) {
      throw new IndexOutOfBoundsException();
    }
    Node[] nodeArray2 = new Node[DEFAULT_SIZE++];
    int arrIndex = 0;
    MyFor:
    for (Node node : nodeArray) {
      if (arrIndex == index) {
        nodeArray2[index] = new Node(num, null);
        for (int i = index + 1; i < nodeArray2.length; i++) {
          nodeArray2[i] = nodeArray[i - 1];
        }
        break MyFor;
      }
      nodeArray2[arrIndex++] = node;
    }
    nodeArray = nodeArray2;
  }

  public void addLast(int num) {
    Node[] nodeArray2 = new Node[DEFAULT_SIZE++];
    int arrIndex = 0;
    MyFor:
    for (Node node : nodeArray) {
      if (node != null) {
        nodeArray2[arrIndex++] = node;
      }
    }
    nodeArray2[arrIndex] = new Node(num, null);
    nodeArray = nodeArray2;
  }

  public void addFirst(int num) {
    Node[] nodeArray2 = new Node[DEFAULT_SIZE++];
    int arrIndex = 0;
    MyFor:
    for (Node node : nodeArray) {
      if (arrIndex == 1) {
        for (int i = 1; i < nodeArray2.length; i++) {
          nodeArray2[i] = nodeArray[i - 1];
        }
        break MyFor;
      }
      nodeArray2[arrIndex++] = new Node(num, null);
    }
    nodeArray = nodeArray2;
  }

  public int getLast() {
    return nodeArray[this.size() - 1].getDataField();
  }

  public int getFirst() {
    return nodeArray[0].getDataField();
  }

  public boolean contains(int num) {
    for (Node node : nodeArray) {
      if (node != null) {
        if (node.getDataField() == num) {
          return true;
        }
      }
    }
    return false;
  }

  public int size() {
    int size = 0;
    for (Node node : nodeArray) {
      if (node != null) {
        size++;
      }
    }
    return size;
  }

  public void clear() {
    Node[] newArrayNode = new Node[DEFAULT_SIZE];
    nodeArray = newArrayNode;
  }

//  public int get(int index) {
//    if (index > this.size() - 1) {
//      throw new IndexOutOfBoundsException();
//    }
//    int index1 = 0;
//    for (Node node : nodeArray) {
//      index1 = indexOf(node.getDataField());
//      if (index1 == index) {
//        return node.getDataField();
//      }
//    }
//    return index1;
//  }

//  public boolean remove(int num) {
//    int occurrence = 0;
//    if (contains(num) == true) {
//      Node[] nodeArray2 = new Node[DEFAULT_SIZE];
//      int index = 0;
//      for (Node node : nodeArray) {
//        if (node != null) {
//          if (node.getDataField() == num && occurrence == 0) {
//            occurrence++;
//            continue;
//          }
//          if (occurrence == 0 || occurrence == 1) {
//            nodeArray2[index++] = node;
//          }
//        }
//      }
//      nodeArray = nodeArray2;
//      return true;
//    }
//    return false;
//  }


  public int indexOf(int element) {
    int index = 0;
    for (Node node : nodeArray) {
      if (node != null) {
        if (node.getDataField() == element) {
          return index;
        }
      }
      index++;
    }
    return - 1;
  }

  public void print() {
    int nodeNumber1 = 1;
    int nodeNumber2 = 2;
    for (Node node : nodeArray) {
      if (node != null) {
        node.setNextField("node" + nodeNumber2++);
        System.out.println("node" + nodeNumber1++ + " = " + node);
        if (nodeNumber1 == this.size()) {
          node.setNextField(null);
          System.out.println("node" + nodeNumber1++ + " = " + nodeArray[this.size() - 1]);
          break;
        }
      }
    }
  }

}
