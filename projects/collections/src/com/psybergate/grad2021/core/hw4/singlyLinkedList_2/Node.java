package com.psybergate.grad2021.core.hw4.singlyLinkedList_2;

public class Node {

  private Object data;

  private Node nextNode;

  public Node(Object data, Node nextNode) {
    this.data = data;
    this.nextNode = nextNode;
  }

  public Node getNextNode() {
    return nextNode;
  }

  public void setNextNode(Node nextNode) {
    this.nextNode = nextNode;
  }

  public Object getData() {
    return data;
  }

}
