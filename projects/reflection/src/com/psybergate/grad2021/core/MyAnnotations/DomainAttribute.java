package com.psybergate.grad2021.core.MyAnnotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainAttribute {

  public String type() default "String";

  public String name() default "";

  public boolean nullable() default false;

  public boolean primaryKey() default false;

  public boolean foreignKey() default false;

  public String foreignKeyRef() default "";

}
