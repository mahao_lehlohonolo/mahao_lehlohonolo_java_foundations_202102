package com.psybergate.grad2021.core.ce3;

public class ExceptionChaining {

  public static void main(String[] args) {
    try {
      myMeth2();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }
  }

  public static void myMeth1() {
    IllegalArgumentException ex = new IllegalArgumentException();
    throw ex;
  }

  public static void myMeth2() throws Throwable {
    try {
      myMeth1();
    } catch (IllegalArgumentException ex) {
      throw new Throwable("hello", ex);//catches the exception from myMeth1() and throws its own
      // exception with the cause being the myMeth1(). The stack trace will show the cause being
      // the exception from myMeth1()
    }

  }

}
