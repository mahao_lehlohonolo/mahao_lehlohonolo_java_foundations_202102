package com.psybergate.grad2021.core.hw2;

import java.sql.SQLException;

public class ClassA {

  public static void methodA() throws ApplicationException {
    try {
      methodB();
    } catch (SQLException sqlException) {
      throw new ApplicationException("Hello I am in methodA()",sqlException);
    }
  }

  public static void methodB() throws SQLException {
    throw new SQLException();
  }

}
