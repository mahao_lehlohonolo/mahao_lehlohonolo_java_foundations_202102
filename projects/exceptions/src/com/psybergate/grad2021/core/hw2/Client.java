package com.psybergate.grad2021.core.hw2;

import static com.psybergate.grad2021.core.hw2.ClassA.methodA;

public class Client {

  public static void main(String[] args) {
    try {
      methodA();
    } catch (Throwable e) {
      e.getCause().printStackTrace();//this will retrieve the underlying cause of the exception
      // which is the SQLException in ClassA.methodB(), then prints the stack trace.
    }
  }

}
