package com.psybergate.grad2021.core.hw1;

public class NestedMethodsCheckedException {

  public static void main(String[] args) throws Throwable {
    //if the exception is thrown out of the main method, the JVM will handle it and print the
    // stack trace by default.
    try {
      Method1();
    } catch (Throwable throwable) {
      throw throwable;
    }
  }

  //all the methods will have to handle/propagate the exception since it is a checked exception.
  public static void Method1() throws Throwable {
    Method2();
  }

  public static void Method2() throws Throwable {
    Method3();
  }

  public static void Method3() throws Throwable {
    Method4();
  }

  public static void Method4() throws Throwable {
    Method5();
  }

  public static void Method5() throws Throwable {
    throw new Throwable("In Method5");
  }


}
