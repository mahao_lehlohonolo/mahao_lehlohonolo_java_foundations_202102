package com.psybergate.grad2021.core.hw1;

public class NestedMethodsRuntimeException {

  //the runtime exception does not need to be handled by any method, the compiler does not force
  // you to do something about it because it is a runtime exception.
  public static void main(String[] args) {
    Method1();
    System.out.println("I am in main, I do not know that there was an issue/exception");
    //if the exception is handled by catching it in one of the methods, the initial caller will
    // not know that there was an exception, it continues to do what it is told to do.
  }

  public static void Method1() {
    Method2();
  }

  public static void Method2() {
    try {
      Method3();
    } catch (RuntimeException e) {
      e.printStackTrace();
    }
  }

  public static void Method3() {
    Method4();
  }

  public static void Method4() {
    Method5();
  }

  public static void Method5() {
    throw new RuntimeException("In Method5");
  }

}
