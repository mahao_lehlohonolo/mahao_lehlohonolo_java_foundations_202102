package com.psybergate.grad2021.core.ce2;

import static com.psybergate.grad2021.core.ce2.ClassB.*;

public class ClassA {

  public static void main(String[] args) {
    try {
      //exceptions are executed according to their order
      classA2();
      classA1();
      classA3();
      classA4();
    } catch (Throwable e) {
      e.printStackTrace();
    }
  }

  public static void classA1() {
    meth1();
  }

  public static void classA2() {
    meth2();
  }

  public static void classA3() {
    meth3();
  }

  public static void classA4() {
    meth4();
  }

}
