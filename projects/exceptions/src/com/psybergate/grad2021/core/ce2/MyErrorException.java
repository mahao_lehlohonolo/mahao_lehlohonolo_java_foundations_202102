package com.psybergate.grad2021.core.ce2;

public class MyErrorException extends Error{

  public MyErrorException(String message) {
    super(message);
  }

}
