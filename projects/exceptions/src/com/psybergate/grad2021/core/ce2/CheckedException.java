package com.psybergate.grad2021.core.ce2;

public class CheckedException {
  public static void main(String[] args) {//putting 'throws Exception' in method signature mean
    // that the method must throw/propagate the exception. If it does not throw in the method
    // (catch block) then that 'throws Exception' is not needed.
    try {
      someMethod();
    } catch (Exception e) {
      e.printStackTrace(); //squashed
    }
  }

  private static void someMethod() throws Exception {
    Exception e = new Exception("Hey I am the exception");
    throw e; //checked exception, has to be handled and in method signature.
  }

}
