package com.psybergate.grad2021.core.ce2;

public class ClassB {

  //when i am propagating a checked exception the methods are required to handle the exception,
  // by propagating or handling it.
  public static void main(String[] args) {
    try {
      meth4();
    } catch (Throwable e) {
      e.printStackTrace();
    }
  }

  public static void meth1() {
    throw new MyErrorException("I started in meth1()");
  }

  public static void meth2(){
    try {
      meth1();
    } catch (MyErrorException e) {
      throw e;
    }
  }

  public static void meth3() {
    try {
      meth2();
    } catch (MyErrorException e) {
      throw e;
    }
  }

  public static void meth4() {
    try {
      meth3();
    } catch (MyErrorException e) {
      throw e;
    }
  }

}
