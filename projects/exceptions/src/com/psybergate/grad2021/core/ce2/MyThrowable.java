package com.psybergate.grad2021.core.ce2;

public class MyThrowable extends Throwable{

  public MyThrowable(String message) {
    super(message);
  }

  public MyThrowable(MyThrowable cause) {
    super(cause);
  }

  public MyThrowable(String message, Throwable cause) {
    super(message, cause);
  }

}
