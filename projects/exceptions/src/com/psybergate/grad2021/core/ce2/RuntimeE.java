package com.psybergate.grad2021.core.ce2;

public class RuntimeE {

  public static void main(String[] args) throws MyThrowable {
    try {
//      someMethod();
//      someMethod1();
      someMethod2();
    } catch (MyThrowable ex) {
      ex.printStackTrace();
      System.out.println(ex.getCause());
    }
  }

  private static void someMethod() {
    throw new RuntimeException();
  }

  public static void someMethod1(){
    throw new MyErrorException("in MyErrorException");
  }

  public static void someMethod2() throws MyThrowable{
    NullPointerException n = new NullPointerException();
    MyThrowable ex = new MyThrowable("exception message",n);
    throw ex;
  }


}
