package com.psybergate.grad2021.core.ce1;

public class SquashingAnException {

  public static void main(String[] args) throws Exception {
    try {
      someMethod();
    } catch (Exception e) {
      e.printStackTrace(); //squashing it, did not do anything(handle) about the exception
      System.out.println("I am here"); //caller does not know that there is a problem, it got
      // here because it continues after the printStackTrace.
      throw new Exception();//anything after this will not be executed, program terminates here.
    }
  }

  private static void someMethod() throws Exception {
      int i = 0;
    try {
      int j = 1/i;
    } catch (Exception e) {
      throw new Exception();
    }
  }

}
