package com.psybergate.grad2021.core.hw4.runtimeExceptions;

import static com.psybergate.grad2021.core.hw4.runtimeExceptions.AccountDBController.accDBObject;
import static com.psybergate.grad2021.core.hw4.runtimeExceptions.AccountDBController.getIndex;

public class Deposit extends Services {

  private static double MAX_DEPOSIT;

  static {
    MAX_DEPOSIT = 20_000;
  }

  public Deposit(Account account) {
    super(account);
  }

  public static double getMaxDeposit() {
    return MAX_DEPOSIT;
  }

  public void deposit(Account account, double amount) throws AccountValidator {
    if (amount > getMaxDeposit()) {
      throw new AccountValidator("Maximum deposit amount exceeded.");
    }
    if (amount < 0) {
      throw new AccountValidator("Negative amount not allowed.");
    }
    double accBal = 0;
    try {
      accBal = accDBObject.getAccDB().get(getIndex(account)).getBalance();
      account.setBalance(accBal + amount);
    } catch (AccountValidator e) {
      throw new AccountValidator("Cannot do a deposit, account does not exist.", e);
    }
  }

}
