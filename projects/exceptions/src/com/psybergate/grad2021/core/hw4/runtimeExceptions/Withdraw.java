package com.psybergate.grad2021.core.hw4.runtimeExceptions;

import static com.psybergate.grad2021.core.hw4.runtimeExceptions.AccountDBController.accDBObject;
import static com.psybergate.grad2021.core.hw4.runtimeExceptions.AccountDBController.getIndex;

public class Withdraw extends Services {

  private static double MAX_WITHDRAWAL;

  static {
    MAX_WITHDRAWAL = 10_000;
  }

  public Withdraw(Account account) {
    super(account);
  }

  public static double getMaxWithdrawal() {
    return MAX_WITHDRAWAL;
  }

  public void withdraw(double amount) throws AccountValidator {
    if (amount > getMaxWithdrawal()) {
      throw new AccountValidator("Maximum withdrawal amount exceeded.");
    }
    if (account.getBalance() == 0) {
      throw new AccountValidator("Account balance is R0.00 ,not eligible for overdraft");
    }
    if (amount < 0) {
      throw new AccountValidator("Negative amount not allowed.");
    }
    double accBal = 0;
    try {
      accBal = accDBObject.getAccDB().get(getIndex(account)).getBalance();
      account.setBalance(accBal - amount);
    } catch (AccountValidator e) {
      throw new AccountValidator("Cannot do a withdrawal, account does not exist.", e);
    }
  }


}
