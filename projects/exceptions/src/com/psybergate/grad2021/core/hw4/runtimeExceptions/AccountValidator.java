package com.psybergate.grad2021.core.hw4.runtimeExceptions;

public class AccountValidator extends RuntimeException{

  public AccountValidator(String message) {
    super(message);
  }

  public AccountValidator(String message, Throwable cause) {
    super(message, cause);
  }

}
