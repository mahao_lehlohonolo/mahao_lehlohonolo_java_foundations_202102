package com.psybergate.grad2021.core.hw4.runtimeExceptions;

public class ServicesController {

  public static void depositMoney(Account account, double amount) {
    Deposit deposit = new Deposit(account);
    try {
      deposit.deposit(account, amount);
    } catch (AccountValidator accountValidator) {
      throw accountValidator;
    }
  }

  public static void withdrawMoney(Account account, double amount) {
    Withdraw withdraw = new Withdraw(account);
    withdraw.withdraw(amount);
  }

  public static void balancePrint(Account account) {
    Services balancePrintService = new Services(account);
    balancePrintService.print();
  }

}
