package com.psybergate.grad2021.core.hw4.checkedExceptions;

public class Account {

  private String identificationNum;

  private String name;

  private String surname;

  private double balance;

  public Account(String identificationNum, String name, String surname) {
    this.identificationNum = identificationNum;
    this.name = name;
    this.surname = surname;
    this.balance = 0;
  }

  public String getIdentificationNum() {
    return identificationNum;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

}
