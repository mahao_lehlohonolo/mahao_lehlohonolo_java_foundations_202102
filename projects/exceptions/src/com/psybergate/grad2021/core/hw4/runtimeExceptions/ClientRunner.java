package com.psybergate.grad2021.core.hw4.runtimeExceptions;

import static com.psybergate.grad2021.core.hw4.runtimeExceptions.AccountDBController.addToDB;
import static com.psybergate.grad2021.core.hw4.runtimeExceptions.AccountDBController.searchInDB;

public class ClientRunner {

  public static void main(String[] args) {
    Account acc1 = new Account("0000001", "Lehlohonolo", "Mahao");
    Account acc2 = new Account("0000001", "Lehlohonolo", "Mahao");
    try {
      addToDB(acc1);
      searchInDB(acc1);
      addToDB(acc2);
//    removeFromDB(acc1);
//    depositMoney(acc1, 1900);
//    withdrawMoney(acc1,600);
//    balancePrint(acc1);
    } catch (AccountValidator accountValidator) {
      accountValidator.printStackTrace();
    }
  }

}