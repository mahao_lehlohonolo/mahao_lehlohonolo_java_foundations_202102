package com.psybergate.grad2021.core.hw4.runtimeExceptions;

public class AccountDBController extends AccountDB {

  static AccountDB accDBObject = new AccountDB();

  public static void addToDB(Account account) {
    for (Account acc : accDBObject.getAccDB()) {
      if (acc.getIdentificationNum() == account.getIdentificationNum()) {
        throw new AccountValidator("Account for Identification Num: " + account.getIdentificationNum() + " already exist.");
      }
    }
    accDBObject.getAccDB().add(account);
  }

  public static void removeFromDB(Account account) {
    int num = 0;
    for (Account acc : accDBObject.getAccDB()) {
      if (acc.getIdentificationNum() == account.getIdentificationNum()) {
        accDBObject.getAccDB().remove(account);
        num = 1;
      }
      if (num == 0) {
        throw new AccountValidator("Account for Identification Num: " + account.getIdentificationNum() + " does not exist.");
      }
    }
  }

  public static void searchInDB(Account account) {
    int num = 0;
    myFor:
    for (Account acc : accDBObject.getAccDB()) {
      if (acc.getIdentificationNum() == account.getIdentificationNum()) {
        num = 1;
        break myFor;
      }
      if (num == 0) {
        throw new AccountValidator("Account for Identification Num: " + account.getIdentificationNum() + " does not exist.");
      }
    }
    if (num == 1) {
      System.out.println("Account for Identification Num: " + account.getIdentificationNum() + " " +
              "exists.");
    }
  }

  public static int getIndex(Account account) {
    if (accDBObject.getAccDB().size() == 0) {
      throw new AccountValidator("Accounts DataBase empty.");
    }
    if (accDBObject.getAccDB().contains(account) == false) {
      throw new AccountValidator("No such account in database.");
    }
    int index = 0;
    myFor:
    for (Account acc : accDBObject.getAccDB()) {
      if (acc.equals(account)) {
        break myFor;
      }
      index++;
    }
    return index;
  }

}
