package com.psybergate.grad2021.core.hw4.runtimeExceptions;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

  //still need to make it better.
  //I need to protect this somehow, make AccountDBController the only class that can access this.
  //used a getter for the time being
  private static List<Account> accDB = new ArrayList<>();

  protected static List<Account> getAccDB() {
    return accDB;
  }

}
