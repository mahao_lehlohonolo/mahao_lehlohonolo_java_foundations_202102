package com.psybergate.grad2021.core.hw4.checkedExceptions;

public class AccountValidator extends Exception{

  public AccountValidator(String message) {
    super(message);
  }

  public AccountValidator(String message, Throwable cause) {
    super(message, cause);
  }

}
