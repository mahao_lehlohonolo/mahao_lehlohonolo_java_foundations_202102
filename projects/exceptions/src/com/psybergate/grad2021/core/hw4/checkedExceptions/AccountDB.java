package com.psybergate.grad2021.core.hw4.checkedExceptions;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

  //I need to protect this somehow, make AccountDBController the only class that can access this.
  private static List<Account> accDB = new ArrayList<>();

  //not really protecting the database through a getter but its something for now.
  protected static List<Account> getAccDB() {
    return accDB;
  }

}
