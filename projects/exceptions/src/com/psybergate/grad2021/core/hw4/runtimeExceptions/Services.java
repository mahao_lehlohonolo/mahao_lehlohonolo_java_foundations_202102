package com.psybergate.grad2021.core.hw4.runtimeExceptions;

public class Services {

  Account account;

  public Services(Account account) {
    this.account = account;
  }

  public void print() {
    System.out.println("Account holder: " + account.getName() + " " + account.getSurname() + "|"
            + "Balance: R" + account.getBalance());
  }


}
