package com.psybergate.grad2021.core.hw4.checkedExceptions;

public class ServicesController {

  public static void depositMoney(Account account, double amount) throws AccountValidator {
    Deposit deposit = new Deposit(account);
    deposit.deposit(amount);
  }

  public static void withdrawMoney(Account account, double amount) throws AccountValidator{
    Withdraw withdraw = new Withdraw(account);
    withdraw.withdraw(amount);
  }

  public static void balancePrint(Account account){
    Services balancePrintService = new Services(account);
    balancePrintService.print();
  }

}
