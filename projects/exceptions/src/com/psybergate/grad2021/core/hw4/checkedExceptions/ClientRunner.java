package com.psybergate.grad2021.core.hw4.checkedExceptions;

import static com.psybergate.grad2021.core.hw4.checkedExceptions.AccountDBController.addToAccDB;
import static com.psybergate.grad2021.core.hw4.checkedExceptions.ServicesController.*;

public class ClientRunner {

  public static void main(String[] args) throws AccountValidator {
    try {
      Account acc1 = new Account("0000001", "Lehlohonolo", "Mahao");
      Account acc2 = new Account("0000001", "Lehlohonolo", "Mahao");
      addToAccDB(acc1);
      addToAccDB(acc2);
      depositMoney(acc1, 20000.00);
      depositMoney(acc2, 6000.00);
      withdrawMoney(acc1, 10000);
      withdrawMoney(acc2, 1000);
      balancePrint(acc1);
      balancePrint(acc2);
    } catch (AccountValidator accountValidator) {
      accountValidator.printStackTrace();
    }
  }

}
