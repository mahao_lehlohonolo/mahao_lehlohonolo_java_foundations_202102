package com.psybergate.grad2021.core.demo01;

public class ThreadCreationA extends Thread {

  private MultiplierAdder multiplierAdder;

  public ThreadCreationA(MultiplierAdder multiplierAdder) {
    this.multiplierAdder = multiplierAdder;
  }

  @Override
  public void run() {
    System.out.println("Thread running...");
  }

  public void multiply(int factor) {
    multiplierAdder.multiply(factor);
  }

  public void add(int factor){
    multiplierAdder.add(factor);
  }

}
