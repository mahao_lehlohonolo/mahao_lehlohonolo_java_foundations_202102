package com.psybergate.grad2021.core.hw1a;

public class MessageReader2 implements Runnable {

  private Messages messages;

  public MessageReader2(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() +" running...");
    messages.reader();
  }

}
