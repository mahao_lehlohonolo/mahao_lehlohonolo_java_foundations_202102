package com.psybergate.grad2021.core.hw1a_v2;

public class Client {

  public static void main(String[] args) {
    Messages messages = new Messages();
    someWork(messages, 4);
  }

  public static void someWork(Messages messages, int numOfIterations) {
    MessageWriter writer = new MessageWriter(messages);
    MessageReader reader = new MessageReader(messages);
    Thread t2 = new Thread(reader, "Reader_thread");
    t2.start();
    while (numOfIterations > 0) {
      try {
        numOfIterations--;
        Thread t1 = new Thread(writer, "Writer_thread");
        t1.start();
        t1.join();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }


}
