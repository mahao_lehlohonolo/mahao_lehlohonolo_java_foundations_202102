package com.psybergate.grad2021.core.hw1a;

import java.util.Arrays;
import java.util.List;

public class MessageWriter implements Runnable{

  List<String> myMessages = Arrays.asList("Hello There!","How are you?","I am good thanks.","Awe " +
          "sweet stuff then");

  private Messages messages;

  public MessageWriter(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() +" running...");
    try {
      messages.writer(myMessages.get(randomIndex()));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public int randomIndex(){
    int max = myMessages.size();
    return (int) (Math.random()*max);
  }

}
