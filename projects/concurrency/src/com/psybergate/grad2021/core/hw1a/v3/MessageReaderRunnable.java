package com.psybergate.grad2021.core.hw1a.v3;

public class MessageReaderThread implements Runnable {

  private Messages messages;

  public MessageReaderThread(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " running...");
    new MessageReader(messages).reader();
  }

}
