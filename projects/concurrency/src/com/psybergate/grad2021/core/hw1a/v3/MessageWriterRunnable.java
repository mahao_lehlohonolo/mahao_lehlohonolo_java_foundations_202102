package com.psybergate.grad2021.core.hw1a.v3;

public class MessageWriterThread implements Runnable {

  private Messages messages;

  public MessageWriterThread(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " running...");
    MessageWriter messageWriter = new MessageWriter(messages);
    messageWriter.writer(messageWriter.getRandomMessage());
  }

}
