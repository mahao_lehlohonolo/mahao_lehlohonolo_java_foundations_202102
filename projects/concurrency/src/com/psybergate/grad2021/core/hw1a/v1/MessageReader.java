package com.psybergate.grad2021.core.hw1a;

public class MessageReader implements Runnable {

  private Messages messages;

  public MessageReader(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() +" running...");
    messages.reader();
  }

}
