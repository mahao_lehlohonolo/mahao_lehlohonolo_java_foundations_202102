package com.psybergate.grad2021.core.hw1a;

public class Messages {

  private static String message = null;

  public Messages() {
  }

  public void writer(String string) throws InterruptedException {
    synchronized (this) {
      System.out.println(Thread.currentThread().getName() + " writing message...");
      message = string;
      this.notifyAll();
      System.out.println("Reader_thread notified...");
    }
  }

  public void reader() {
    synchronized (this) {
      while (message == null) {
        try {
          System.out.println(Thread.currentThread().getName() + " waiting...");
          this.wait();
          if (message != null) {
            System.out.println(Thread.currentThread().getName() + " reading message...");
            System.out.println("Read message: " + message);
            message = null;
          }
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  public void reader2() {
    synchronized (this) {
      while (message == null) {
        try {
          System.out.println(Thread.currentThread().getName() + " waiting...");
          this.wait();
          if (message != null) {
            System.out.println(Thread.currentThread().getName() + " reading message...");
            System.out.println("Read message: " + message);
            message = null;
          }
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }


}
