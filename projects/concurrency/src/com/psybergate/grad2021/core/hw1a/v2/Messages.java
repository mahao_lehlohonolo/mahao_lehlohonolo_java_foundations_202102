package com.psybergate.grad2021.core.hw1a_v2;

public class Messages {

  private static String message = null;

  public Messages() {
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    Messages.message = message;
  }

}
