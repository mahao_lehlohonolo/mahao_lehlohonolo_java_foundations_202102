package com.psybergate.grad2021.core.hw1a.v2;

import java.util.Arrays;
import java.util.List;

public class MessageWriter implements Runnable {

  private static final List<String> MY_MESSAGES = Arrays.asList("Hello There!", "How are you?", "I" +
                  " am good thanks.",
          "Awe sweet stuff then");

  private Messages messages;

  public MessageWriter(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " running...");
    writer(getMyMessages().get(randomIndex()));
  }

  private List<String> getMyMessages() {
    return MY_MESSAGES;
  }

  public void writer(String string) {
    synchronized (getMessages()) {
      System.out.println(Thread.currentThread().getName() + " writing message...");
      getMessages().setMessage(string);
      getMessages().notify();
      System.out.println("Reader_thread notified...");
    }
  }

  private Messages getMessages() {
    return messages;
  }

  public int randomIndex() {
    int max = getMyMessages().size();
    return (int) (Math.random() * max);
  }

}
