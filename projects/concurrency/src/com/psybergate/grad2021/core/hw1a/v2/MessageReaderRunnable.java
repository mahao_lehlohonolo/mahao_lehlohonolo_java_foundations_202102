package com.psybergate.grad2021.core.hw1a.v2;

public class MessageReader implements Runnable {

  private Messages messages;

  public MessageReader(Messages messages) {
    this.messages = messages;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " running...");
    reader();
  }

  public void reader() {
    synchronized (getMessages()) {
      while (true) {
        try {
          System.out.println(Thread.currentThread().getName() + " waiting...");
          getMessages().wait();
          if (getMessages().getMessage() != null) {
            System.out.println(Thread.currentThread().getName() + " reading message...");
            System.out.println("Read message: " + getMessages().getMessage());
            getMessages().setMessage(null);
          }
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  private Messages getMessages() {
    return messages;
  }

}
