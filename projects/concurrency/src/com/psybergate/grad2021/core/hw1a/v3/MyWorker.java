package com.psybergate.grad2021.core.hw1a.v3;public class MyWorker{public static void someWork(com.psybergate.grad2021.core.hw1a.v3.Messages messages, int numOfIterations) {
    java.lang.Thread thread1 = new java.lang.Thread(new com.psybergate.grad2021.core.hw1a.v3.MessageReaderThread(messages),"Reader_thread");
    thread1.start();
    while (numOfIterations > 0) {
      try {
        numOfIterations--;
    java.lang.Thread thread2 = new java.lang.Thread(new com.psybergate.grad2021.core.hw1a.v3.MessageWriterThread(messages),"Writer_thread");
        thread2.start();
        thread2.join();
      } catch (java.lang.InterruptedException e) {
        throw new java.lang.RuntimeException(e);
      }
    }
  }}