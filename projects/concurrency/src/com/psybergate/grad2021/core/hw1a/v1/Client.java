package com.psybergate.grad2021.core.hw1a;

public class Client {

  public static void main(String[] args) {
    Messages messages = new Messages();
    someWork(messages, 6);
  }

  public static void someWork(Messages messages, int numOfIterations) {
    MessageWriter writer = new MessageWriter(messages);
    MessageReader reader = new MessageReader(messages);
    MessageReader2 reader2 = new MessageReader2(messages);
    Thread t2 = new Thread(reader, "Reader_thread");
    Thread t3 = new Thread(reader2, "Reader_thread_2");
    t2.start();
    t3.start();
    while (numOfIterations > 0) {
      numOfIterations--;
      Thread t1 = new Thread(writer, "Writer_thread");
      t1.start();
      try {
        t1.join();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }

}
