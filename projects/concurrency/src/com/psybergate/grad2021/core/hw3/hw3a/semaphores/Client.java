package com.psybergate.grad2021.core.hw3.hw3a;

import java.util.concurrent.Semaphore;

public class Client {

  public static void main(String[] args) {
    Semaphore semaphore = new Semaphore(1);
    SemaphoresDemo semaphoresDemo = new SemaphoresDemo();
    ThreadA t1 = new ThreadA(semaphore,semaphoresDemo);
    ThreadB t2 = new ThreadB(semaphore,semaphoresDemo);
    Thread thread1 = new Thread(t1);
    Thread thread2 = new Thread(t2);
    thread1.start();
    thread2.start();
  }

}
