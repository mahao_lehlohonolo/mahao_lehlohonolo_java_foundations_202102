package com.psybergate.grad2021.core.hw3.hw3a.cyclicbarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadA implements Runnable {

  private CyclicBarrier cyclicBarrier;

  private CyclicBarrierDemo cyclicBarrierDemo;

  public ThreadA(CyclicBarrier cyclicBarrier, CyclicBarrierDemo cyclicBarrierDemo) {
    this.cyclicBarrier = cyclicBarrier;
    this.cyclicBarrierDemo = cyclicBarrierDemo;
  }

  @Override
  public void run() {
    try {
      System.out.println(Thread.currentThread().getName() + ": running");
      cyclicBarrierDemo.getIntegers().add(cyclicBarrierDemo.product());
      System.out.println(Thread.currentThread().getName() + ": waiting for the other thread");
      cyclicBarrier.await();
      System.out.println(Thread.currentThread().getName() + ": done");

    } catch (BrokenBarrierException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
