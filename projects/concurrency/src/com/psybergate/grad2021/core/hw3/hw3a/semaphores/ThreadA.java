package com.psybergate.grad2021.core.hw3.hw3a;

import java.util.concurrent.Semaphore;

public class ThreadA implements Runnable {

  private Semaphore semaphore;

  private SemaphoresDemo semaphoresDemo;

  public ThreadA(Semaphore semaphore, SemaphoresDemo semaphoresDemo) {
    this.semaphore = semaphore;
    this.semaphoresDemo = semaphoresDemo;
  }

  @Override
  public void run() {
    try {
      System.out.println(Thread.currentThread().getName() + ": waiting for permit");
      semaphore.acquire();
      System.out.println(Thread.currentThread().getName() + ": permit acquired");
      semaphoresDemo.setNUMBER(10);
      semaphore.release();
      System.out.println(Thread.currentThread().getName() + ": permit released");
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
