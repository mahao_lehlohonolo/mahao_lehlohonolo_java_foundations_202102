package com.psybergate.grad2021.core.hw3.hw3a.exchanger;

public class ThreadA implements Runnable {


  @Override
  public void run() {
      System.out.println(Thread.currentThread().getName() + ": running");
      System.out.println(Thread.currentThread().getName() + ": waiting for the other thread");
      System.out.println(Thread.currentThread().getName() + ": done");

  }

}
