package com.psybergate.grad2021.core.hw3.hw3a.semaphores;

import java.util.concurrent.Semaphore;

public class ThreadB implements Runnable {

  private Semaphore semaphore;

  private SemaphoresDemo semaphoresDemo;

  public ThreadB(Semaphore semaphore, SemaphoresDemo semaphoresDemo) {
    this.semaphore = semaphore;
    this.semaphoresDemo = semaphoresDemo;
  }

  @Override
  public void run() {
    try {
      System.out.println(Thread.currentThread().getName() + ": waiting for permit");
      semaphore.acquire();
      System.out.println(Thread.currentThread().getName() + ": permit acquired");
      semaphoresDemo.getNUMBER();
      semaphore.release();
      System.out.println(Thread.currentThread().getName() + ": permit released");
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
