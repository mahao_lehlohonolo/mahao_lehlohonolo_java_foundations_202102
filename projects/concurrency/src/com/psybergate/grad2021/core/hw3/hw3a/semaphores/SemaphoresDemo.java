package com.psybergate.grad2021.core.hw3.hw3a;

public class SemaphoresDemo {

  private static int NUMBER = 0;

  public int getNUMBER() {
    return NUMBER;
  }

  public void setNUMBER(int NUMBER) {
    try {
      SemaphoresDemo.NUMBER = NUMBER;
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
