package com.psybergate.grad2021.core.hw3.hw3a.exchanger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ThreadA implements Runnable {

  private String name2 = "Mushy";

  private Exchanger<String> exchanger;

  public ThreadA(Exchanger<String> exchanger) {
    this.exchanger = exchanger;
  }

  @Override
  public void run() {
    try {
      System.out.println(Thread.currentThread().getName() + ": running");
      System.out.println(Thread.currentThread().getName() + ": (Before exchange: " + name2 + ")");
      System.out.println(Thread.currentThread().getName() + ": waiting to exchange");
      name2 = exchanger.exchange(name2, 3000, TimeUnit.MILLISECONDS);
      System.out.println(Thread.currentThread().getName() + ": (After exchange: " + name2 + ")");
    } catch (InterruptedException | TimeoutException e) {
      if (e instanceof TimeoutException) {
        throw new RuntimeException(Thread.currentThread().getName() + ": waiting time elapsed");
      } else {
        throw new RuntimeException(e);
      }
    }
  }

}
