package com.psybergate.grad2021.core.ce3a;

public class ThreadCreationB implements Runnable {

  private static int NUMBER = 0;

  private int num;

  public ThreadCreationB(int num) {
    this.num = num;
  }

  public static int getNUMBER() {
    return NUMBER;
  }

  @Override
  public void run() {
    try {
      this.add(2, num);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void add(int numberOfThreads, int max) throws InterruptedException {
    int divider = numberOfThreads;
    while (numberOfThreads > 0) {
      Thread thread = new Thread(this);
      System.out.println(Thread.currentThread().getName() + " (START) NUMBER =" + getNUMBER());
      thread.start();
      for (int count = 0; count < max / divider; count++) {
        NUMBER = NUMBER + 1;
      }
      thread.join();
      System.out.println(Thread.currentThread().getName() + " (END) NUMBER =" + getNUMBER());
      numberOfThreads--;
    }

  }

}
