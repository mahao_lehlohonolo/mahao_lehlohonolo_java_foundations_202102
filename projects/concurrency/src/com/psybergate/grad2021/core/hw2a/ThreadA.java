package com.psybergate.grad2021.core.Deadlock;

public class ThreadA implements Runnable {

  private SomeClass cls;

  public ThreadA(SomeClass cls) {
    this.cls = cls;
  }

  @Override
  public void run() {
    cls.methodA();
  }

}
