package com.psybergate.grad2021.core.hw2a;

import com.psybergate.grad2021.core.Deadlock.SomeClass;

public class ThreadA implements Runnable {

  private SomeClass cls;

  public ThreadA(SomeClass cls) {
    this.cls = cls;
  }

  @Override
  public void run() {
    cls.methodA();
  }

}
