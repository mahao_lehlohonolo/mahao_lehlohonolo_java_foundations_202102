package com.psybergate.grad2021.core.RaceCondition;

public class ClientB {

  public static void main(String[] args) {
    SomeClass cls = new SomeClass();
    methodA(10,cls);
  }

  public static void methodA(int numberOfThreads, SomeClass someClass){
    for(int count = 0 ; count<numberOfThreads;count++){
      Thread thread = new Thread(new ThreadC(someClass));
      thread.start();
    }
  }

}
