package com.psybergate.grad2021.core.RaceCondition;

public class ThreadC implements Runnable {

  private SomeClass cls;

  public ThreadC(SomeClass cls) {
    this.cls = cls;
  }

  @Override
  public void run() {
      cls.increment();
  }


}
