package com.psybergate.grad2021.core.RaceCondition;

public class ThreadA implements Runnable {

  private SomeClass cls;

  public ThreadA(SomeClass cls) {
    this.cls = cls;
  }

  @Override
  public void run() {
      cls.methodA();
  }

}
