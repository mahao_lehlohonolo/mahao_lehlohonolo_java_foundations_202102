package com.psybergate.grad2021.core.RaceCondition;

public class Client {

  public static void main(String[] args) {
    SomeClass cls = new SomeClass();
    Thread t1 = new Thread(new ThreadA(cls));
    Thread t2 = new Thread(new ThreadB(cls));
    t1.start();
    t2.start();
  }

}
