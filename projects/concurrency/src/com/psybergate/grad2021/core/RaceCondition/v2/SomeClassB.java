package com.psybergate.grad2021.core.RaceCondition;

public class SomeClass {

  public static final Object object = new Object();

  private static int NUMBER = 0;

  public static int getNUMBER() {
    return NUMBER;
  }

  public void increment() {
    synchronized (object) {
    }
      try {
        Thread.sleep(0);
        System.out.println(Thread.currentThread().getName() + ": " + NUMBER++);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
  }

  public void methodA() {
    System.out.println(Thread.currentThread().getName() + " (START)NUMBER = " + getNUMBER());
    NUMBER += 2;
    System.out.println(Thread.currentThread().getName() + " (END)NUMBER = " + getNUMBER());
  }

  public void methodB() {
    System.out.println(Thread.currentThread().getName() + " (START)NUMBER = " + getNUMBER());
    NUMBER += 5;
    System.out.println(Thread.currentThread().getName() + " (END)NUMBER = " + getNUMBER());
  }

}
