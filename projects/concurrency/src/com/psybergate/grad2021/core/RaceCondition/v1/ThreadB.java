package com.psybergate.grad2021.core.RaceCondition;

public class ThreadB implements Runnable {

  private SomeClass cls;

  public ThreadB(SomeClass cls) {
    this.cls = cls;
  }

  @Override
  public void run() {
      cls.methodB();
  }

}
