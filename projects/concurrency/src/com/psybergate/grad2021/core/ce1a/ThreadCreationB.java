package com.psybergate.grad2021.core.demo01;

public class ThreadCreationB implements Runnable {

  private Operations operations;

  public ThreadCreationB(Operations operations) {
    this.operations = operations;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName()+ " running...");
    this.add();
  }

  public void multiply(int factor) {
    operations.multiply(factor);
  }

  public void add() {
    operations.add(1000);
  }

}
