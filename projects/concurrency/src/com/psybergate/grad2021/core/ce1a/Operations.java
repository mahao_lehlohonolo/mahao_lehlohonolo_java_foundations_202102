package com.psybergate.grad2021.core.demo01;

public class Operations {

  public static int NUMBER = 5000;

  private int num;

  public Operations(int num) {
    this.num = num;
  }

  public void multiply(int factor) {
    System.out.println("(Thread: " + Thread.currentThread().getName() + ") Start: num = " + this.num);
    this.num *= factor;
    System.out.println("(Thread: " + Thread.currentThread().getName() + ") End: num = " + this.num);
  }

  public void add(int factor) {
    System.out.println("(Thread: " + Thread.currentThread().getName() + ") Start: num = " + this.num);
    for (int count = 1; count <= factor; count++) {
      num = num + 1;
    }
    System.out.println("(Thread: " + Thread.currentThread().getName() + ") End: num = " + this.num);

  }

  public int getNum() {
    return this.num;
  }

}
