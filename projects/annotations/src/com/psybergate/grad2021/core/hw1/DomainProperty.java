package com.psybergate.grad2021.core.hw1;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainProperty {

  public String value() default "";

  public boolean nullable() default false;

  public String type() default "";

  public String name() default "";



}
