package com.psybergate.grad2021.core.hw3;

import com.psybergate.grad2021.core.hw1.DomainClass;
import com.psybergate.grad2021.core.hw1.DomainProperty;
import com.psybergate.grad2021.core.hw2.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;

public class DatabaseManager {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DATABASE_URL = "jdbc:postgresql://localhost/customerdb";

  private static final String USER = "postgres";

  private static final String PASSWORD = "admin";

  private static String attributeConstraints(Field field) {
    String type = "";
    Annotation[] annotations = field.getDeclaredAnnotations();
    DomainProperty domainProperty = (DomainProperty) annotations[0];
    if (domainProperty.nullable() == true) {
      type += " not NULL";
    } else {
      type = "";
    }
    return type;
  }

  private static String attributeType(Field field) {
    String type = "";
    Annotation[] annotations = field.getDeclaredAnnotations();
    DomainProperty domainProperty = (DomainProperty) annotations[0];
    if (domainProperty.type().equals("String")) {
      type += " VARCHAR(20)";
    }
    if (domainProperty.type().equals("Integer")) {
      type += " INTEGER";
    }
    return type;
  }

  public void generateDatabase() throws SQLException, ClassNotFoundException {
    Connection connection = null;
    Statement statement = null;

    Class cls = Customer.class;
    Field[] fields = cls.getDeclaredFields();
    Annotation[] declaredAnnotations = cls.getDeclaredAnnotations();

    try {
      Class.forName(JDBC_DRIVER);
      connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
      statement = connection.createStatement();
      String tableName = ((DomainClass)declaredAnnotations[0]).name() + "_tbl";
      String cmd = "create table " + tableName + " (";
      for (Field field : fields) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        if (annotations[0] instanceof DomainProperty) {
          cmd += field.getName() + attributeType(field) + attributeConstraints(field) + ", ";
        }
      }
      cmd += "primary key(" + fields[0].getName() + "))";
      DatabaseMetaData dbm = connection.getMetaData();
      ResultSet tables = dbm.getTables(null, null, tableName, null);
      if (tables.next()) {
        statement.executeUpdate("DROP TABLE IF EXISTS " + tableName);
        System.out.println("Existing table was dropped. Then created again.");
      }
      statement.executeUpdate(cmd);

    } catch (SQLException e) {
      throw e;
    } catch (ClassNotFoundException e) {
      throw e;
    }
    statement.close();
    connection.close();
  }

}
