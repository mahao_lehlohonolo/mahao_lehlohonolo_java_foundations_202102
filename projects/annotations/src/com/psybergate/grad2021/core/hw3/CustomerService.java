package com.psybergate.grad2021.core.hw3;

import com.psybergate.grad2021.core.hw1.DomainClass;
import com.psybergate.grad2021.core.hw1.DomainProperty;
import com.psybergate.grad2021.core.hw1.DomainTransient;
import com.psybergate.grad2021.core.hw2.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomerService {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DATABASE_URL = "jdbc:postgresql://localhost/customerdb";

  private static final String USER = "postgres";

  private static final String PASSWORD = "admin";


  Class cls = Customer.class;

  Annotation[] declaredAnnotations = cls.getDeclaredAnnotations();

  String tableName = ((DomainClass) declaredAnnotations[0]).name() + "_tbl";

  Field[] fields = cls.getDeclaredFields();

  public void saveCustomer(Customer customer) throws SQLException, ClassNotFoundException {

    try {
      Connection connection = null;
      Statement statement = null;
      Class.forName(JDBC_DRIVER);
      connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
      statement = connection.createStatement();
      String sql = sqlStatement(customer);
      statement.executeUpdate(sql);
      statement.close();
      connection.close();
    } catch (Exception e) {
      throw e;
    }
  }

  private String sqlStatement(Customer customer) {
    String insertIntoTable = "INSERT INTO " + tableName + " (";
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      if (annotations[0] instanceof DomainProperty) {
        insertIntoTable += field.getName().toLowerCase() + ", ";
      }
      if (annotations[0] instanceof DomainTransient) {
        continue;
      }
    }
    char[] chars = insertIntoTable.toCharArray();
    chars[chars.length - 2] = ')';
    insertIntoTable = String.valueOf(chars);
    //I am not happy with how I find 'values' below, how can I use annotations for this?.
    String values =
            "VALUES('" + customer.getCustomerNum() + "','" + customer.getName() +
                    "','" + customer.getSurname() + "','" + customer.getDateOfBirth() + "');";
    String sql = insertIntoTable + values;
    return sql;
  }

}
