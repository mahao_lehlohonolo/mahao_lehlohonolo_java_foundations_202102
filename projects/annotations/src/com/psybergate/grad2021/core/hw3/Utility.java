package com.psybergate.grad2021.core.hw3;

import com.psybergate.grad2021.core.hw2.Customer;

import java.sql.SQLException;

public class Utility {
  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    DatabaseManager databaseManager = new DatabaseManager();
    databaseManager.generateDatabase();

    Customer c1 = new Customer("1", "Lehlohonolo", "Mahao", "961127", 24);
    Customer c2 = new Customer("2","Noncedo","Mahao","980704",22);

    CustomerService cs = new CustomerService();

    cs.saveCustomer(c1);
    cs.saveCustomer(c2);
  }

}
