package com.psybergate.grad2021.core.hw4;

import com.psybergate.grad2021.core.hw2.Customer;
import com.psybergate.grad2021.core.hw3.DatabaseManager;

import java.sql.SQLException;

public class Utility {

  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    DatabaseManager databaseManager = new DatabaseManager();
    databaseManager.generateDatabase();
    Customer c1 = new Customer("1", "Lehlohonolo", "Mahao", "1996-11-27", 24);
    Customer c2 = new Customer("2","Noncedo","Mahao","1998-07-04",22);
    CustomerService cs = new CustomerService();
    System.out.println(cs.getCustomer("1"));
  }

}
