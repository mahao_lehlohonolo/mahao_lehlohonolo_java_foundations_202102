package com.psybergate.grad2021.core.hw4;

import com.psybergate.grad2021.core.hw1.DomainProperty;
import com.psybergate.grad2021.core.hw1.DomainTransient;
import com.psybergate.grad2021.core.hw2.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.time.LocalDate;

public class CustomerService {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DATABASE_URL = "jdbc:postgresql://localhost/customerdb";

  private static final String USER = "postgres";

  private static final String PASSWORD = "admin";

  String tableName = Customer.class.getSimpleName().toLowerCase() + "_tbl";

  Class cls = Customer.class;

  Field[] fields = cls.getDeclaredFields();

  public void saveCustomer(Customer customer) {

    try {
      Connection connection = null;
      Statement statement = null;
      Class.forName(JDBC_DRIVER);
      connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
      statement = connection.createStatement();

      String sql = sqlStatement(customer);
      statement.executeUpdate(sql);
      connection.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private String sqlStatement(Customer customer) {
    String insertIntoTable = "INSERT INTO " + tableName + " (";
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      if (annotations[0] instanceof DomainProperty) {
        insertIntoTable += field.getName().toLowerCase() + ", ";
      }
      if (annotations[0] instanceof DomainTransient) {
        continue;
      }
    }
    char[] chars = insertIntoTable.toCharArray();
    chars[chars.length - 2] = ')';
    insertIntoTable = String.valueOf(chars);
    //I am not happy with how I find 'values' below, how can I use annotations or reflection for
    // this?.
    String values =
            "VALUES('" + customer.getCustomerNum() + "','" + customer.getName() +
                    "','" + customer.getSurname() + "','" + customer.getDateOfBirth() + "');";
    String sql = insertIntoTable + values;
    return sql;
  }

  public Customer getCustomer(String customerNum) throws ClassNotFoundException, SQLException {
    Connection connection = null;
    Statement statement = null;
    Class.forName(JDBC_DRIVER);
    connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
    statement = connection.createStatement();

    ResultSet resultSet = statement.executeQuery("Select * from customer_tbl");
    ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
    int colNum = resultSetMetaData.getColumnCount();

    String row = "";

    while (resultSet.next()) {
      if (resultSet.getString(1).equals(customerNum)) {
        for (int count = 1; count <= colNum; count++) {
          row += resultSet.getString(count) + " ";
        }
      }
    }
    String[] str = row.split(" ");
    String[] dateOfBirth = str[3].split("-");
    connection.close();
    return new Customer(str[0],str[1],str[2],str[3],
            LocalDate.now().getYear()-Integer.parseInt(dateOfBirth[0]));
  }

}