package com.psybergate.grad2021.core.hw2;

import com.psybergate.grad2021.core.hw1.DomainClass;
import com.psybergate.grad2021.core.hw1.DomainProperty;
import com.psybergate.grad2021.core.hw1.DomainTransient;

@DomainClass(name = "customers")
public class Customer {

  @DomainProperty(type = "Integer",nullable = true)
  private String customerNum;

  @DomainProperty(type = "String")
  private String name;

  @DomainProperty(type = "String")
  private String surname;

  @DomainProperty(type = "String", nullable = true)
  private String dateOfBirth;

  @DomainTransient()
  private Integer age;

  public Customer(String customerNum, String name, String surname, String dateOfBirth,
                  Integer age) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = age;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public Integer getAge() {
    return age;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum='" + customerNum + '\'' +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", dateOfBirth='" + dateOfBirth + '\'' +
            ", age=" + age +
            '}';
  }

}
