package com.psybergate.grad2021.core.hw3.hw3B_2;

import java.util.Arrays;
import java.util.Collection;

public class Utils {

  public static void main(String[] args) {
    Collection<Integer> c1 = Arrays.asList(1,5,8,6,10,17,21,23,31,45);
    Collection<String> c2 = Arrays.asList("chh","ths","char","that");
    Collection<Account> c3 = Arrays.asList(new Account("01",25),new Account("02",-19));

    System.out.println(new EvenNumbersCounter().countElements(c1));
    System.out.println(new OddNumbersCounter().countElements(c1));
    System.out.println(new PrimeNumbersCounter().countElements(c1));
    System.out.println(new StringsStartingWithCCounter().countElements(c2));
    System.out.println(new OverdraftAccsCounter().countElements(c3));

  }

}
