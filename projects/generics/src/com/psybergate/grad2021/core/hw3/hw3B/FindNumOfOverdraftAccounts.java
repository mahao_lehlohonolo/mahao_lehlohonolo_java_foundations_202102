package com.psybergate.grad2021.core.hw3.hw3B;

import com.psybergate.grad2021.core.hw3.hw3A.Account;

import java.util.List;

public class FindNumOfOverdraftAccounts {

  List<Account> list;

  public FindNumOfOverdraftAccounts() {
  }

  public FindNumOfOverdraftAccounts(List<Account> list) {
    this.list = list;
  }

  public List<Account> getList() {
    return list;
  }

}
