package com.psybergate.grad2021.core.hw3.hw3B_2;

import java.util.Collection;
import java.util.Iterator;

public class PrimeNumbersCounter implements ElementCounter<Integer, Integer> {

  @Override
  public Integer countElements(Collection<Integer> collection) {
    int numberOfPrimes = 0;
    for (Iterator<Integer> iterator = collection.iterator(); iterator.hasNext(); ) {
      int integer = iterator.next();
      if (integer == 1) {
        continue;
      }
      if (integer == 2 || integer == 3 || integer == 5 || integer == 7) {
        numberOfPrimes++;
        continue;
      } else {
        if (integer % 2 != 0 && integer % 3 != 0 && integer % 5 != 0 && integer % 7 != 0) {
          numberOfPrimes++;
        }
      }
    }
    return numberOfPrimes;
  }

}
