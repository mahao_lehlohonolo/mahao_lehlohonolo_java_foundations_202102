package com.psybergate.grad2021.core.hw3.hw3B;

import java.util.ArrayList;
import java.util.List;

public class MyUtilsB {

  public static void main(String[] args) {
    MyClassB object = new MyClassB();
    MyClassC object2 = new MyClassC();

    List<Integer> l1 = new ArrayList<>();
    l1.add(2);
    l1.add(8);
    l1.add(15);
    l1.add(19);
    l1.add(23);
    l1.add(20);

    FindNumOfEven findNumOfEven = new FindNumOfEven(l1);
    System.out.println(object2.filterMethod(findNumOfEven));

    FindNumOfOdds findNumOfOdds = new FindNumOfOdds(l1);
    System.out.println(object.filterMethod(findNumOfOdds));

    FindNumOfPrimes findNumOfPrimes = new FindNumOfPrimes(l1);
    System.out.println(object.filterMethod(findNumOfPrimes));


  }

}
