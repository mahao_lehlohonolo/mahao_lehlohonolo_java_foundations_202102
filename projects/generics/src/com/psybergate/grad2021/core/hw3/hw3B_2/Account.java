package com.psybergate.grad2021.core.hw3.hw3B_2;

public class Account {

  private String accountNum;

  private int balance;

  public Account(String accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public Account() {
  }

  public int getBalance() {
    return balance;
  }

  @Override
  public String toString() {
    return "Account{" +
            "accountNum='" + accountNum + '\'' +
            ", balance=" + balance +
            '}';
  }

}
