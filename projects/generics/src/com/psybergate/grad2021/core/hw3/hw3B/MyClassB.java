package com.psybergate.grad2021.core.hw3.hw3B;

import com.psybergate.grad2021.core.hw3.hw3A.*;

public class MyClassB {

  public MyClassB() {
  }

  public Object filterMethod(Object object) {
    MyClassA obj = new MyClassA();
    int num = 0;
    if (object instanceof FindNumOfEven) {
      num = obj.evenNumbers(((FindNumOfEven) object).getList()).size();
    }
    if (object instanceof FindNumOfOdds) {
      num = obj.oddNumbers(((FindNumOfOdds) object).getList()).size();
    }
    if (object instanceof FindNumOfPrimes) {
      num = obj.numberOfPrimes(((FindNumOfPrimes) object).getList());
    }
    if (object instanceof FindNumOfOverdraftAccounts) {
      num = obj.overdraftAccounts(((FindNumOfOverdraftAccounts) object).getList());
    }
    if (object instanceof StringsStartingWithLetterC) {
      num = obj.stringsStartingWithLetterC(((StringsStartingWithLetterC) object).getList()).size();
    }
    return num;
  }

}
