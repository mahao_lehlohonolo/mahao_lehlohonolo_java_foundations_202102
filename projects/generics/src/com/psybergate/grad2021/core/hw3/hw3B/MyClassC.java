package com.psybergate.grad2021.core.hw3.hw3B;

import com.psybergate.grad2021.core.hw3.hw3A.MyClassA;

public class MyClassC<T> {

  public MyClassC() {
  }

  public <T> int filterMethod(T object) {
    MyClassA obj = new MyClassA();
    int num = 0;
    String typeName = object.getClass().getTypeName();
    if (typeName == getTypeName1()) {
      num = obj.evenNumbers(((FindNumOfEven) object).getList()).size();
    }
    if (typeName == getTypeName2()) {
      num = obj.oddNumbers(((FindNumOfOdds) object).getList()).size();
    }
    if (typeName == getTypeName3()) {
      num = obj.numberOfPrimes(((FindNumOfPrimes) object).getList());
    }
    if (typeName == getTypeName4()) {
      num = obj.overdraftAccounts(((FindNumOfOverdraftAccounts) object).getList());
    }
    if (typeName == getTypeName5()) {
      num = obj.stringsStartingWithLetterC(((StringsStartingWithLetterC) object).getList()).size();
    }
    return num;
  }

  private String getTypeName5() {
    return new StringsStartingWithLetterC().getClass().getTypeName();
  }

  private String getTypeName4() {
    return new FindNumOfOverdraftAccounts().getClass().getTypeName();
  }

  private String getTypeName3() {
    return new FindNumOfPrimes().getClass().getTypeName();
  }

  private String getTypeName2() {
    return new FindNumOfOdds().getClass().getTypeName();
  }

  private String getTypeName1() {
    return new FindNumOfEven().getClass().getTypeName();
  }


}
