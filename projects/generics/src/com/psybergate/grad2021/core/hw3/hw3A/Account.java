package com.psybergate.grad2021.core.hw3.hw3A;

public class Account {

  private String accountNum;

  private int balance;

  public Account(String accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public int getBalance() {
    return balance;
  }

  @Override
  public String toString() {
    return "Account{" +
            "accountNum='" + accountNum + '\'' +
            ", balance=" + balance +
            '}';
  }

}
