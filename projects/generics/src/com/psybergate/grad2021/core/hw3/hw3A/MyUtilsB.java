package com.psybergate.grad2021.core.hw3.hw3A;

import java.util.ArrayList;
import java.util.List;

public class MyUtilsB {

  public static void main(String[] args) {
    MyClassA object = new MyClassA();

    List<Integer> l1 = new ArrayList<>();
    l1.add(1);
    l1.add(2);
    l1.add(9);
    l1.add(15);
    l1.add(21);
    l1.add(27);
    l1.add(29);
    l1.add(35);
    l1.add(42);
    l1.add(51);
    l1.add(100);
    l1.add(5);
    l1.add(3);

    List<Integer> l2 = object.evenNumbers(l1);
    System.out.println(l2);

    List<Integer> l3 = object.oddNumbers(l1);
    System.out.println(l3);

    int numberOfPrimes = object.numberOfPrimes(l1);
    System.out.println(numberOfPrimes);

    List<String> l4 = new ArrayList<>();
    l4.add("cht");
    l4.add("hkd");
    l4.add("hhyara");
    l4.add("chyta");
    l4.add("Char");
    l4.add("yehh");
    l4.add("ohhh");
    l4.add("craxxccvv");

    List<String> l5 = object.stringsStartingWithLetterC(l4);
    System.out.println(l5);

    Account acc1 = new Account("00000", 500);
    Account acc2 = new Account("00001", 0);
    Account acc3 = new Account("00011", - 79);
    Account acc4 = new Account("00111", - 56);

    List<Account> l6 = new ArrayList<>();
    l6.add(acc1);
    l6.add(acc2);
    l6.add(acc3);
    l6.add(acc4);

    int numberOfOverdraftAccounts = object.overdraftAccounts(l6);
    System.out.println(numberOfOverdraftAccounts);
  }

}
