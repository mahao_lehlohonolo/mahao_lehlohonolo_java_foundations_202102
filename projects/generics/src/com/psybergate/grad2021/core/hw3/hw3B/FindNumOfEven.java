package com.psybergate.grad2021.core.hw3.hw3B;

import java.util.List;

public class FindNumOfEven {

  List<Integer> list;

  public FindNumOfEven() {
  }

  public FindNumOfEven(List<Integer> list) {
    this.list = list;
  }

  public List<Integer> getList() {
    return list;
  }

}
