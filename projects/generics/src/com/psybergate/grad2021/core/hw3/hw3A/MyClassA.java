package com.psybergate.grad2021.core.hw3.hw3A;

import java.util.ArrayList;
import java.util.List;

public class MyClassA {

  public MyClassA() {
  }

  public List<Integer> evenNumbers(List<Integer> list) {
    List<Integer> evenNumbersList = new ArrayList<>();
    for (Integer integer : list) {
      if (integer % 2 == 0) {
        evenNumbersList.add(integer);
      }
    }
    return evenNumbersList;
  }

  public List<Integer> oddNumbers(List<Integer> list) {
    List<Integer> oddNumbersList = new ArrayList<>();
    for (Integer integer : list) {
      if (integer % 2 != 0) {
        oddNumbersList.add(integer);
      }
    }
    return oddNumbersList;
  }

  public int numberOfPrimes(List<Integer> list) {
    int numberOfPrimes = 0;
    for (Integer integer : list) {
      if(integer == 2 || integer==3 || integer==5 || integer==7){
        numberOfPrimes++;
        continue;
      }else{
        if (integer % 2 != 0 && integer % 3 != 0 && integer % 5 != 0 && integer % 7 != 0) {
          numberOfPrimes++;
        }
      }
    }
    return numberOfPrimes;
  }

  public List<String> stringsStartingWithLetterC(List<String> list) {
    List<String> filteredList = new ArrayList<>();
    for (String string : list) {
      if (string.charAt(0) == 'c' || string.charAt(0) == 'C') {
        filteredList.add(string);
      }
    }
    return filteredList;
  }

  public int overdraftAccounts(List<Account> list) {
    int overdraftAccountsNum = 0;
    for (Account account : list) {
      if (account.getBalance() < 0) {
        overdraftAccountsNum++;
      }
    }
    return overdraftAccountsNum;
  }

}
