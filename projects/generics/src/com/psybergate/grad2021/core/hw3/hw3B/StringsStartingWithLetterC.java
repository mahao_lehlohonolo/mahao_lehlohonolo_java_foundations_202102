package com.psybergate.grad2021.core.hw3.hw3B;

import java.util.List;

public class StringsStartingWithLetterC {

  List<String> list;

  public StringsStartingWithLetterC() {
  }

  public StringsStartingWithLetterC(List<String> list) {
    this.list = list;
  }

  public List<String> getList() {
    return list;
  }

}
