package com.psybergate.grad2021.core.hw3.hw3B_2;

import java.util.Collection;
import java.util.Iterator;

public class StringsStartingWithCCounter implements ElementCounter<String,Integer> {

  @Override
  public Integer countElements(Collection<String> collection) {
    int num = 0;
    for (Iterator<String> iterator = collection.iterator(); iterator.hasNext(); ) {
      String element = iterator.next().toUpperCase();
      if (element.charAt(0) == 'C') {
        num++;
      }
    }
    return num;
  }

}
