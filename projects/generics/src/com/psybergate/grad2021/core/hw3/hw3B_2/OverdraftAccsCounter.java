package com.psybergate.grad2021.core.hw3.hw3B_2;

import java.util.Collection;
import java.util.Iterator;

public class OverdraftAccsCounter implements ElementCounter<Account,Integer> {

  @Override
  public Integer countElements(Collection<Account> collection) {
    int num = 0;
    for (Iterator<Account> iterator = collection.iterator(); iterator.hasNext(); ) {
      Account account = iterator.next();
      if (account.getBalance() < 0) {
        num++;
      }
    }
    return num;
  }

}
