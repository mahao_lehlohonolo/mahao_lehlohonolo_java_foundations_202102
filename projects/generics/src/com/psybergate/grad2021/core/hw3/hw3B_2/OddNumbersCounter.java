package com.psybergate.grad2021.core.hw3.hw3B_2;

import java.util.Collection;
import java.util.Iterator;

public class OddNumbersCounter implements ElementCounter<Integer, Integer> {

  @Override
  public Integer countElements(Collection<Integer> collection) {
    int num = 0;
    for (Iterator<Integer> iterator = collection.iterator(); iterator.hasNext(); ) {
      int element = iterator.next();
      if (element % 2 != 0) {
        num++;
      }
    }
    return num;
  }

}
