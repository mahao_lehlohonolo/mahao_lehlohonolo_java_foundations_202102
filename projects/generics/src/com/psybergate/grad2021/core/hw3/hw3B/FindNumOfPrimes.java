package com.psybergate.grad2021.core.hw3.hw3B;

import java.util.List;

public class FindNumOfPrimes {

  List<Integer> list;

  public FindNumOfPrimes() {
  }

  public FindNumOfPrimes(List<Integer> list) {
    this.list = list;
  }

  public List<Integer> getList() {
    return list;
  }

}
