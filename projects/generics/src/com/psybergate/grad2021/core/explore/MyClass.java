package com.psybergate.grad2021.core.explore;

import java.util.ArrayList;
import java.util.List;

public class MyClass<T> {

  public static void main(String[] args) throws InstantiationException, IllegalAccessException {
    List<String> l1 = new ArrayList<>();
    MyClass<String> o = new MyClass<>();
    MyClass o2 = o.getClass().newInstance();
    myMethod(l1, new MyClass<String>());
  }

  public static <T> void myMethod(List<T> l, MyClass<T> cls) throws IllegalAccessException,
          InstantiationException {
    //T someObj = new T();//will not compile
    T someObj2 = (T) new MyClass<T>();
    T someObj3 = (T) cls.getClass().newInstance();
    System.out.println("someObj3.getClass().getTypeName() = " + someObj3.getClass().getTypeName());
  }

}
