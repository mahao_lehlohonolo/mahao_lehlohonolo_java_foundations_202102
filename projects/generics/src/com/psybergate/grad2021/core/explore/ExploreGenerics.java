package com.psybergate.grad2021.core.explore;

import java.util.ArrayList;

public class ExploreGenerics<T> {

  public static void main(String[] args) {
    //here are the different ways to declare generic lists
    ArrayList list = new ArrayList<String>();
    ArrayList<String> list2 = new ArrayList<String>();//the RHS String type declaration is
    // redundant, use the diamond notation as shown below.
    ArrayList<String> list3 = new ArrayList<>();
  }

  //static variables cannot use generic types, Why? Because static variables are shared among
  // objects> So objects usually have different declared types and the compiler will not know
  // which type to use for the static variable.
//  private static T NUM = 100;


}
