package com.psybergate.grad2021.core.hw1.hw1C;

import java.util.ArrayList;
import java.util.List;

public class MyClient {

  public static void main(String[] args) {
    List<String> l1 = new ArrayList<>();
    String s1 = "abc";
    String s2 = "def";

    l1.add(s1);
    l1.add(s2);

    MyClass cls = new MyClass();
    MyClassAfterErasure cls2 = new MyClassAfterErasure();
    List<String> l2 = cls.method1(l1);
    List<String> l3 = cls2.method1(l1);

    for (String s : l2) {
      System.out.println(s);
    }

    for (String s : l3) {
      System.out.println(s);
    }
  }

}
