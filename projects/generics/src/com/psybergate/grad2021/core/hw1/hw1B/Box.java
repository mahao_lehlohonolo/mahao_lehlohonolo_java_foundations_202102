package com.psybergate.grad2021.core.hw1.hw1B;

public class Box<T> { // T is a the Type Variable

  private T t;

  public Box() {
  }

  public void set(final T t) {
    this.t = t;
  }

  public T get() {
    return t;
  }

}
