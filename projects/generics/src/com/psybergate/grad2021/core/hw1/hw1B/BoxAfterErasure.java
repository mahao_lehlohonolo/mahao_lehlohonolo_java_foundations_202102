package com.psybergate.grad2021.core.hw1.hw1B;

public class BoxAfterErasure {

  private Object object;

  public BoxAfterErasure() {
  }

  public void set(final Object object) {
    this.object = object;
  }

  public Object get() {
    return object;
  }

}
