package com.psybergate.grad2021.core.hw1.hw1C;

import java.util.ArrayList;
import java.util.List;

public class MyClassAfterErasure {

  public MyClassAfterErasure() {
  }

  public List method1(List list) {
    return getReversedElements(list);
  }

  private List getReversedElements(List list) {
    List list2 = new ArrayList<>();
    for (Object s : list) {
      char[] c = s.toString().toCharArray();
      String str = new String();
      for (int i = c.length - 1; i >= 0; i--) {
        str += Character.toString(c[i]);
      }
      list2.add(str);
    }
    return list2;
  }

}
