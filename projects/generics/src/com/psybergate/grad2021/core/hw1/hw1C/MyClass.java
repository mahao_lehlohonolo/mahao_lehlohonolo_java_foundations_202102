package com.psybergate.grad2021.core.hw1.hw1C;

import java.util.ArrayList;
import java.util.List;

public class MyClass {

  public MyClass() {
  }

  public List<String> method1(List<String> list) {
    return getReversedElements(list);
  }

  private List<String> getReversedElements(List<String> list) {
    List<String> list2 = new ArrayList<>();
    for (String s : list) {
      char[] c = s.toCharArray();
      String str = new String();
      for (int i = c.length - 1; i >= 0; i--) {
        str += Character.toString(c[i]);
      }
      list2.add(str);
    }
    return list2;
  }

}
