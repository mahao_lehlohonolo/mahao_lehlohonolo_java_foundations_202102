SELECT name,STRING_AGG(accountnum,',') AS accounts,SUM(balance),COUNT(accountnum)
FROM customers_tbl,accounts_tbl
WHERE customers_tbl.customerid = accounts_tbl.customerid
GROUP BY name
HAVING COUNT(accountnum) > 5 AND SUM(balance) > 1000;