ALTER TABLE accounts_tbl ADD COLUMN needsToBeReviewed boolean;
ALTER TABLE accounts_tbl ALTER COLUMN needsToBeReviewed SET DEFAULT false;
UPDATE accounts_tbl SET needstobereviewed = false where balance > 2000;
UPDATE accounts_tbl SET needstobereviewed = true where balance < 2000;