SELECT name,MIN(balance) AS min_bal,MAX(balance) 
FROM accounts_tbl,customers_tbl 
GROUP BY name 
HAVING MIN(balance) > 1000 AND MAX(balance) > 8000;