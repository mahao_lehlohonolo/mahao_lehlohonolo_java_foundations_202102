//to list all the tables managed by postgres:
select * from information_schema.tables;
//to list all the tables that the user created:
select * from information_schema.tables where table_schema = 'public';
//can also use:
\z