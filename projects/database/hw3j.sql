SELECT transactions_tbl.accountnum,count(transactions_tbl.accountnum),customer_town,name,balance 
FROM transactions_tbl,customers_tbl,accounts_tbl 
WHERE customer_town = 'Johannesburg' AND transactions_tbl.accountnum = accounts_tbl.accountnum AND customers_tbl.customerid = accounts_tbl.customerid
GROUP BY transactions_tbl.accountnum,customer_town,name,balance
HAVING COUNT(transactions_tbl.accountnum) > 20;

SELECT name,customer_town,accounts_tbl.accountnum,balance,count(transactions_tbl.accountnum) AS num_of_transactions FROM customers_tbl
JOIN accounts_tbl on accounts_tbl.customerid = customers_tbl.customerid
JOIN transactions_tbl on transactions_tbl.accountnum = accounts_tbl.accountnum
WHERE customer_town = 'Johannesburg' GROUP BY name,customer_town,accounts_tbl.accountnum,balance
HAVING COUNT(transactions_tbl.accountnum) > 20;