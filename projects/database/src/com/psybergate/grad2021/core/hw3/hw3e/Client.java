package com.psybergate.grad2021.core.hw3.hw3e;

import com.psybergate.grad2021.core.hw3.hw3a.PrintToFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Client {

  public static void main(String[] args) throws FileNotFoundException {
    SelectCustomer selectCustomer = new SelectCustomer();
    PrintToFile printToFile = new PrintToFile();
    List<String> list = new ArrayList<>();

    list.add(selectCustomer.selectCustomer("accounts_tbl","customerid","'cust_3488'"));
    printToFile.printUpdateSQLToFile("selectcustomeraccounts.sql",list);

  }

}
