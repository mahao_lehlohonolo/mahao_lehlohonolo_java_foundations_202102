package com.psybergate.grad2021.core.hw3.hw3a;

import java.util.Arrays;
import java.util.List;

public class TransactionTypesGenerator {

  private final List<String> transactionTypes = Arrays.asList("('deposit')", "('withdrawal')");

  public String transactionTypes() {
    return "('" + transactionTypes.get(0) + "','" + transactionTypes.get(1) + "');";
  }

  public List<String> getTransactionTypes() {
    return transactionTypes;
  }

}
