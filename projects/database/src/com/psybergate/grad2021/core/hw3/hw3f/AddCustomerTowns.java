package com.psybergate.grad2021.core.hw3.hw3f;

import com.psybergate.grad2021.core.PostgreSQLConnector.Connector;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddCustomerTowns {

  /**
   * run hw3f1.sql and hw3f2.sql
   */

  private List<String> towns = Arrays.asList("'Johannesburg'", "'Cape Town'", "'Durban'");

  public String randomTown() {
    int max = towns.size();
    return towns.get((int) ((Math.random()) * max));
  }

  public List<String> addRandomTown() {
    List<String> list = new ArrayList<>();
    Connector connector = new Connector();
    Connection connection = connector.dataBaseConnector("homework_2b_db");
    Statement statement = null;
    ResultSet resultSet = null;
    try {
      statement = connection.createStatement();
      resultSet = statement.executeQuery("Select * from customers_tbl");
      while (resultSet.next()) {
        String customerID = resultSet.getString(1);
        String sql = "UPDATE customers_tbl SET customer_town = " + randomTown() + " WHERE " +
                "customerid = '" + customerID + "';";
        list.add(sql);
      }
      connection.close();
      statement.close();
      resultSet.close();
    } catch (SQLException throwable) {
      throw new RuntimeException(throwable);
    }
    return list;
  }


}
