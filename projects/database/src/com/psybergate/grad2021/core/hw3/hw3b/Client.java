package com.psybergate.grad2021.core.hw3.hw3b;

import com.psybergate.grad2021.core.hw3.hw3a.PrintToFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Client {

  public static void main(String[] args) throws FileNotFoundException {
    PrintToFile printToFile = new PrintToFile();
    List<String> list = new ArrayList<>();
    List<String> list2 = new ArrayList<>();
    AlterTable alterTable = new AlterTable();
    UpdateNeedsToBeReviewed untbr = new UpdateNeedsToBeReviewed();
    list2.add(untbr.needsToBeReviewed());
    list2.add(untbr.needsToBeReviewed2());
    printToFile.printUpdateSQLToFile("updateneedstobereviewed.sql",list2);

    String addColumn = alterTable.addColumn("accounts_tbl", "needsToBeReviewed", "boolean");
    String columnDefault = alterTable.setColumnDefault("accounts_tbl", "needsToBeReviewed",
            "false");
    list.add(addColumn);
    list.add(columnDefault);
    printToFile.printUpdateSQLToFile("addNeedsToBeReviewed.sql", list);
  }

}
