package com.psybergate.grad2021.core.hw3.hw3a;

import com.psybergate.grad2021.core.PostgreSQLConnector.Connector;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UpdateAccountBalances {

  public static void main(String[] args) {
    new UpdateAccountBalances().updateBalances();
  }

  public void updateBalances() {
    PrintToFile printToFile = new PrintToFile();
    List<String> updateStatements = new ArrayList<>();
    Connector connector = new Connector();
    Connection connection = connector.dataBaseConnector("homework_2b_db");
    ResultSet resultSet = null;
    Statement statement = null;
    try {
      statement = connection.createStatement();
      resultSet = statement.executeQuery("Select * from transactions_tbl");
      while (resultSet.next()) {
        String accountNum = null;
        int amount = 0;
        String sign = "+";
        String transactionType = resultSet.getString(2);
        accountNum = resultSet.getString(3);
        if (transactionType.equals("withdrawal")) {
          sign = "-";
        }
        amount = resultSet.getInt(4);
        String sql =
                "update accounts_tbl set balance = accounts_tbl.balance " + sign + " " + amount +
                        " where accountnum = '" + accountNum + "';";
        updateStatements.add(sql);
      }
      printToFile.printUpdateSQLToFile("updatebalances.sql", updateStatements);
      connection.close();
    } catch (SQLException | FileNotFoundException throwable) {
      throw new RuntimeException(throwable);
    }
  }

}
