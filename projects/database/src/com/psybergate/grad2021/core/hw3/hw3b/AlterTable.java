package com.psybergate.grad2021.core.hw3.hw3b;

public class AlterTable {

  /**
   * run hw3b.sql
   */

  public String addColumn(String tableName, String newColumnName, String type) {
    return "ALTER TABLE " + tableName + " ADD COLUMN " + newColumnName + " " + type + ";";
  }

  public String setColumnDefault(String tableName, String columnName, String defaultValue) {
    return "ALTER TABLE " + tableName + " ALTER COLUMN " + columnName + " SET DEFAULT " + defaultValue + ";";
  }

}
