package com.psybergate.grad2021.core.hw3.hw3f;

import com.psybergate.grad2021.core.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.core.MyAnnotations.DomainClass;

@DomainClass(name = "locations_tbl")
public class Location {

  @DomainAttribute(name = "customer_location", primaryKey = true)
  private Locations customerLocation;

}
