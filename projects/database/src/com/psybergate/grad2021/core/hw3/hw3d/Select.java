package com.psybergate.grad2021.core.hw3.hw3d;

public class Select {

  /**
   * run hw3d.sql
   */

  public String tableView(String tableName, String attribute, String attributeValue) {
    return "SELECT * FROM " + tableName + " WHERE " + attribute + " = " + attributeValue + ";";
  }

}
