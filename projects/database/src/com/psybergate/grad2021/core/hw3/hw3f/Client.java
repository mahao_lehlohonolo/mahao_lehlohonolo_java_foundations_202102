package com.psybergate.grad2021.core.hw3.hw3f;

import com.psybergate.grad2021.core.hw3.hw3a.PrintToFile;
import com.psybergate.grad2021.core.hw3.hw3c.AlterTable;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Client {

  public static void main(String[] args) throws FileNotFoundException {
    AlterTable alterTable = new AlterTable();
    AddCustomerTowns addCustomerTowns = new AddCustomerTowns();
    List<String> list = new ArrayList<>();
    list.add(alterTable.addColumn("customers_tbl","customer_town","varchar"));
    PrintToFile printToFile = new PrintToFile();
    printToFile.printUpdateSQLToFile("addtowncolumn.sql",list);

    List<String> list2 = new ArrayList<>();
    printToFile.printUpdateSQLToFile("addtowns.sql", addCustomerTowns.addRandomTown());

    //run addtowncolumn.sql
    //run addtowns.sql
    //run selecttransactions.sql for answering the question
    //run outerjoin.sql
    //run namesstartingwithn.sql

  }

}
