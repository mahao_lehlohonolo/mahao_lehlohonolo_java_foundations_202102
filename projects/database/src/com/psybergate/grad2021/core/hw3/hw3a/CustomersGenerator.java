package com.psybergate.grad2021.core.hw3.hw3a;

import java.util.ArrayList;
import java.util.List;

public class CustomersGenerator {

  private int COUNTER = 100;

  private List<String> customerIDs = new ArrayList<>();

  public List<String> generateCustomer(int numberOfCustomers) {
    List<String> customers = new ArrayList<>();
    String customer = null;
    while (numberOfCustomers > 0) {
      String customerID = customerID();
      customerIDs.add(customerID);
      customer = "('" + customerID + "','" + name() + surname() + "')";
      numberOfCustomers--;
      customers.add(customer);
    }
    return customers;
  }

  public List<String> getCustomerIDs() {
    return customerIDs;
  }

  private String customerID() {
    String customerId = "cust_" + randomString() + COUNTER++;
    return customerId;
  }

  private String name() {
    String name = "Nonny_";
    return name + randomString() + "','";
  }

  private String surname() {
    String surname = "Mahao_";
    return surname + randomString() + COUNTER++;
  }

  private String randomString() {
    int max = 10000;
    String random = "";
    random = (int) (Math.random() * max) + "";
    return random;
  }

}
