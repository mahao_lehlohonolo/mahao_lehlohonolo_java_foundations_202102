package com.psybergate.grad2021.core.hw3.hw3a;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class PrintToFile {

  public void printInsertSQLToFile(String fileName,String tableName ,List<String> list) throws IOException {
    sqlStatement sqlStatement = new sqlStatement();
    PrintWriter file = new PrintWriter(fileName);
    int count = 0;
    for (String record : list) {
      if (count == 0) {
        file.write(sqlStatement.insertSQL(tableName,record));
        count = 1;
      } else {
        file.write("\n" + sqlStatement.insertSQL(tableName,record));
      }
    }
    file.close();
  }

  public void printUpdateSQLToFile(String fileName, List<String> list) throws FileNotFoundException {
    PrintWriter file = new PrintWriter(fileName);
    int count = 0;
    for (String record : list) {
      if (count == 0) {
        file.write(record);
        count = 1;
      } else {
        file.write("\n" + record);
      }
    }
    file.close();
  }

}
