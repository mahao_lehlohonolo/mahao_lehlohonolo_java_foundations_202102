package com.psybergate.grad2021.core.hw3.hw3a;

import java.util.Arrays;
import java.util.List;

public class AccountTypesGenerator {

  final private List<String> accountTypes = Arrays.asList("('credit')", "('current')",
          "('savings')");

  public String accountTypes() {
    return "('" + accountTypes.get(0) + "','" + accountTypes.get(1) + "','" + accountTypes.get(2) + "');";
  }

  public List<String> getAccountTypes() {
    return accountTypes;
  }

}
