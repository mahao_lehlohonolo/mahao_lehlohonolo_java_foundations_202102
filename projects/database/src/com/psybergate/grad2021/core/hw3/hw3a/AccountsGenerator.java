package com.psybergate.grad2021.core.hw3.hw3a;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccountsGenerator {

  final static private List<String> accountTypes = Arrays.asList("credit", "current",
          "savings");

  private static int COUNTER = 20;

  DecimalFormat df = new DecimalFormat("##.##");

  private List<String> accountNumbers = new ArrayList<>();

  public List<String> generateAccount(int numOfAccountsPerCustomer, List<String> customerIDs) {
    List<String> accounts = new ArrayList<>();
    for (String customerID : customerIDs) {
      int counter = numOfAccountsPerCustomer;
      String account = null;
      if (customerID.charAt(customerID.length() - 1) == '2') {
        continue;
      }
      while (counter > 0) {
        String accountNumber = randomAccountNumber();
        accountNumbers.add(accountNumber);
        account =
                "('" + accountNumber + "','" + accountType() + customerID + "'," + balance() + ")";
        accounts.add(account);
        counter--;
      }
    }
    return accounts;
  }

  public List<String> getAccountNumbers() {
    return accountNumbers;
  }

  private String balance() {
    int min = - 50;
    int max = 500;
    return df.format(Math.random() * ((max - min) + 1) + min);
  }

  private String accountType() {
    return accountTypes.get(accountTypeRandomIndex()) + "','";
  }

  private int accountTypeRandomIndex() {
    return (int) (Math.random() * 3);
  }

  private String randomAccountNumber() {
    String prefix = "acc_";
    return prefix + randomInt() + COUNTER++;
  }

  private String randomInt() {
    int random = (int) (Math.random() * 100000);
    return random + "";
  }

}
