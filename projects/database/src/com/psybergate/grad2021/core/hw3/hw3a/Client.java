package com.psybergate.grad2021.core.hw3.hw3a;

import java.io.IOException;
import java.util.List;

public class Client {

  public static void main(String[] args) {


    CustomersGenerator custGen = new CustomersGenerator();
    AccountTypesGenerator accTypesGen = new AccountTypesGenerator();
    TransactionTypesGenerator transTypesGen = new TransactionTypesGenerator();
    AccountsGenerator accGen = new AccountsGenerator();
    TransactionsGenerator transGen = new TransactionsGenerator();

    List<String> customers = custGen.generateCustomer(20);
    List<String> accountTypes = accTypesGen.getAccountTypes();
    List<String> transactionTypes = transTypesGen.getTransactionTypes();
    List<String> accounts = accGen.generateAccount(8, custGen.getCustomerIDs());
    List<String> transactions = transGen.generateTransaction(100, accGen.getAccountNumbers());

//    printInsertSQL(customers, "customers_tbl");
//    printInsertSQL(accountTypes, "account_types_tbl");
//    printInsertSQL(transactionTypes, "transaction_types_tbl");
//    printInsertSQL(accounts, "accounts_tbl");
//    printInsertSQL(transactions, "transactions_tbl");

    printInsertSQLToFile(customers, "customers.sql", "customers_tbl");
    printInsertSQLToFile(accountTypes,"accounttypes.sql","account_types_tbl");
    printInsertSQLToFile(transactionTypes,"transactiontypes.sql","transaction_types_tbl");
    printInsertSQLToFile(accounts, "accounts.sql", "accounts_tbl");
    printInsertSQLToFile(transactions, "transactions.sql", "transactions_tbl");
  }

  private static void printInsertSQLToFile(List<String> records, String fileName,
                                           String tableName) {
    PrintToFile print = new PrintToFile();
    try {
      print.printInsertSQLToFile(fileName, tableName, records);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static void printInsertSQL(List<String> records, String tableName) {
    sqlStatement sqlStatement = new sqlStatement();
    for (String record : records) {
      System.out.println(sqlStatement.insertSQL(tableName, record));
    }
  }

}

