package com.psybergate.grad2021.core.hw3.hw3d;

import com.psybergate.grad2021.core.hw3.hw3a.PrintToFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Client {

  public static void main(String[] args) throws FileNotFoundException {
    Select view = new Select();
    PrintToFile printToFile = new PrintToFile();
    List<String> list = new ArrayList<>();

    list.add(view.tableView("accounts_tbl","account_type","'current'"));
    printToFile.printUpdateSQLToFile("viewcurrentaccounts.sql",list);
  }

}
