package com.psybergate.grad2021.core.hw3.hw3a;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransactionsGenerator {

  private static int COUNTER = 10;

  private final List<String> transactionTypes = Arrays.asList("deposit", "withdrawal");

  DecimalFormat df = new DecimalFormat("##.##");

  public List<String> generateTransaction(int numOfTransactionsPerAcc,
                                          List<String> accountNumbers) {
    List<String> transactions = new ArrayList<>();
    for (String accountNumber : accountNumbers) {
      int counter = numOfTransactionsPerAcc;
      String transaction = null;
      if (accountNumber.charAt(accountNumber.length() - 1) == '2') {
        continue;
      }
      while (counter > 0) {
        transaction = "('" +
                transactionID() + randomTransactionType() + accountNumber + "'," + randomAmount() + ",'" + date() + "')";
        transactions.add(transaction);
        counter--;
      }
    }
    return transactions;
  }

  private String transactionID() {
    String prefix = "trans_";
    int transactionID = (int) (Math.random() * 10000);
    return prefix + transactionID + COUNTER++ + "','";
  }

  private String date() {
    return LocalDate.now() + "";
  }

  private String randomTransactionType() {
    int max = transactionTypes.size();
    return transactionTypes.get((int) (Math.random() * max)) + "','";
  }

  private String randomAmount() {
    return df.format(Math.random() * 1000);
  }

}
