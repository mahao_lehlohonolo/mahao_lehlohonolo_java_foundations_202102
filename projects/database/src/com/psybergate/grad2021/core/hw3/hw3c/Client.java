package com.psybergate.grad2021.core.hw3.hw3c;

import com.psybergate.grad2021.core.hw3.hw3a.PrintToFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Client {

  public static void main(String[] args) throws FileNotFoundException {
    AlterTable deleteRow = new AlterTable();
    AlterTable dropAndAdd = new AlterTable();
    PrintToFile printToFile = new PrintToFile();
    List<String> list = new ArrayList<>();
    List<String> list2 = new ArrayList<>();
    list.add(deleteRow.dropRowOfLowestValue("accounts_tbl","balance"));
    printToFile.printUpdateSQLToFile("droplowbalaccount.sql",list);

    String dropConstraint = dropAndAdd.dropConstraint("transactions_tbl", "accountnum");
    String onDeleteCascade = dropAndAdd.addConstraintWithOnDeleteCascade("transactions_tbl",
            "accounts_tbl", "accountnum");
    String dropConstraint2 = dropAndAdd.dropConstraint("transactions_tbl", "transaction_type");
    String onDeleteCascade2 = dropAndAdd.addConstraintWithOnDeleteCascade("transactions_tbl",
            "transaction_types_tbl", "transaction_type");

    list2.add(dropConstraint);
    list2.add(onDeleteCascade);
    list2.add(dropConstraint2);
    list2.add(onDeleteCascade2);

    printToFile.printUpdateSQLToFile("dropaddfkey.sql",list2);
  }

}
