package com.psybergate.grad2021.core.hw3.hw3a;

import com.psybergate.grad2021.core.hw2.hw2b.AccountTypes;
import com.psybergate.grad2021.core.hw2.hw2b.TransactionTypes;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class RecordsGenerator {

  final private Collection<AccountTypes> accountTypes = Arrays.asList(AccountTypes.CREDIT,
          AccountTypes.CURRENT, AccountTypes.SAVINGS);

  final private Collection<TransactionTypes> transactionTypes =
          Arrays.asList(TransactionTypes.DEPOSIT,
                  TransactionTypes.WITHDRAWAL);

  private Collection<String> customerIDs = new ArrayList<>();

  private Collection<String> accountNumbers = new ArrayList<>();

//  public void customers(int numberOfCustomers) {
//    CustomersGenerator c = new CustomersGenerator();
//    try {
//      FileWriter file = new FileWriter("customers.sql");
//      c.generateCustomer(numberOfCustomers);
//    } catch (IOException e) {
//      throw new RuntimeException(e);
//    }
//  }

}
