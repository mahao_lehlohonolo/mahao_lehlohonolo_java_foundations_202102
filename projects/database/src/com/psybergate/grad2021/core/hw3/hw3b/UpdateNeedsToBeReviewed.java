package com.psybergate.grad2021.core.hw3.hw3b;

public class UpdateNeedsToBeReviewed {

  public String needsToBeReviewed() {
    return "update accounts_tbl SET needstobereviewed = false where balance > 2000;";
  }

  public String needsToBeReviewed2() {
    return "update accounts_tbl SET needstobereviewed = true where balance < 2000;";
  }

}
