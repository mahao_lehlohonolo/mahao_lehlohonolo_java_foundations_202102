package com.psybergate.grad2021.core.hw3.hw3e;

import com.psybergate.grad2021.core.hw3.hw3d.Select;

public class SelectCustomer {

  /**
   * run hw3e1.sql , hw3e2.sql, hw3e3.sql
   */

  public String selectCustomer(String tableName, String attribute, String attributeValue) {
    Select view = new Select();
    return view.tableView(tableName, attribute, attributeValue);
  }

}
