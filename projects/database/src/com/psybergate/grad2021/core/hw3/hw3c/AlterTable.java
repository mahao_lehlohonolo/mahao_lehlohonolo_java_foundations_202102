package com.psybergate.grad2021.core.hw3.hw3c;

public class AlterTable {

  /**
   * run hw3c.sql
   */

  public String dropRowOfLowestValue(String tableName, String attribute) {
    return "DELETE FROM " + tableName + " WHERE " + attribute + " = (select min(" + attribute +
            ") from " + tableName + ");";
  }

  public String dropConstraint(String tableName, String attributeOfReference) {
    return "ALTER TABLE " + tableName + " DROP CONSTRAINT " + tableName + "_" + attributeOfReference + "_fkey;";
  }

  public String addConstraintWithOnDeleteCascade(String tableName, String parentTable,
                                                 String attribute) {
    return "ALTER TABLE " + tableName + " ADD CONSTRAINT " + tableName + "_" + attribute +
            "_fkey" + " FOREIGN KEY (" + attribute +
            ") REFERENCES " + parentTable + "(" + attribute + ") ON DELETE CASCADE;";
  }

  public String addColumn(String tableName, String newColumnName, String type) {
    return "ALTER TABLE " + tableName + " ADD COLUMN " + newColumnName + " " + type + ";";
  }

  public String setColumnDefault(String tableName, String columnName, String defaultValue) {
    return "ALTER TABLE " + tableName + " ALTER COLUMN " + columnName + " SET DEFAULT " + defaultValue + ";";
  }


}
