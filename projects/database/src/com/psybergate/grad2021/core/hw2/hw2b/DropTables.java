package com.psybergate.grad2021.core.hw2.hw2b;

import com.psybergate.grad2021.core.PostgreSQLConnector.Connector;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DropTables {

  public void dropTables() throws IOException, SQLException {
    Connection connection = new Connector().dataBaseConnector("homework_2b_db");
    String filePath = "C:\\myworkbench\\mymentoring\\java_foundations_202102\\projects\\database" +
            "\\droptables.sql";
    List<String> sqlStatements = Files.readAllLines(Paths.get(filePath));
    Statement statement = connection.createStatement();
    for (String sql : sqlStatements) {
      try {
        statement.executeUpdate(sql);
        System.out.println(sql);
      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    }
  }

}
