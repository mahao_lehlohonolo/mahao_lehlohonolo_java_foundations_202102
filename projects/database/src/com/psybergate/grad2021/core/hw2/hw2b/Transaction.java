package com.psybergate.grad2021.core.hw2.hw2b;

import com.psybergate.grad2021.core.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.core.MyAnnotations.DomainClass;

@DomainClass(name = "transactions_tbl")
public class Transaction {

  @DomainAttribute(name = "transactionID", primaryKey = true)
  private String transactionID;

  @DomainAttribute(name = "transaction_type", foreignKey = true, foreignKeyRef =
          "transaction_types_tbl")
  private TransactionType transactionType;

  @DomainAttribute(name = "accountNum", foreignKey = true, foreignKeyRef = "accounts_tbl")
  private String accountNum;

  @DomainAttribute(name = "amount", type = "int")
  private Integer amount;

  @DomainAttribute(name = "dateOfTransaction")
  private String dateOfTransaction;

  public Transaction(String transactionID, TransactionType transactionType, String accountNum,
                     Integer amount, String dateOfTransaction) {
    this.transactionID = transactionID;
    this.transactionType = transactionType;
    this.accountNum = accountNum;
    this.amount = amount;
    this.dateOfTransaction = dateOfTransaction;
  }

}
