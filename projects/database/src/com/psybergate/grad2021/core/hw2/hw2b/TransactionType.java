package com.psybergate.grad2021.core.hw2.hw2b;

import com.psybergate.grad2021.core.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.core.MyAnnotations.DomainClass;

@DomainClass(name = "transaction_types_tbl")
public class TransactionType {

  @DomainAttribute(primaryKey = true, name = "transaction_type")
  private TransactionTypes transactionType;

}
