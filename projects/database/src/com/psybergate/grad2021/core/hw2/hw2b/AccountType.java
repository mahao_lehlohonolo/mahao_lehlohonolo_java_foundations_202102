package com.psybergate.grad2021.core.hw2.hw2b;

import com.psybergate.grad2021.core.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.core.MyAnnotations.DomainClass;

@DomainClass(name = "account_types_tbl")
public class AccountType {

  @DomainAttribute(primaryKey = true, name = "account_type")
  private AccountTypes accountType;

}
