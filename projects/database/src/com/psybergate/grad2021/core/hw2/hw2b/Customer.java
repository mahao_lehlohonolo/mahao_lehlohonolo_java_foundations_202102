package com.psybergate.grad2021.core.hw2.hw2b;

import com.psybergate.grad2021.core.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.core.MyAnnotations.DomainClass;
import com.psybergate.grad2021.core.MyAnnotations.DomainTransient;

import java.util.Collection;

@DomainClass(name = "customers_tbl")
public class Customer {

  @DomainAttribute(name = "customerID", primaryKey = true)
  private String customerID;

  @DomainAttribute(name = "name")
  private String name;

  @DomainAttribute(name = "surname")
  private String surname;

  @DomainTransient
  private Collection<Account> customerAccounts;

  public Customer(String customerID, String name, String surname) {
    this.customerID = customerID;
    this.name = name;
    this.surname = surname;
  }

}
