package com.psybergate.grad2021.core.hw2.hw2b;

import com.psybergate.grad2021.core.MyAnnotations.DomainAttribute;
import com.psybergate.grad2021.core.MyAnnotations.DomainClass;

@DomainClass(name = "accounts_tbl")
public class Account {

  @DomainAttribute(name = "accountNum",primaryKey = true)
  private String accountNum;

  @DomainAttribute(name = "account_type",foreignKey = true, foreignKeyRef = "account_types_tbl")
  private AccountType accountType;

  @DomainAttribute(name = "customerID",foreignKey = true, foreignKeyRef = "customers_tbl")
  private String customerID;

  @DomainAttribute(name = "balance",type = "int")
  private Integer balance;

  public Account(String accountNum, AccountType accountType, String customerID, Integer balance) {
    this.accountNum = accountNum;
    this.accountType = accountType;
    this.customerID = customerID;
    this.balance = balance;
  }

}
