package com.psybergate.grad2021.core.hw2.hw2b;

import java.io.IOException;
import java.sql.SQLException;

public class Client {

  public static void main(String[] args) throws SQLException, IOException {

    Class cls1 = Account.class;
    Class cls2 = Transaction.class;
    Class cls3 = AccountType.class;
    Class cls4 = TransactionType.class;
    Class cls5 = Customer.class;

    Worker worker = new Worker();

    worker.createTable(cls5);
    worker.createTable(cls3);
    worker.createTable(cls4);
    worker.createTable(cls1);
    worker.createTable(cls2);

  }

}
