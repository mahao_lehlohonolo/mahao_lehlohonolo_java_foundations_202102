package com.psybergate.grad2021.core.hw2.hw2b;

public enum AccountTypes {
  CURRENT,
  SAVINGS,
  CREDIT;
}
