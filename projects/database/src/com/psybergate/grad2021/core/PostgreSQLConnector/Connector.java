package com.psybergate.grad2021.core.PostgreSQLConnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Connector {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DATABASE_URL = "jdbc:postgresql://localhost/";

  private static final String USER = "postgres";

  private static final String PASSWORD = "admin";

  public Connection dataBaseConnector(String dataBaseName) {
    Connection connection = null;
    Statement statement = null;
    try {
      Class.forName(JDBC_DRIVER);
      connection = DriverManager.getConnection(DATABASE_URL + dataBaseName, USER, PASSWORD);
    } catch (Throwable e) {
      throw new RuntimeException(e);
    }
    return connection;
  }

}
