SELECT name,accountnum,accounts_tbl.account_type,balance FROM accounts_tbl
JOIN customers_tbl ON customers_tbl.customerid = accounts_tbl.customerid
JOIN account_types_tbl ON account_types_tbl.account_type = accounts_tbl.account_type 
WHERE accounts_tbl.account_type = 'credit' OR accounts_tbl.account_type = 'savings';