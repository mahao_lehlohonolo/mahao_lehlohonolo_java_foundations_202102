ALTER TABLE accounts_tbl ADD COLUMN needsToBeReviewed boolean;
ALTER TABLE accounts_tbl ALTER COLUMN needsToBeReviewed SET DEFAULT false;
update accounts_tbl SET needstobereviewed = false where balance > 2000;
update accounts_tbl SET needstobereviewed = true where balance < 2000;