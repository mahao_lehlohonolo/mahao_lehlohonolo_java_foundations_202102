ALTER TABLE accounts_tbl ADD COLUMN needsToBeReviewed boolean;
ALTER TABLE accounts_tbl ALTER COLUMN needsToBeReviewed SET DEFAULT false;