ALTER TABLE transactions_tbl DROP CONSTRAINT transactions_tbl_accountnum_fkey;
ALTER TABLE transactions_tbl ADD CONSTRAINT transactions_tbl_accountnum_fkey FOREIGN KEY (accountnum) REFERENCES accounts_tbl(accountnum) ON DELETE CASCADE;
ALTER TABLE transactions_tbl DROP CONSTRAINT transactions_tbl_transaction_type_fkey;
ALTER TABLE transactions_tbl ADD CONSTRAINT transactions_tbl_transaction_type_fkey FOREIGN KEY (transaction_type) REFERENCES transaction_types_tbl(transaction_type) ON DELETE CASCADE;