SELECT * FROM accounts_tbl WHERE customerid = 'cust_3488';
ALTER TABLE customers_tbl ADD COLUMN customer_town varchar;
UPDATE customers_tbl SET customer_town = 'Durban' WHERE customerid = 'cust_8343100';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_4686102';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_1529104';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_2096106';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_6873108';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_6744110';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_5193112';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_302114';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_1135116';
UPDATE customers_tbl SET customer_town = 'Durban' WHERE customerid = 'cust_2714118';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_5546120';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_1016122';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_8391124';
UPDATE customers_tbl SET customer_town = 'Durban' WHERE customerid = 'cust_5304126';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_6117128';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_8714130';
UPDATE customers_tbl SET customer_town = 'Durban' WHERE customerid = 'cust_4842132';
UPDATE customers_tbl SET customer_town = 'Durban' WHERE customerid = 'cust_4191134';
UPDATE customers_tbl SET customer_town = 'Johannesburg' WHERE customerid = 'cust_4589136';
UPDATE customers_tbl SET customer_town = 'Cape Town' WHERE customerid = 'cust_152138';

SELECT name,count(accountnum),sum(balance) FROM customers_tbl JOIN accounts_tbl 
ON accounts_tbl.customerid = customers_tbl.customerid 
GROUP BY name;

SELECT name,count(accountnum),sum(balance) FROM customers_tbl,accounts_tbl 
WHERE accounts_tbl.customerid = customers_tbl.customerid 
GROUP BY name;

SELECT name,count(accountnum),sum(balance) FROM customers_tbl JOIN accounts_tbl 
ON accounts_tbl.customerid = customers_tbl.customerid GROUP BY name 
HAVING count(accountnum)>5 and sum(balance)>1000;