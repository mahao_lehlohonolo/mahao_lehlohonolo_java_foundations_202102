//to update everything:
UPDATE customer_tbl SET name = 'mahao',surname = 'mahao';
//use the returning clause, renames the column of the resultset
UPDATE customer_tbl SET name = 'mahao' WHERE customerid = 'cust_1234' RETURNING name AS newname;
//to delete using not equals to:
DELETE FROM customer_tbl WHERE name <> 'Mahao';
