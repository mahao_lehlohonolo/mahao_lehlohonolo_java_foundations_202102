package com.psybergate.grad2021.core.ce2;

public class StringImmutability {

  public static void main(String[] args) {
    String str1 = "abc";
    System.out.println("str1 before concat: " + str1);
    str1.concat("def");
    System.out.println("str1 after concat: " + str1);
    //str1 was not changed, shows immutability.

    String str2 = "abc";

  }

}
