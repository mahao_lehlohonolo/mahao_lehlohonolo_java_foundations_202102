package com.psybergate.grad2021.core.ce2;

public class StringsBuilderAndBuffer {

  public static void main(String[] args) {
    StringBuilder str1 = new StringBuilder("abc");
    str1.append("def");
    //StringBuilder class is mutable.
    System.out.println(str1);

    StringBuffer str2 = new StringBuffer("zxy");
    str2.append("def");
    //StringBuffer class is also mutable
    System.out.println(str2);
    //StringBuilder and Buffer are fluent interfaces
    str2.append("abc").append("def").insert(0,'D');
    str1.append("123").append("456").insert(0,'G');
    System.out.println("str1 = " + str1);
    System.out.println("str2 = " + str2);
    System.out.println("(str2 instanceof StringBuffer) = " + (str2 instanceof StringBuffer));

    System.out.println(str1.getClass().getSuperclass());
    System.out.println(str2.getClass().getSuperclass());

  }

}
