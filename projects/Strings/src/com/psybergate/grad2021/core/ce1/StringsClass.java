package com.psybergate.grad2021.core.ce1;

public class StringsClass {

  public static void main(String[] args) {
    String myString = "";

    String str1 = new String("Hello");
    String str2 = str1.intern();
    String str5 = "Mahao";
    System.out.println("str5.intern().getClass() = " + str5.intern().getClass());
    String str3 = "Hello";
    String str4 = new String("Hello");
    System.out.println("str1.equals(str2) = " + str1.equals(str2));
    System.out.println("(str1==str2) = " + (str1==str2));
    System.out.println("(str2==str3) = " + (str2 == str3));
    System.out.println("str1.equals(str3) = " + str1.equals(str3));
    System.out.println("str1.equals(str4) = " + str1.equals(str4)); //same contents
    System.out.println("(str1==str4) = " + (str1 == str4)); //different memory location/address.
    // the '==' checks for identity.
    System.out.println(str1.getClass().getSuperclass());

  }


}
