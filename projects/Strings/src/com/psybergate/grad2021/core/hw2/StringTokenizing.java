package com.psybergate.grad2021.core.hw2;

import java.util.StringTokenizer;

public class StringTokenizing {

  public static void main(String[] args) {
    String myStr = "I@love you";
    StringTokenizer myStr2 = new StringTokenizer(myStr,"@");
    // '@' is the delimiter.
    //there is no difference in hasMoreElements and hasMoreTokens method, just that the
    // hasMoreElements calls and returns the hasMoreTokens.
    while (myStr2.hasMoreElements()){
      System.out.println(myStr2.nextElement());
    }
  }

}
