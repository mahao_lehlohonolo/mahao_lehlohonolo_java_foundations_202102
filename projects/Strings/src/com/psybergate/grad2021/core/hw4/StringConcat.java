package com.psybergate.grad2021.core.hw4;

public class StringConcat {

  public static void main(String[] args) {
    String st = "String";
    int num = 2;
    System.out.println(num+st);//if one of the operands is a string, the compiler converts the
    // other to a string as well.
    System.out.println(num+2);//if the operands are the same type, it does the addition based on
    // the types and then converts the result to a string
    System.out.println((num+2.1));//if the operands are int and double types, it casts the int to
    // a double then adds the two then converts the result to a string to be printed

    //check out the many different println() methods
  }

}
