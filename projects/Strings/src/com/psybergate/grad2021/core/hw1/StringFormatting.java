package com.psybergate.grad2021.core.hw1;

/**
 *
 */
public class StringFormatting {

  public static void main(String[] args) {
    String str1 = "mahao";
    String str2 = String.format("lehlohonolo %s",str1);
    String str3 = String.format("%h",140);
    String str4 = String.format("%o",140);
    String str5 = String.format("%x",140);
    String str6 = String.format("%d",140);
    String str7 = String.format("%c",'L');
    String str8 = String.format("%f",140.0);

    System.out.println(str2);
    System.out.println(str3);
    System.out.println(str4);
    System.out.println(str5);
    System.out.println(str6);
    System.out.println(str7);
    System.out.println(str8);
  }

}
