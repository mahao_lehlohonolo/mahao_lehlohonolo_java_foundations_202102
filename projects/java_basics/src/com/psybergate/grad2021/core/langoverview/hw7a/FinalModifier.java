package com.psybergate.grad2021.core.langoverview.hw7a;

public class FinalModifier {
  static final int somenumber = 0;
  //the static variable somenumber can be final if it is initialized.
  //if it is just 'final int somenumber' and not initialized, a constructor must be added since
  // it is not a static variable it is an instance/object variable

  private int someNum;
  private String someString;

  public FinalModifier(int someNum, String someString) { //final is not allowed on a constructor
    // as  it is already final.
    this.someNum = someNum;
    this.someString = someString;
  }

  public int someInstanceMethod(int b) { //only works through an object, see under main.
    return b*10;
  }

  public final static int someFinalMethod(int a) {
    return a;
  }

  public static void main(String[] args) {
    System.out.println(someFinalMethod(somenumber));
    FinalModifier obj = new FinalModifier(100,"Mahao");
    System.out.println(obj.someInstanceMethod(100));
  }

}
//the parent class FinalModifier cannot be extended if it is final.
//class ChildClass extends FinalModifier{
//   @Override
//   public final static int someFinalMethod(int a){ //cannot override a final method
//      return a;
//   }
//}
