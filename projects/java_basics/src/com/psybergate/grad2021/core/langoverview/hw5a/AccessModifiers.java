package com.psybergate.grad2021.core.langoverview.hw5a;

final class AccessModifiers {
  //if class is public it can be accessed anywhere.
  protected static void someMethod() {
    //the method is protected, meaning its accessible in the package only and through a  child
    // class(inheritance)
    System.out.println("SomeString");
  }

  public static void main(String[] args) {

  }
}

//class ExtenderClass extends AccessModifiers{ //the extending class is not allowed  because
// AccessModifiers is final.
//
//}

