package com.psybergate.grad2021.core.langoverview.hw1a;

import java.util.function.IntBinaryOperator;

public class MinMax {
   public static void main(String[] args) {
      byte someByte = 127; //maximum byte value
      byte someByte1 = -128; //minimum byte value

      int someInt = 0b1111111111111111111111111111111; //maximum int value
      int someInt1 = -0b1111111111111111111111111111111; //minimum int value

      short someShort = 0b111111111111111; //maximum short value
      short someShort1 = -0b111111111111111; //maximum short value

      long someLong = 0b111111111111111111111111111111111111111111111111111111111111111L; //maximum long value
      long someLong1 = -0b111111111111111111111111111111111111111111111111111111111111111L; //minimum long value

      System.out.println(someLong1);
   }
}
