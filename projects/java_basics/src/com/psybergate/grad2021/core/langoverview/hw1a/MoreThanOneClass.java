package com.psybergate.grad2021.core.langoverview.hw1a;

public class MoreThanOneClass { //no matter how many classes you have in a .java file, you are
  // allowed to have only one public class that has the same name as the java file.
  public static void main(String[] args) {

  }
}

class A {

}

class B {

}

class C {

}

class D {

}
