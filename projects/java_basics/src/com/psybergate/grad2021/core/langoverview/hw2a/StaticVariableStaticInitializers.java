package com.psybergate.grad2021.core.langoverview.hw2a;

public class StaticVariableStaticInitializers {
   static int number;//a static variable, belongs to the class.

   static {
      //when the class is loaded, the static initializer will initialize all the static variables
      // it is asked to initialize. Even if the static variable is initialized at the top the
      // value that the initializer is asked to assign will be the priority.
      System.out.println("I am initalizing static variables");
      number = 100;
   }


   public static void main(String[] args) {

   }
}
