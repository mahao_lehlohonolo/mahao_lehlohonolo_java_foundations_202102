package com.psybergate.grad2021.core.langoverview.hw5a;

public class SomeClass {
  public static void main(String[] args) {
    AccessModifiers.someMethod();
    //someMethod that is in AccessModifiers is accessible in the same package because//
    // the class is public and the method is protected/public
  }
}
