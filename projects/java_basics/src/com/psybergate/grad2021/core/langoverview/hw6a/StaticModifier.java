package com.psybergate.grad2021.core.langoverview.hw6a;

public class StaticModifier {
   static int number = 50; //must have static as well even if it is in the static initializer.
   //the int literal assigned to number in the static initializer takes priority.
   static{
      System.out.println("Static initializer ran.");
      number=100;
   }

   public static int someOtherMethod(int num){
      return num;
   }

   public static void main(String[] args) {
      System.out.println(someOtherMethod(number));//this would not compile if someOtherMethod
      //was not static meaning a static method only accepts static(class) variables
      //if it is non-static then it is an instance method, it will only work if you have an
      // object created.
   }
}
