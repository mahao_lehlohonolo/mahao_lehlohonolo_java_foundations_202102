public class OpenClosedDoors {
	public static void Solver() {
		int lockerNum = 1000;
		char[] lockers = new char[lockerNum];

		for (int count = 0; count < lockerNum; count++) {
			lockers[count] = '1';
		}

		for (int count1 = 1; count1 <= lockerNum; count1++) {
			for (int count2 = count1; count2 < lockerNum; count2+=count1 + 1) {
				if (lockers[count2] == '0') {
					lockers[count2] = '1';
				}
				else if (lockers[count2] == '1') {
					lockers[count2] = '0';	
				}
			}
		}

		int numberofopen = 0;
		for (int count = 0; count < lockerNum; count++) {
			if (lockers[count] == '1') {
				numberofopen++;
			}
		}
		System.out.println(numberofopen);
	}
	public static void main(String[] args) {
		Solver();
	}
}