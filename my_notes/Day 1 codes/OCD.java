public class OCD {
	public static void Solver() {
		int nums = 1000;
		char[] Lockers = new char[nums];
		
		for(int count=0; count<nums; count++) {
			Lockers[count] = '0';
		}
		
		for(int count1=1; count1<=nums; count1++) {
			for(int count2=0; count2<nums; count2++) {
				int counter = count1*count2;
				if(counter<nums) {
					if(Lockers[counter]=='0') {
						Lockers[counter]='1';
						continue;
					}
					if(Lockers[counter]=='1') {
						Lockers[counter]='0';
						continue;
					}
				}
			}
		}
		int numberofopen = 0;
		for(int count=0; count<nums; count++) {
			if(Lockers[count]=='1') {
				numberofopen++;
			}
		}
		System.out.println(numberofopen);
	}
	public static void main(String[] args) {
	Solver();	
	}
}